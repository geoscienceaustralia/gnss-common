# shellcheck shell=bash

# Run `nix develop`, but in the current shell (see `nix print-dev-env --help`)
#
# Bitbucket wraps pipeline step commands in a bash script, so by sourcing `nix print-dev-env` in
# bashrc we avoid having to wrap every script command in `nix develop --command <command>`.
#
# This functionality is available only if `AUTO_NIX_SHELL` is set to `true` in Bitbucket repository
# variables.
#
# Both `nix develop` and `nix print-dev-env` set `IN_NIX_SHELL`, which we use to guard against
# loading the shell multiple times.

export AWS_PROFILE=gnss-common
export AWS_REGION=ap-southeast-2

if [[ $AUTO_NIX_SHELL == true && ! -v IN_NIX_SHELL && -f flake.nix ]]; then
    # If repository variable `PURE_NIX_SHELL_PATH` is true, only packages explicitly defined in
    # `flake.nix` should be in PATH, however, `nix print-dev-env` itself will need access to `nix`
    # and `git` from the docker image to load the flake.
    path=$PATH
    if [[ $PURE_NIX_SHELL_PATH == true ]]; then
        # shellcheck disable=2123
        PATH=
    fi
    # shellcheck source=/dev/null
    . <(PATH=$path nix print-dev-env .#ci)
fi

if [[ -v BITBUCKET_STEP_OIDC_TOKEN && ! -d /root/.aws ]]; then
    mkdir -p /root/.aws
    echo "$BITBUCKET_STEP_OIDC_TOKEN" > /root/.aws/web-identity-token
    cat > /root/.aws/config << EOF
[profile $AWS_PROFILE]
region = $AWS_REGION
role_arn = arn:aws:iam::023072794443:role/bitbucket-pipelines
web_identity_token_file = /root/.aws/web-identity-token

[profile $AWS_PROFILE-prod]
region = $AWS_REGION
role_arn = arn:aws:iam::334594953176:role/bitbucket-pipelines
web_identity_token_file = /root/.aws/web-identity-token
EOF
fi
