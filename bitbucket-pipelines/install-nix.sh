#!/usr/bin/env bash

set -euo pipefail

# Install nix 2.21.2

installer=https://github.com/DeterminateSystems/nix-installer/releases/download/v0.19.1/nix-installer-x86_64-linux

# To lookup what version of nix installed by a particular version of the installer, run the following:
#
#   ```bash
#   nix flake metadata github:DeterminateSystems/nix-installer?ref=v0.19.1 --json | jq -r '.locks.nodes.nix.locked.url'`
#   https://api.flakehub.com/f/pinned/DeterminateSystems/nix/2.21.2/018ef218-45b2-731b-8c3b-a9fc57c55fd1/source.tar.gz
#   ```

curl -sSf -L "$installer" -o nix-installer
chmod +x nix-installer

./nix-installer install linux \
    --extra-conf "system-features = benchmark big-parallel nixos-test uid-range kvm" \
    --extra-conf "start-id = 10100" \
    --init none \
    --no-confirm

# --extra-conf
#
#   * system-features: KVM is unavailable in unprivileged docker containers and also unavailable in
#     EC2 instances, but we advertise it in system features anyway because nixos-generators
#     requires it, eventhough it does not necessarily use it. The other items declared in system-features are
#     the defaults.
#
#   * start-id: Override default user start uid (872415232), which is, for some reason, too high
#     for docker in Bitbucket Pipelines.

