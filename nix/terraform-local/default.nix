{ inputs, ... }:
{
  perSystem =
    { pkgs, ... }:
    {
      packages.terraform-local = inputs.dream2nix.lib.evalModules {
        modules = [
          ./dream2nix.nix
          {
            paths.projectRoot = ./.;
            paths.package = "/";
          }
        ];
        packageSets.nixpkgs = pkgs;
      };
    };
}
