{ config, dream2nix, ... }:
{
  imports = [ dream2nix.modules.dream2nix.pip ];

  deps =
    { nixpkgs, ... }:
    {
      python = nixpkgs.python3;
    };

  name = "terraform-local";
  version = "0.18.1";

  pip = {
    requirementsList = [ "${config.name}==${config.version}" ];
  };

  mkDerivation = {
    doCheck = false;
  };
}
