{ self, ... }:
{
  perSystem =
    {
      config,
      inputs',
      pkgs,
      ...
    }:
    {
      # Environment for custom pipeline `build-pipelines-docker-image`
      devShells.ciImage = pkgs.mkShellNoCC rec {
        name = "${self.name}-ci-image";

        packages = [
          (pkgs.buildEnv {
            inherit name;
            paths = [ config.packages.docker-login ];
          })
        ];

        shellHook = ''
          export PROJECT="${self.name}"
        '';
      };

      # Environment for all pipelines (except `build-pipelines-docker-image`)
      devShells.ci = pkgs.mkShellNoCC rec {
        name = "${self.name}-ci";

        inputsFrom = [ config.devShells.ciImage ];

        packages = [
          (pkgs.buildEnv {
            inherit name;

            paths = [
              config.packages.gnss-common-reader-role
              config.packages.init-kubeconfig
              config.packages.kubectl-slice
              config.packages.terraform-local

              pkgs.alejandra
              pkgs.awscli2
              pkgs.curl
              pkgs.docker
              pkgs.eksctl
              pkgs.fluxctl
              pkgs.git
              pkgs.jq
              pkgs.kubectl
              pkgs.kubernetes-helm
              pkgs.kustomize
              pkgs.openssh
              pkgs.openssl
              pkgs.python3Packages.credstash
              pkgs.terraform_1
              pkgs.yq

              # TODO: Change to `pkgs.omnix-cli` once https://github.com/juspay/omnix/issues/204 is done.
              inputs'.omnix.packages.omnix-cli
            ];
          })
        ];

        shellHook = ''
          # TODO: Workaround for
          #  *) https://github.com/NixOS/nix/issues/8355
          #  *) https://discourse.nixos.org/t/nix-no-longer-works-inside-nix-develop-inside-a-container/35875
          unset TMPDIR

          ${config.legacyPackages.developerShellHook}
        '';
      };

      # Environment for developer workstations
      devShells.default = config.devShells.developer;
      devShells.developer = pkgs.mkShellNoCC rec {
        name = self.name;

        inputsFrom = [ config.devShells.ci ];

        packages = [
          (pkgs.buildEnv {
            inherit name;

            paths = [
              config.packages.rnxcmp-4_1_0

              inputs'.gomod2nix.packages.default

              pkgs.kubectl-convert
              pkgs.kubent
              pkgs.nixfmt-rfc-style
              pkgs.plantuml
              pkgs.ssm-session-manager-plugin
            ];
          })
        ];
      };

      # Add to downstream flake shellHooks
      legacyPackages.developerShellHook = ''
        ${config.pre-commit.installationScript}
      '';
    };
}
