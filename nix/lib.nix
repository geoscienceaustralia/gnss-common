{ config, ... }:
{
  flake = {
    lib.systems = config.systems;

    /**
      TODO: Deprecated in favour of `buildEnv`.

      Unset PYTHONPATH for Python applications (built with `buildPythonApplication`) to avoid potential
      conflicts with any undeclared dependencies.

      See https://github.com/NixOS/nixpkgs/issues/11423 and linked issues.

      If rebuilding due to use of `overrideAttrs` is a problem, it is instead possible to use
      `writeShellApplication` to wrap a Python application and avoid rebuilding.

      ```
      lib.unsetPythonPath = pythonAppPkg: (pkgs.writeShellApplication {
        name = pythonAppPkg.meta.mainProgram;
        runtimeInputs = [ pythonAppPkg ];
        text = ''
          PYTHONPATH="" ${pythonAppPkg.meta.mainProgram} "$@"
        '';
      }).overrideAttrs (old: {
        buildCommand = old.buildCommand + ''
          # Delegate shell completion, etc., to the wrapped application
          ln -s ${pythonAppPkg}/share $out/share
        '';
      });
      ```
    */
    lib.unsetPythonPath =
      pythonAppPkg:
      pythonAppPkg.overrideAttrs (old: {
        makeWrapperArgs = (old.makeWrapperArgs or [ ]) ++ [ "--unset PYTHONPATH" ];
      });

    # TODO: Deprecated in favour of `buildEnv`.
    # Strip propagatedBuildInputs from Python packages.
    # See  https://github.com/NixOS/nixpkgs/issues/170887.
    lib.stripPropagatedBuildInputs =
      pkg:
      pkg.overrideAttrs (
        {
          postFixup ? "",
          ...
        }:
        {
          postFixup =
            postFixup
            + ''
              rm $out/nix-support/propagated-build-inputs
            '';
        }
      );
  };
}
