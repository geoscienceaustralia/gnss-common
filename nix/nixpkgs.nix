{ inputs, ... }:
{
  perSystem =
    { lib, system, ... }:
    {
      # Reconfigure pkgs
      _module.args.pkgs = import inputs.nixpkgs {
        inherit system;

        config.allowUnfreePredicate = pkg: lib.elem (lib.getName pkg) [ "terraform" ];

        # Uncomment to nest stable packages under pkgs, e.g., `pkgs.stable.packer`.
        # See also nixpkgs-stable input in flake.nix.
        # overlays = [ (_final: _prev: { stable = import inputs.nixpkgs-stable { inherit system; }; }) ];
      };
    };
}
