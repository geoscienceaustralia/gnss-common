_: {
  perSystem =
    { pkgs, ... }:
    {
      packages.list-vpc-cidr-blocks = pkgs.writeShellApplication {
        name = "list-vpc-cidr-blocks";

        runtimeInputs = [
          pkgs.awscli2
          pkgs.jq
        ];

        text = ''
          # List non-default VPC CIDR blocks. Unset AWS_PROFILE to run script for all
          # profiles in your ~/.aws/config.

          if [[ -n ''${AWS_PROFILE:-} ]]; then
              profiles=("''$AWS_PROFILE")
          else
              readarray -t profiles < <(aws configure list-profiles)
          fi

          for profile in "''${profiles[@]}"; do
              AWS_PROFILE=$profile aws ec2 describe-vpcs \
                | jq '
                  .Vpcs[]
                      | select (.IsDefault == false)
                      | {
                          "name": .Tags[] | select(.Key == "Name") | .Value,
                          "cidr": .CidrBlock
                        }
                '
          done
        '';
      };
    };
}
