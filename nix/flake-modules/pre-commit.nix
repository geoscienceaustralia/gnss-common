{ inputs, ... }:
_: {
  imports = [ inputs.git-hooks.flakeModule ];

  perSystem =
    { config, pkgs, ... }:
    {
      formatter = pkgs.nixfmt-rfc-style;

      # https://pre-commit.com
      pre-commit = {

        # Don't run hooks during `nix flake check`.
        check.enable = false;

        settings.default_stages = [
          "pre-commit"
          "push"
        ];

        settings.hooks = {
          # Bash
          shellcheck.enable = true;

          # Go
          gofmt.enable = true;

          # Nix
          nixfmt-rfc-style = {
            enable = true;
            package = config.formatter;
          };

          deadnix.enable = true;

          # Python
          black = {
            enable = true;
            settings.flags = "--line-length 120";
          };

          # Secrets
          ripsecrets.enable = true;

          # Terraform
          terraform-format.enable = true;
        };
      };
    };
}
