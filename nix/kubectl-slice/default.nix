_: {
  perSystem =
    { inputs', pkgs, ... }:
    {
      packages.kubectl-slice = inputs'.gomod2nix.legacyPackages.buildGoApplication rec {
        pname = "kubectl-slice";
        version = "1.2.5";

        src = pkgs.fetchFromGitHub {
          owner = "patrickdappollonio";
          repo = pname;
          rev = "v${version}";
          sha256 = "Q+yEmo1cYbti8P0FjqCRFt2o13QZL/uGBp9iGS+aYVg=";
        };

        modules = ./gomod2nix.toml; # TOML file generated with `gomod2nix generate`

        meta = {
          homepage = "https://github.com/patrickdappollonio/kubectl-slice";
        };
      };
    };
}
