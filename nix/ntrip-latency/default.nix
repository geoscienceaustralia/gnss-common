_: {
  perSystem =
    { inputs', pkgs, ... }:
    {
      packages.ntrip-latency =
        (inputs'.gomod2nix.legacyPackages.buildGoApplication rec {
          pname = "ntrip-latency";
          version = "0.0.6";

          src = pkgs.fetchFromGitHub {
            owner = "go-gnss";
            repo = "rtcm";
            rev = "v${version}";
            sha256 = "V0BOWMUq0PofyQNvRx9sV+0+PqtXs5z3YXbcbxAC7Qk=";
          };

          modules = ./gomod2nix.toml; # TOML file generated with `gomod2nix generate`

          meta = {
            homepage = "https://github.com/go-gnss/rtcm";
          };
        }).overrideAttrs
          (
            _final: prev: {
              installPhase = ''
                ${prev.installPhase}
                mv "$out/bin/ntriplatency" "$out/bin/ntrip-latency"
              '';
            }
          );
    };
}
