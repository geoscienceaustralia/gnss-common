_: {
  perSystem =
    { lib, pkgs, ... }:
    {
      packages.awscli-local = pkgs.python3.pkgs.buildPythonApplication rec {
        pname = "awscli-local";
        version = "0.21.1";
        pyproject = true;

        src = pkgs.fetchPypi {
          inherit pname version;
          hash = "sha256-MaV5Q4qTISfTLCfB29ZWGXoSwxg/rLGK/ovXE+yaqz0=";
        };

        buildInputs = [ pkgs.awscli2 ];

        nativeBuildInputs = [
          pkgs.installShellFiles
          pkgs.python3.pkgs.setuptools
          pkgs.python3.pkgs.wheel
        ];

        propagatedBuildInputs = with pkgs.python3.pkgs; [ localstack-client ];

        passthru.optional-dependencies = with pkgs.python3.pkgs; {
          ver1 = [ awscli ];
        };

        postInstall = ''
          # Install same bash completion as for awscli2
          installShellCompletion --cmd awslocal \
            --bash <(echo "complete -C aws_completer awslocal")
        '';

        meta = with lib; {
          description = "Thin wrapper around the \"aws\" command line interface for use with LocalStack";
          homepage = "https://pypi.org/project/awscli-local/";
          license = licenses.asl20;
          maintainers = [ ];
          mainProgram = "awslocal";
        };
      };
    };
}
