_:
let
  rnxcmp =
    { version, hash }:
    {
      fetchurl,
      gcc,
      glibc,
      stdenv,
    }:
    stdenv.mkDerivation rec {
      pname = "rnxcmp";
      inherit version;

      src = fetchurl {
        # This is same as https://terras.gsi.go.jp/ja/crx2rnx/RNXCMP_${version}_src.tar.gz,
        # which will be removed when superseded by a newer version.
        url = "https://geodesy-third-party-tools.s3-ap-southeast-2.amazonaws.com/RNXCMP_${version}_src.tar.gz";
        inherit hash;
      };

      buildInputs = [ glibc.static ];

      buildPhase = ''
        cd source
        ${gcc}/bin/gcc -static -o rnx2crx rnx2crx.c
        ${gcc}/bin/gcc -static -o crx2rnx crx2rnx.c
      '';

      installPhase = ''
        mkdir -p $out/bin
        cp rnx2crx $out/bin
        cp crx2rnx $out/bin
      '';
    };
in
{
  perSystem =
    { pkgs, ... }:
    {
      packages.rnxcmp-4_1_0 = pkgs.callPackage (rnxcmp {
        version = "4.1.0";
        hash = "sha256-SJ8mCeCDtpuFk5VYh1NCziFneoF0vVHPV1uZZLYId/Y=";
      }) { };
    };
}
