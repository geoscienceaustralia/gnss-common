{ config, dream2nix, ... }:
{
  imports = [
    dream2nix.modules.dream2nix.pip
    #    dream2nix.modules.dream2nix.WIP-python-pyproject
  ];

  deps =
    { nixpkgs, ... }:
    {
      python = nixpkgs.python3;
    };

  name = "localstack";
  version = "3.2.0";

  #  buildPythonPackage = {
  #    format = "pyproject";
  #  };

  pip = {
    requirementsList = [ "${config.name}==${config.version}" ];

    #buildDependencies = {
    #  easy_install = true;
    #};

    overrides = {
      localstack-ext.buildPythonPackage.catchConflicts = false;
    };
  };

  mkDerivation = {
    doCheck = false;
  };
}
