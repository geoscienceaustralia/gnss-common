_: {
  perSystem =
    { config, pkgs, ... }:
    {
      packages.docker-login = pkgs.writeShellApplication {
        name = "docker-login";

        runtimeInputs = [
          config.packages.parameters
          pkgs.docker
        ];

        text = ''
          function usage {
              cat << EOF
          Authenticate against DockerHub using password stored in SSM at /dockerhub/user/<username>/password.

          Usage: docker-login [username]

            username - DockerHub username, default "geodesyarchive"
          EOF
          }

          positional=()
          while (( $# > 0 )); do
              case $1 in
                  -h|--help)
                      usage
                      exit 0
                      ;;
                  *)
                      positional+=("$1")
                      shift
                      ;;
              esac
          done
          set -- "''${positional[@]}"

          if (( $# > 1 )); then
            usage
            exit 1
          fi

          username=''${1:-geodesyarchive}
          password=$(get-parameter --name "/dockerhub/user/$username/password" -d)
          docker login -u "$username" --password-stdin <<< "$password"
        '';
      };
    };
}
