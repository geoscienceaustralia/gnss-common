_: {
  perSystem =
    { pkgs, ... }:
    {
      packages.assume-role = pkgs.writeShellApplication {
        name = "assume-role.sh";

        runtimeInputs = [
          pkgs.awscli2
          pkgs.jq
        ];

        text = ''
          while [[ $# -gt 0 ]]; do
              case $1 in
                  --role-arn)
                      roleArn=$2
                      shift 2
                      ;;
                  --role-session-name)
                      roleSessionName=$2
                      shift 2
                      ;;
                  *)
                      echo "Unknown option: $1"
                      exit 1
                      ;;
              esac
          done

          if [ ! -v roleArn ]; then
            echo "Missing --role-arn"
            exit 1
          fi

          if [ ! -v roleSessionName ]; then
            # Default to user email
            roleSessionName=$(aws sts get-caller-identity | jq -r '.UserId | split (":")[1]')
          fi

          credentials=$(aws sts assume-role \
            --role-arn="$roleArn" \
            --role-session-name="$roleSessionName"  \
            | jq '.Credentials' \
          )

          echo "export AWS_ACCESS_KEY_ID=$(jq <<< "$credentials" '.AccessKeyId' -r)"
          echo "export AWS_SECRET_ACCESS_KEY=$(jq <<< "$credentials" '.SecretAccessKey' -r)"
          echo "export AWS_SESSION_TOKEN=$(jq <<< "$credentials" '.SessionToken' -r)"
        '';
      };
    };
}
