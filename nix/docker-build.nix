{
  docker,
  git,
  writeShellApplication,
  parameters,
}:
writeShellApplication {
  name = "docker-build.sh";

  runtimeInputs = [
    docker
    git
    parameters
  ];

  text = ''
    commitId=$(git rev-parse HEAD)
    tag=''${commitId::7}

    username=geodesyarchive
    push=false

    while [[ $# -gt 0 ]]; do
        case $1 in
            -i|--image)
                image=$2
                shift
                shift
                ;;
            -p|--push)
                push=true
                shift
                ;;
            *)
                echo "Unknown option: $1"
                exit 1
                ;;
        esac
    done

    if [ ! -v image ]; then
      echo "missing --image"
      exit 1
    fi

    docker build -t "$image:$tag" -t "$image:latest" .

    if [[ $push = true ]]; then
        get-parameter --env dev --name "/dockerhub/user/$username/password" -d \
            | docker login -u "$username" --password-stdin

        docker push "$image:$tag"
        docker push "$image:latest"
    fi
  '';
}
