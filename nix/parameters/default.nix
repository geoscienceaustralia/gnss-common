_: {
  perSystem =
    { inputs', ... }:
    {
      packages.parameters = inputs'.gomod2nix.legacyPackages.buildGoApplication {
        name = "parameters";
        src = ../../components/parameters;
        modules = ./gomod2nix.toml; # TOML file generated with `gomod2nix generate`
      };
    };
}
