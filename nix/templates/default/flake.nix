{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    gnss-common.url = "git+https://bitbucket.org/geoscienceaustralia/gnss-common";
    gnss-common.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs =
    { nixpkgs, gnss-common, ... }:
    let
      project = "${PROJECT_NAME}";
      forAllSystems =
        f:
        nixpkgs.lib.genAttrs gnss-common.lib.systems (
          system:
          f {
            pkgs = nixpkgs.legacyPackages.${system}.extend gnss-common.overlays.default;
            inherit system;
          }
        );
    in
    {
      formatter = forAllSystems ({ system, ... }: gnss-common.formatter.${system});

      devShells = forAllSystems (
        { pkgs, ... }:
        rec {
          # Environment for custom pipeline `build-pipelines-docker-image`
          ciImage = pkgs.mkShellNoCC {
            name = "${project}-ci-image";

            packages = [
              # gnss-common overlay
              pkgs.docker-login
            ];

            shellHook = ''
              export PROJECT="${project}"
            '';
          };

          # Environment for all pipelines (except `build-pipelines-docker-image`)
          ci = pkgs.mkShellNoCC {
            name = "${project}-ci";

            inputsFrom = [ ciImage ];

            packages = [
              # gnss-common overlay
              pkgs.init-kubeconfig

              # nixpkgs
              pkgs.bash
            ];
          };

          # Environment for developer workstations
          developer = pkgs.mkShellNoCC {
            name = project;

            inputsFrom = [ ci ];

            packages = [
              # nixpkgs
              pkgs.awscli2
            ];
          };

          default = developer;
        }
      );
    };
  nixConfig.bash-prompt-prefix = "(nix-shell:$name) ";
}
