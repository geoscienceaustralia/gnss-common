#!/usr/bin/env bash

# Build Bitbucket Pipelines docker image containing nix packages as defined
# in nix flake devShell `ci`.

set -euo pipefail

cd "$(dirname "${BASH_SOURCE[0]}")"

image=geoscienceaustralia/$PROJECT-bitbucket-pipelines

commitId=$(git rev-parse HEAD)
tag=${commitId::7}

push=false

while [[ $# -gt 0 ]]; do
    case $1 in
        -p|--push)
            push=true
            shift
            ;;
        *)
            echo "Unknown option: $1"
            exit 1
            ;;
    esac
done

docker build -t "$image:$tag" -t "$image:latest" -f Dockerfile ..

if [[ $push = true ]]; then
    docker-login
    docker push "$image:$tag"
    # NOTE: When experimenting with the image, don't break CI for others by pushing a bad image to
    # `latest`.
    docker push "$image:latest"
fi
