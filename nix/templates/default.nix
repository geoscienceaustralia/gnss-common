{ config, ... }:
{
  flake = {
    templates = {
      project0 = {
        path = ./project0;
        description = "See https://bitbucket.org/geoscienceaustralia/project0-nix.";
        welcomeText = ''
          # To complete the setup

          1. Edit the following files:

            * `flake.nix`
            * `bitbucket-pipelines.yml`
            * `bitbucket-pipelines/bashrc`

            to set `PROJECT_NAME`, `AWS_PROFILE_NAME`, `NONPROD_AWS_ACCOUNT_ID`, and `PROD_AWS_ACCOUNT_ID`, e.g.,:

            ```bash
            $ sed 's/''${PROJECT_NAME}/geodesyml-converter/g' -i flake.nix bitbucket-pipelines.yml
            $ sed 's/''${AWS_PROFILE_NAME}/gnss-metadata/g' -i bitbucket-pipelines/bashrc
            $ sed 's/''${NONPROD_AWS_ACCOUNT_ID}/704132972316/g' -i bitbucket-pipelines/bashrc
            $ sed 's/''${PROD_AWS_ACCOUNT_ID}/484116167126/g' -i bitbucket-pipelines/bashrc
            ```

          2. Make the following files executable:

            * `bitbucket-pipelines/install-nix.sh`
            * `bitbucket-pipelines/docker-build.sh`

            ```bash
            $ chmod +x bitbucket-pipelines/{install-nix.sh,docker-build.sh}
            ```
        '';
      };

      default = config.flake.templates.project0;
    };
  };
}
