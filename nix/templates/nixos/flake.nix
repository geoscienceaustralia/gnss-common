{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    gnss-common.url = "git+https://bitbucket.org/geoscienceaustralia/gnss-common";
    gnss-common.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs =
    { nixpkgs, gnss-common, ... }:
    let
      forAllSystems = f: nixpkgs.lib.genAttrs gnss-common.lib.systems (system: f { inherit system; });
    in
    {
      formatter = forAllSystems ({ system, ... }: gnss-common.formatter.${system});
    };
}
