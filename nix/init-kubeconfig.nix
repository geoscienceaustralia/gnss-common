_: {
  perSystem =
    { pkgs, ... }:
    {
      packages.init-kubeconfig = pkgs.writeShellApplication {
        name = "init-kubeconfig.sh";

        runtimeInputs = [
          pkgs.awscli2
          pkgs.jq
        ];

        text = ''
          ${../components/gnss-eks/scripts/init-kubeconfig.sh} "$@";
        '';
      };
    };
}
