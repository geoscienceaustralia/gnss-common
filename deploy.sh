#!/usr/bin/env bash

# Deploy shared RDS
# Usage: ./deploy.sh components/rds dev

set -euo pipefail

component=$1
env=$2
shift 2

dryRun=false
blueGreenAction=
commonVars=$(pwd)/common.tfvars

positional=()
while [ $# -gt 0 ]; do
    case $1 in
        -d|--dry-run )
            dryRun=true
            shift
            ;;
        --blue-green )
            shift
            blueGreenAction=$1
            shift
            ;;
        * )
            positional+=("$1")
            shift
            ;;
    esac
done
set -- "${positional[@]}"

if [ -f "$component/env.sh" ]; then
    # shellcheck source=/dev/null
    . "$component/env.sh"
fi

cd "$component"

rm -rf plan.tf .terraform

backend=environments/$env/backend.cfg
variables=environments/$env/terraform.tfvars

terraform init -backend-config "$backend"
terraform workspace select "$env" || terraform workspace new "$env"

if ! TF_VAR_region="$(aws configure get region)"; then
    TF_VAR_region=$AWS_DEFAULT_REGION
fi
export TF_VAR_region

# Must use --blue-green <action> with components that use blue-green deployment strategy
currentBlueGreenStackSSM=/$(basename "$component")/current-blue-green-stack-$env
if currentBlueGreenStack=$(aws ssm get-parameter --name "$currentBlueGreenStackSSM" | jq -r .Parameter.Value 2> /dev/null); then
    if [[ -z $blueGreenAction ]]; then
        echo "You must use --blue-green option"
        exit 1
    fi
fi

# Terraform CLI options to use when doing blue-green deployments
blueGreenTerraformTarget=()

if [[ ${blueGreenAction:-} != "" ]]; then
    # Initially, current stack will be none and next stack will be blue. For subsequent deployments,
    # if current stack is blue, next stack will be green and vice versa.
    if [[ -z $currentBlueGreenStack ]]; then
        nextBlueGreenStack=blue
    else
        nextBlueGreenStack=$(if [[ $currentBlueGreenStack == blue ]]; then echo green; else echo blue; fi)
    fi
    case $blueGreenAction in
        # show current and next stack labels
        show )
            echo "Current stack is ${currentBlueGreenStack:-none}"
            echo "Next stack will be $nextBlueGreenStack"
            exit 0
            ;;
        # deploy next stack
        deploy )
            blueGreenTerraformTarget=(-var next_stack="$nextBlueGreenStack" -target "module.$nextBlueGreenStack")
            ;;
        # activate next stack and deactivate current stack
        switch )
            blueGreenTerraformTarget=(-var next_stack="$nextBlueGreenStack" -var current_stack="$currentBlueGreenStack" -target module.switch)
            ;;
        # delete inactive stack
        destroy )
            blueGreenTerraformTarget=(-var next_stack="$currentBlueGreenStack" -target "module.$nextBlueGreenStack")
            ;;
        * )
            echo "Unrecognised argument to --blue-green"
            exit 1
        ;;
    esac
fi

terraform plan -input=false -var-file="$commonVars" -var-file="$variables" -out plan.tf "${blueGreenTerraformTarget[@]}" "$@"

if [[ $dryRun = false ]]; then
    terraform apply -refresh-only -auto-approve plan.tf
fi

rm -f plan.tf
