Components which are shared between GNSS Systems.

Individual components can be infracode, terraform modules, library
code, applications, etc. - they are organized under the `components/`
directory for the sake of keeping the top level directory clean.

See `components/<name>/README.md` for component specific information.

# Developer and CI Build Environments

This project uses [Nix](https://nixos.org/nix) to manage build environments
for both developer workstations and Bitbucket Pipelines.

1) Install Nix

```bash
$ curl --proto '=https' --tlsv1.2 -sSf -L https://install.determinate.systems/nix | sh -s -- install
```

2) Enter nix shell to download and activate required software as specified
in devShell `developer` in `flake.nix`.

```bash
$ nix develop
```

## Flake Outputs

List available flake outputs with `nix flake show`. You can also use flake outputs outside the
gnss-common nix shell.

```bash
$ nix run git+https://bitbucket.org/geoscienceaustralia/gnss-common#init-kubeconfig

# You can add gnss-common to your nix registry, `~/.config/nix/registry.json`.

$ nix registry add gnss-common git+https://bitbucket.org/geoscienceaustralia/gnss-common
$ # nix registry add gnss-common /path/to/gnss-common

$ nix run gnss-common#init-kubeconfig -- dev --cached
```

## Flake Structure

Besides [flakes.nix](./flakes.nix), all nix code is in [nix/](./nix).

The aim is for most `.nix` files to be flake modules as defined by [flake-parts](https://flake.parts).
This approach gives access to flake inputs and configuration when defining flake outputs (packages,
shells, etc.), without resorting to use of nixpkgs overlays or adhoc plumbing -- the latter being
standardised by flake-parts.

Here is an outline of a flake module, with all parameters shown.

```nix
toplevel@{
  lib,        # TODO: Reference to inputs.nixpkgs.lib?
  options,    # Option declarations for top-level flake module
  config,     # Option definitions for top-level flake module

  self,       # This flake
  inputs,     # Flake inputs
}:
{
  perSystem =
    {
        lib,                 # TODO: How is this different from toplevel.lib?
        options,             # Option declarations for perSystem submodule
        config,              # Option definitions for perSystem submodule

        self',               # This flake with system pre-selected

        inputs',             # Flake inputs with system pre-selected

        pkgs,               # Reference to inputs.nixpkgs.legacyPackages.${system},
                             # configured in [nix/nixpkgs.nix](./nix/nixpkgs.nix)

        system,              # System parameter, e.g., "x86_64-linux"

        getSystem,           # See https://flake.parts/module-arguments
        moduleWithSystem,
        withSystem,
    }:
    {
        # Flake outputs that are system dependent
        packages = ...;
        devShells = ...;
        ...
    };

  flake = {
    # Flake outputs that are system independent
    lib = ...;
    nixosModules = ...;
    templates = ...;
    ...
  };
}
```

Nix code is formatted using `nixfmt-rfc-style`.

```bash
# Format all nix code in the repository 
$ nix fmt 

# or just one file
$ nix fmt flake.nix
```

## Upstream Dependencies

Edit `flake.nix` to add or remove dependencies. Use `nix flake lock --update-input <input>`
or `nix flake update` to update some or all flake inputs. Regularly cache changes to project
dependencies by running custom pipeline `build-pipelines-docker-image`.

## Downstream Dependencies

You can equip another project with a similar nix setup, and with `gnss-common` pre-configured as a
dependency.

```bash
$ nix flake init --template gnss-common # provided gnss-common is in your `nix registry`
```

See [project0-nix](https://bitbucket.org/geoscienceaustralia/project0-nix) for an example.

# Deployments

All deployable components currently use deploy.sh (but can be use their
own deploy scripts if necessary). Components deployed this way must
include all variables mentioned in common.tfvar.

Default branch performs dev dry-run deployments for all components (or
dry-run prod deployments for prod only components).

Master branch performs dev deployment and prod dry-run deployment for
all components.

`component.<name>` branches should be used for prod deployments.

