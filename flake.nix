{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

    # This is not used at the moment, but is useful to have around.
    # For usage, see `./nix/nixpkgs.nix`.
    # nixpkgs-stable.url = "github:NixOS/nixpkgs/nixos-24.11";

    dream2nix.url = "github:nix-community/dream2nix";
    dream2nix.inputs.nixpkgs.follows = "nixpkgs";

    gomod2nix.url = "github:nix-community/gomod2nix";
    gomod2nix.inputs.nixpkgs.follows = "nixpkgs";

    git-hooks.url = "github:cachix/git-hooks.nix";
    git-hooks.inputs.nixpkgs.follows = "nixpkgs";

    flake-parts.url = "github:hercules-ci/flake-parts";

    # TODO: Remove once https://github.com/juspay/omnix/issues/204 is done.
    omnix.url = "github:juspay/omnix";
    omnix.inputs.nixpkgs.follows = "nixpkgs";
    omnix.inputs.flake-parts.follows = "flake-parts";
  };

  outputs =
    inputs@{ flake-parts, ... }:
    flake-parts.lib.mkFlake { inherit inputs; } (
      { flake-parts-lib, ... }:
      let
        inherit (flake-parts-lib) importApply;
        inherit (builtins) attrValues removeAttrs;

        # Modules in ./nix/flake-modules/ are imported in a special way, because they are used as
        # both module imports and flake outputs. See https://flake.parts/dogfood-a-reusable-module.
        flakeModules = {
          preCommit = importApply ./nix/flake-modules/pre-commit.nix { inherit inputs; };
          all = {
            imports = attrValues (removeAttrs flakeModules [ "all" ]);
          };
        };
      in
      {
        flake = {
          name = "gnss-common";

          # Export all modules in ./nix/flake-modules/
          inherit flakeModules;
        };

        systems = [
          "x86_64-linux"
          "x86_64-darwin"
          "aarch64-darwin"
        ];

        imports = [
          # Import all modules in ./nix/flake-modules/
          flakeModules.all

          ./nix/assume-role.nix
          ./nix/awscli-local.nix
          ./nix/docker-login.nix
          ./nix/gnss-common-reader-role.nix
          ./nix/init-kubeconfig.nix
          ./nix/kubectl-slice
          ./nix/lib.nix
          ./nix/list-vpc-cidr-blocks
          ./nix/nixpkgs.nix
          ./nix/ntrip-latency
          ./nix/parameters
          ./nix/rnxcmp.nix
          ./nix/shells.nix
          ./nix/templates
          ./nix/terraform-local
        ];
      }
    );

  nixConfig.bash-prompt-prefix = "(nix-shell:$name) ";
}
