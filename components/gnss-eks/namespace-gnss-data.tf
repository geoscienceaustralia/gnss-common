module "namespace_gnss_data" {
  source              = "./modules/namespace"
  cluster_id          = local.cluster_id
  aws_role_policy_arn = aws_iam_policy.eks_user.arn
  namespace           = "gnss-data"
  admin_aws_nonprod_principals = [
    "arn:aws:iam::023072794443:root", # gnss-common-nonprod
    "arn:aws:iam::688660191997:root", # gnss-data-prod
  ]
  admin_aws_prod_principals = [
    "arn:aws:iam::334594953176:root", # gnss-common-prod
    "arn:aws:iam::623223935732:root", # gnss-data-nonprod
  ]
}
