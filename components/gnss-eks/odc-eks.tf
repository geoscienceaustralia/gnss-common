locals {
  owner   = "gnss-informatics"
  system  = "gnss-eks"
  release = "2"

  cluster_id = "${local.system}-${local.release}-${terraform.workspace}"
  vpc_cidr   = "10.49.0.0/16"
}

data "aws_ssm_parameter" "docker_config_json" {
  name = "/dockerhub/user/geodesyarchive/docker-config-json"
}

module "eks" {
  source = "github.com/lbodor/datacube-k8s-eks//odc_eks?ref=1cdcd37"

  providers = {
    aws.us-east-1 = aws.us-east-1
  }

  # Cluster config
  region          = var.region
  cluster_id      = local.cluster_id
  cluster_version = var.k8s_version

  # Default Tags
  owner       = local.owner
  namespace   = local.system
  environment = terraform.workspace

  # VPC config
  vpc_cidr             = local.vpc_cidr
  public_subnet_cidrs  = ["10.49.0.0/24", "10.49.1.0/24", "10.49.2.0/24"]
  private_subnet_cidrs = ["10.49.10.0/24", "10.49.11.0/24", "10.49.12.0/24"]

  domain_name = aws_route53_zone.eks.name

  # ACM - used by ALB
  create_certificate = true

  # Nodes
  ami_image_id                 = var.node_ami
  default_worker_instance_type = var.node_instance_type
  spot_nodes_enabled           = false
  min_spot_nodes               = 0
  max_spot_nodes               = 3
  max_spot_price               = "0.40"
  min_nodes                    = var.min_nodes
  desired_nodes                = var.desired_nodes
  max_nodes                    = var.max_nodes
  volume_size                  = 100
  spot_volume_size             = 100

  metadata_options = {
    # This is the default value in the created resource, but without setting it here, terraform will always recreate the launch template
    http_endpoint = "enabled"
  }

  extra_userdata = templatefile("${path.module}/node-userdata.sh", {
    docker_config_json = data.aws_ssm_parameter.docker_config_json.value
  })

  cf_enable  = false
  waf_enable = false

  enabled_cluster_log_types       = ["api", "audit"]
  enable_custom_cluster_log_group = true
  log_retention_period            = 30
}

# TODO: enable later
# data "aws_acm_certificate" "domain_cert" {
#   count       = local.create_certificate ? 0 : 1
#   domain      = "*.${local.domain_name}"
#   most_recent = true
# }
