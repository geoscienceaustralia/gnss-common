module "namespace_southpan" {
  source              = "./modules/namespace"
  cluster_id          = local.cluster_id
  aws_role_policy_arn = aws_iam_policy.eks_user.arn
  namespace           = "southpan"
  admin_aws_nonprod_principals = [
    "arn:aws:iam::023072794443:root", # gnss-common-nonprod
  ]
  admin_aws_prod_principals = [
    "arn:aws:iam::334594953176:root", # gnss-common-prod
  ]
}
