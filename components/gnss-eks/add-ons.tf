resource "aws_eks_addon" "coredns" {
  cluster_name      = local.cluster_id
  addon_name        = "coredns"
  addon_version     = var.managed_add_on_versions.coredns
  resolve_conflicts = "OVERWRITE"
}

resource "aws_eks_addon" "kube_proxy" {
  cluster_name      = local.cluster_id
  addon_name        = "kube-proxy"
  addon_version     = var.managed_add_on_versions.kube_proxy
  resolve_conflicts = "OVERWRITE"
}

// TODO: Migrate VPC CNI add-on to use IAM roles for service accounts
// arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy is still attached to node policy in datacube-k8s-eks
// See https://docs.amazonaws.cn/en_us/eks/latest/userguide/cni-iam-role.html
// See https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eks_addon
resource "aws_eks_addon" "vpc_cni" {
  cluster_name      = local.cluster_id
  addon_name        = "vpc-cni"
  addon_version     = var.managed_add_on_versions.vpc_cni
  resolve_conflicts = "OVERWRITE"
}

module "ebs_csi_driver" {
  source                     = "./modules/ebs-csi-driver"
  add_on_version             = var.managed_add_on_versions.ebs_csi_driver
  cluster_id                 = local.cluster_id
  cluster_oidc_provider_name = local.cluster_oidc_provider_name
  cluster_oidc_provider_arn  = local.cluster_oidc_provider_arn

  depends_on = [
    # The installation instructions state that external-snapshotter needs to be
    # installed before ebc-csi-driver (not sure why).
    module.external_snapshotter,
  ]
}
