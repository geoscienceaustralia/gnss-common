terraform {
  backend "s3" {
    encrypt = true
  }
}

provider "kubernetes" {
  config_path = "~/.kube/config"
}
