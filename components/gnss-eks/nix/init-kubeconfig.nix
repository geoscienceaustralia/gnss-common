{
  stdenv,
  lib,
  awscli2,
  bash,
  git,
  jq,
  makeWrapper,
  openssh,
  terraform_1,
  gnss-common-reader-role,
  toybox,
}:

let
  runtimeDependencies = [
    awscli2
    bash
    git
    gnss-common-reader-role
    jq
    openssh
    terraform_1
    toybox
  ];

in

stdenv.mkDerivation rec {
  name = "init-kubeconfig";
  src = ./..;

  nativeBuildInputs = [ makeWrapper ];

  phases = "installPhase";

  installPhase = ''
    mkdir -p $out/bin

    cp -r $src $out/dist
    chmod -R u+w $out/dist
    chmod +x $out/dist/scripts/${name}.sh

    wrapProgram $out/dist/scripts/${name}.sh \
      --set PATH /usr/bin:${lib.makeBinPath runtimeDependencies}

    # TODO: Migrate everyone from `init-kubeconfig.sh` to `init-kubeconfig`.
    ln -s $out/dist/scripts/${name}.sh $out/bin/${name}.sh
    ln -s $out/dist/scripts/${name}.sh $out/bin/${name}
  '';
}
