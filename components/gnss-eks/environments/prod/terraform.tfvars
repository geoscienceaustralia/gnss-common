data_gnss_ga_gov_au_external_dns_role_arn     = "arn:aws:iam::623223935732:role/gnss-data-prod-external-dns"
metadata_gnss_ga_gov_au_external_dns_role_arn = "arn:aws:iam::484116167126:role/gnss-metadata-prod-external-dns"

node_instance_type = "m5.4xlarge"
desired_nodes      = 4
max_nodes          = 5
min_nodes          = 4

developer_principals = [
  "arn:aws:iam::623223935732:root", # geodesy-operations-prod
  "arn:aws:iam::334594953176:root", # gnss-common-prod
]

readonly_principals = [
  "arn:aws:iam::623223935732:root", # geodesy-operations-prod
  "arn:aws:iam::334594953176:root", # gnss-common-prod 
]

gnss_stream_developer_principals = [
  "arn:aws:iam::623223935732:root", # geodesy-operations-prod
]

loki_chunks_expiration_days = 180
