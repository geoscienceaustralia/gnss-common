data_gnss_ga_gov_au_external_dns_role_arn     = "arn:aws:iam::688660191997:role/gnss-data-test-external-dns"
metadata_gnss_ga_gov_au_external_dns_role_arn = "arn:aws:iam::704132972316:role/gnss-metadata-test-external-dns"

desired_nodes = 3
max_nodes     = 5

developer_principals = [
  "arn:aws:iam::023072794443:root", # gnss-common-nonprod
  "arn:aws:iam::604917042985:root", # ga-aws-gpsprocess (non-prod)
  "arn:aws:iam::688660191997:root", # geodesy-operations (non-prod)
]

readonly_principals = [
  "arn:aws:iam::023072794443:root", # gnss-common-nonprod
  "arn:aws:iam::604917042985:root", # ga-aws-gpsprocess (non-prod)
  "arn:aws:iam::688660191997:root", # geodesy-operations (non-prod)
]

gnss_stream_developer_principals = [
  "arn:aws:iam::023072794443:root", # gnss-common-nonprod
]
