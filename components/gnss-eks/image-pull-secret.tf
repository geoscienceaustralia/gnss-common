locals {
  manifest = "${path.module}/image-pull-secret.yaml"
}

resource "kubernetes_manifest" "externalsecret_image_pull_secret" {
  manifest = {
    apiVersion = "external-secrets.io/v1beta1"
    kind       = "ExternalSecret"
    metadata = {
      name      = "image-pull-secret"
      namespace = "default"
    }
    spec = {
      data = [
        {
          remoteRef = {
            key = "/dockerhub/user/geodesyarchive/docker-config-json"
          }
          secretKey = ".dockerconfigjson"
        },
      ]
      secretStoreRef = {
        kind = "ClusterSecretStore"
        name = "parameters"
      }
      target = {
        name = "image-pull-secret"
        template = {
          type = "kubernetes.io/dockerconfigjson"
        }
      }
    }
  }
}

resource "null_resource" "patch_default_service_account" {
  triggers = {
    cluster_id = local.cluster_id
  }

  provisioner "local-exec" {
    command = "kubectl patch serviceaccount default -p '{\"imagePullSecrets\": [{\"name\": \"${kubernetes_manifest.externalsecret_image_pull_secret.manifest.metadata.name}\"}]}'"
  }

  provisioner "local-exec" {
    when    = destroy
    command = "kubectl patch serviceaccount default -p '{\"imagePullSecrets\": null}'"
  }
}
