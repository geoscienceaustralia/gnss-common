module "odc_k8s" {
  # TODO: Update
  source = "github.com/opendatacube/datacube-k8s-eks//odc_k8s?ref=ea02a83"

  region     = var.region
  cluster_id = local.cluster_id

  # Default Tags
  owner       = local.owner
  namespace   = local.system
  environment = terraform.workspace

  role_config_template = data.template_file.map_role_config.rendered

  # Database
  store_db_creds = false

  flux_enabled      = false
  fluxcloud_enabled = false

  # Cloudwatch Log Group - for fluentd
  # Note: Creating a Cloudwatch Log Group outside a cluster provisioning so log-group can be retain even we destroy the full cluster infrastructure.
  cloudwatch_logs_enabled = false

  depends_on = [
    null_resource.kubeconfig,
  ]
}

data "aws_iam_role" "ga_admin" {
  name = "GAAdmin"
}

data "aws_iam_roles" "ga_super_developer" {
  name_regex = "AWSReservedSSO_GASuperDeveloper_[0-9a-f]{16}"
}

data "aws_iam_roles" "ga_read_only" {
  name_regex = "AWSReservedSSO_GAReadOnly_[0-9a-f]{16}"
}

data "template_file" "map_role_config" {
  template = <<-EOF
    - rolearn: ${module.eks.node_role_arn}
      username: system:node:{{EC2PrivateDNSName}}
      groups:
        - system:bootstrappers
        - system:nodes
    - rolearn: ${data.aws_iam_role.ga_admin.arn}
      username: cluster-admin
      groups:
        - system:masters
    - rolearn: arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/${one(data.aws_iam_roles.ga_super_developer.names)}
      username: cluster-admin
      groups:
        - system:masters
    - rolearn: arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/bitbucket-pipelines
      username: cluster-admin
      groups:
        - system:masters
    - rolearn: ${aws_iam_role.admin.arn}
      username: cluster-admin
      groups:
        - system:masters
    - rolearn: arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/${one(data.aws_iam_roles.ga_read_only.names)}
      username: read-only
      groups:
        - ${local.k8s_read_only_group}
    - rolearn: ${aws_iam_role.read_only.arn}
      username: read-only
      groups:
        - ${local.k8s_read_only_group}
    - rolearn: ${module.namespace_ginan_ops.aws_admin_role_arn}
      username: ginan-ops
      groups:
        - ${module.namespace_ginan_ops.k8s_admin_group}
        - ${local.k8s_read_only_group}
    - rolearn: ${module.namespace_gnss_stream.aws_admin_role_arn}
      username: gnss-stream
      groups:
        - ${module.namespace_gnss_stream.k8s_admin_group}
        - ${local.k8s_read_only_group}
    - rolearn: ${module.namespace_gnss_data.aws_admin_role_arn}
      username: gnss-data
      groups:
        - ${module.namespace_gnss_data.k8s_admin_group}
        - ${local.k8s_read_only_group}
    - rolearn: ${module.namespace_gnss_metadata.aws_admin_role_arn}
      username: gnss-data
      groups:
        - ${module.namespace_gnss_metadata.k8s_admin_group}
        - ${local.k8s_read_only_group}
    - rolearn: ${module.namespace_gnss_monitor.aws_admin_role_arn}
      username: gnss-monitor
      groups:
        - ${module.namespace_gnss_monitor.k8s_admin_group}
        - ${local.k8s_read_only_group}
    - rolearn: ${module.namespace_southpan.aws_admin_role_arn}
      username: southpan
      groups:
        - ${module.namespace_southpan.k8s_admin_group}
        - ${local.k8s_read_only_group}
    EOF
}
