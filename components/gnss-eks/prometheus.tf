resource "kubernetes_namespace" "prometheus" {
  metadata {
    name = "prometheus"
  }
}

locals {
  gnss_users_id     = data.terraform_remote_state.cognito.outputs.gnss_user_pool.id
  gnss_users_domain = data.terraform_remote_state.cognito.outputs.gnss_user_pool.domain
  gnss_users_client = data.terraform_remote_state.cognito.outputs.gnss_users_prometheus_grafana_client

  gnss_users_prod_id      = data.terraform_remote_state.cognito_prod.outputs.gnss_user_pool.id
  gnss_users_prod_domain  = data.terraform_remote_state.cognito_prod.outputs.gnss_user_pool.domain
  gnss_users_prod_clients = data.terraform_remote_state.cognito_prod.outputs.gnss_users_prod_prometheus_grafana_clients
}

# Create groups grafana-$env-admin,editor in all environments, but create dev
# and test groups in prod also. We don't need a viewer group, because viewer
# is the minimum level of access, see https://github.com/grafana/grafana/pull/21038
resource "aws_cognito_user_group" "grafana_admin" {
  name         = "grafana-${terraform.workspace}-admin"
  user_pool_id = local.gnss_users_id
}

resource "aws_cognito_user_group" "grafana_editor" {
  name         = "grafana-${terraform.workspace}-editor"
  user_pool_id = local.gnss_users_id
}

resource "aws_cognito_user_group" "grafana_dev_admin" {
  count        = terraform.workspace == "prod" ? 1 : 0
  name         = "grafana-dev-admin"
  user_pool_id = local.gnss_users_prod_id
}

resource "aws_cognito_user_group" "grafana_dev_editor" {
  count        = terraform.workspace == "prod" ? 1 : 0
  name         = "grafana-dev-editor"
  user_pool_id = local.gnss_users_prod_id
}

resource "aws_cognito_user_group" "grafana_test_admin" {
  count        = terraform.workspace == "prod" ? 1 : 0
  name         = "grafana-test-admin"
  user_pool_id = local.gnss_users_prod_id
}

resource "aws_cognito_user_group" "grafana_test_editor" {
  count        = terraform.workspace == "prod" ? 1 : 0
  name         = "grafana-test-editor"
  user_pool_id = local.gnss_users_prod_id
}

resource "kubernetes_secret" "prometheus" {
  metadata {
    name      = "prometheus"
    namespace = kubernetes_namespace.prometheus.metadata[0].name
  }

  data = {
    "values.yaml" = templatefile("${path.module}/prometheus.yaml",
      {
        region          = var.region
        env             = terraform.workspace
        certificate_arn = module.eks.certificate_arn
        domain_name     = aws_route53_zone.eks.name

        admin_group  = aws_cognito_user_group.grafana_admin.name
        editor_group = aws_cognito_user_group.grafana_editor.name

        cognito = [
          {
            enabled          = true
            user_pool_domain = local.gnss_users_domain
            client_id        = local.gnss_users_client.id
            client_secret    = local.gnss_users_client.secret
          },
          {
            # Grafana doesn't actually allow multiple generic oauth providers, only the last entry is used.
            # Set enabled to false to enable the above entry. See https://github.com/grafana/grafana/issues/10209
            enabled          = terraform.workspace != "prod"
            user_pool_domain = local.gnss_users_prod_domain
            client_id        = try(local.gnss_users_prod_clients[terraform.workspace].id, null) # no value for terraform.workspace == prod
            client_secret    = try(local.gnss_users_prod_clients[terraform.workspace].secret, null)
          },
        ]
      }
    )
  }

  type = "Opaque"
}

data "aws_ssm_parameter" "slack_api_url" {
  name = "/slack/webhook/gnss-informatics-notification/url"
}

data "template_file" "alertmanager" {
  template = file("${path.module}/prometheus-alertmanager.yaml")
  vars = {
    slack_api_url = data.aws_ssm_parameter.slack_api_url.value
    domain_name   = aws_route53_zone.eks.name
    env           = terraform.workspace
  }
}

resource "kubernetes_secret" "alertmanager" {
  metadata {
    name      = "alertmanager"
    namespace = kubernetes_namespace.prometheus.metadata[0].name
  }

  data = {
    "alertmanager.yaml" = data.template_file.alertmanager.rendered
  }

  type = "Opaque"
}
