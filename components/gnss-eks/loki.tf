locals {
  loki_index  = "${local.cluster_id}-loki-index"
  loki_chunks = "${local.cluster_id}-loki-chunks"
}

resource "aws_dynamodb_table" "loki_index" {
  name           = local.loki_index
  billing_mode   = "PROVISIONED"
  read_capacity  = 5
  write_capacity = 5
  hash_key       = "h"
  range_key      = "r"

  attribute {
    name = "h"
    type = "S"
  }

  attribute {
    name = "r"
    type = "B"
  }

  lifecycle {
    # These are required on first creation, then managed directly by Loki
    ignore_changes = [
      read_capacity,
      write_capacity
    ]
  }
}

resource "aws_s3_bucket" "loki_chunks" {
  bucket = local.loki_chunks
  acl    = "private"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_s3_bucket_public_access_block" "loki_chunks" {
  bucket = aws_s3_bucket.loki_chunks.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_lifecycle_configuration" "loki_chunks" {
  bucket = aws_s3_bucket.loki_chunks.id

  rule {
    id     = "delete-old-objects"
    status = "Enabled"

    filter {}

    expiration {
      days = var.loki_chunks_expiration_days
    }
  }
}

data "aws_iam_policy_document" "loki" {
  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]
    effect  = "Allow"

    principals {
      identifiers = [local.cluster_oidc_provider_arn]
      type        = "Federated"
    }

    condition {
      test     = "StringEquals"
      variable = "${local.cluster_oidc_provider_name}:sub"
      values   = ["system:serviceaccount:loki:loki"]
    }
  }
}

resource "aws_iam_role" "loki" {
  name = "${local.cluster_id}-loki"
  path = "/"

  assume_role_policy = data.aws_iam_policy_document.loki.json
}

resource "aws_iam_role_policy" "loki" {
  role   = aws_iam_role.loki.id
  policy = <<-EOF
    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Effect": "Allow",
          "Action": [
              "S3:ListBucket"
          ],
          "Resource": [
            "${aws_s3_bucket.loki_chunks.arn}"
          ]
        },
        {
          "Effect": "Allow",
          "Action": [
              "S3:GetObject",
              "S3:PutObject"
          ],
          "Resource": [
            "${aws_s3_bucket.loki_chunks.arn}/*"
          ]
        },
        {
          "Effect": "Allow",
          "Action": [
              "dynamodb:BatchGetItem",
              "dynamodb:BatchWriteItem",
              "dynamodb:DeleteItem",
              "dynamodb:DescribeTable",
              "dynamodb:GetItem",
              "dynamodb:ListTagsOfResource",
              "dynamodb:PutItem",
              "dynamodb:Query",
              "dynamodb:TagResource",
              "dynamodb:UntagResource",
              "dynamodb:UpdateItem",
              "dynamodb:UpdateTable",
              "dynamodb:CreateTable",
              "dynamodb:DeleteTable"
          ],
          "Resource": [
            "${aws_dynamodb_table.loki_index.arn}*"
          ]
        },
        {
          "Effect": "Allow",
          "Action": [
              "dynamodb:ListTables"
          ],
          "Resource": [
            "${trimsuffix(aws_dynamodb_table.loki_index.arn, aws_dynamodb_table.loki_index.name)}*"
          ]
        }
      ]
    }
    EOF
}

data "template_file" "loki" {
  template = file("${path.module}/loki.yaml")
  vars = {
    region              = var.region
    loki_index          = local.loki_index
    loki_chunks         = local.loki_chunks
    service_account_arn = aws_iam_role.loki.arn
  }
}

resource "kubernetes_namespace" "loki" {
  metadata {
    name = "loki"
  }
}

resource "kubernetes_secret" "loki" {
  metadata {
    name      = "loki-config"
    namespace = kubernetes_namespace.loki.metadata[0].name
  }

  data = {
    "values.yaml" = data.template_file.loki.rendered
  }

  type = "Opaque"
}
