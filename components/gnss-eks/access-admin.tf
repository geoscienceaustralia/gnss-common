# Create
# * AWS role `eks-admin.${cluster_id}` TODO: Rename to `${cluster_id}-admin`
#
# See ./odc-k8s.tf for `aws-auth` config map configuration

data "aws_iam_policy_document" "admin_trust_policy" {
  statement {
    sid     = "1"
    actions = ["sts:AssumeRole"]
    principals {
      type = "AWS"
      # TODO: restrict admin access
      identifiers = terraform.workspace == "prod" ? var.prod_aws_principals : var.nonprod_aws_principals
    }
    condition {
      test     = "Bool"
      variable = "aws:SecureTransport"
      values   = ["true"]
    }
  }
}

resource "aws_iam_role" "admin" {
  name                 = "eks-admin.${local.cluster_id}"
  assume_role_policy   = data.aws_iam_policy_document.admin_trust_policy.json
  max_session_duration = "28800"

  # https://eksctl.io/usage/minimum-iam-policies/
  inline_policy {
    name = "eks-all-access"
    policy = jsonencode({
      Version = "2012-10-17",
      Statement = [
        {
          Effect   = "Allow",
          Action   = "eks:*",
          Resource = "*"
        },
        {
          Action = [
            "ssm:GetParameter",
            "ssm:GetParameters"
          ],
          Resource = [
            "arn:aws:ssm:*:${data.aws_caller_identity.current.account_id}:parameter/aws/*",
            "arn:aws:ssm:*::parameter/aws/*"
          ],
          Effect = "Allow"
        },
        {
          Action = [
            "kms:CreateGrant",
            "kms:DescribeKey"
          ],
          Resource = "*",
          Effect   = "Allow"
        },
        {
          Action = [
            "logs:PutRetentionPolicy"
          ],
          Resource = "*",
          Effect   = "Allow"
        }
      ]
    })
  }

  # https://eksctl.io/usage/minimum-iam-policies/
  inline_policy {
    name = "iam-limited-access"

    policy = jsonencode({
      Version = "2012-10-17",
      Statement = [
        {
          Effect = "Allow",
          Action = [
            "iam:CreateInstanceProfile",
            "iam:DeleteInstanceProfile",
            "iam:GetInstanceProfile",
            "iam:RemoveRoleFromInstanceProfile",
            "iam:GetRole",
            "iam:CreateRole",
            "iam:DeleteRole",
            "iam:AttachRolePolicy",
            "iam:PutRolePolicy",
            "iam:ListInstanceProfiles",
            "iam:AddRoleToInstanceProfile",
            "iam:ListInstanceProfilesForRole",
            "iam:PassRole",
            "iam:DetachRolePolicy",
            "iam:DeleteRolePolicy",
            "iam:GetRolePolicy",
            "iam:GetOpenIDConnectProvider",
            "iam:CreateOpenIDConnectProvider",
            "iam:DeleteOpenIDConnectProvider",
            "iam:TagOpenIDConnectProvider",
            "iam:ListAttachedRolePolicies",
            "iam:TagRole"
          ],
          Resource = [
            "arn:aws:iam::${data.aws_caller_identity.current.account_id}:instance-profile/eksctl-*",
            "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/eksctl-*",
            "arn:aws:iam::${data.aws_caller_identity.current.account_id}:oidc-provider/*",
            "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/aws-service-role/eks-nodegroup.amazonaws.com/AWSServiceRoleForAmazonEKSNodegroup",
            "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/eksctl-managed-*"
          ]
        },
        {
          Effect = "Allow",
          Action = [
            "iam:GetRole"
          ],
          Resource = [
            "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/*"
          ]
        },
        {
          Effect = "Allow",
          Action = [
            "iam:CreateServiceLinkedRole"
          ],
          Resource = "*",
          Condition = {
            StringEquals = {
              "iam:AWSServiceName" : [
                "eks.amazonaws.com",
                "eks-nodegroup.amazonaws.com",
                "eks-fargate.amazonaws.com"
              ]
            }
          }
        }
      ]
    })
  }
}

resource "aws_iam_role_policy_attachment" "admin" {
  role       = aws_iam_role.admin.name
  policy_arn = aws_iam_policy.eks_user.arn
}

data "aws_iam_policy" "ec2_full_access" {
  name = "AmazonEC2FullAccess"
}

# https://eksctl.io/usage/minimum-iam-policies/
resource "aws_iam_role_policy_attachment" "ec2_full_access" {
  role       = aws_iam_role.admin.name
  policy_arn = data.aws_iam_policy.ec2_full_access.arn
}

data "aws_iam_policy" "cloud_formation_full_access" {
  name = "AWSCloudFormationFullAccess"
}

# https://eksctl.io/usage/minimum-iam-policies/
resource "aws_iam_role_policy_attachment" "cloud_formation_full_access" {
  role       = aws_iam_role.admin.name
  policy_arn = data.aws_iam_policy.cloud_formation_full_access.arn
}
