resource "null_resource" "kubeconfig" {

  provisioner "local-exec" {
    command = "aws eks update-kubeconfig --region ${var.region} --name ${module.eks.cluster_id} && chmod 600 ~/.kube/config"
  }

  triggers = {
    always_run = timestamp()
  }

  depends_on = [
    module.eks,
    aws_iam_role_policy_attachment.admin
  ]
}
