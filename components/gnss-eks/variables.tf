variable "region" {
}

variable "k8s_version" {
  type    = string
  default = "1.30"
}

# See https://docs.aws.amazon.com/eks/latest/userguide/managing-add-ons.html
# eksctl utils describe-addon-versions --kubernetes-version <k8s-version> | grep AddonName
# eksctl utils describe-addon-versions --kubernetes-version <k8s-version> --name <name-of-add-on> | grep AddonVersion
variable "managed_add_on_versions" {
  type = object({
    coredns        = string
    kube_proxy     = string
    vpc_cni        = string
    ebs_csi_driver = string
  })

  default = {
    coredns        = "v1.11.4-eksbuild.2"
    kube_proxy     = "v1.30.7-eksbuild.2"
    vpc_cni        = "v1.19.2-eksbuild.1"
    ebs_csi_driver = "v1.38.1-eksbuild.1"
  }
}

variable "node_ami" {
  type = string
  # aws ec2 describe-images --filters Name=name,Values=amazon-eks-node-${kubernetes_version}-v* --query "sort_by(Images, &CreationDate)[-1].ImageId" --output text
  default = "ami-0c112c83b91b84022"
}

variable "nonprod_aws_principals" {
  type = list(string)
}

variable "developer_principals" {
  type = list(string)
}

variable "gnss_stream_developer_principals" {
  type = list(string)
}

variable "prod_aws_principals" {
  type = list(string)
}

variable "node_instance_type" {
  type    = string
  default = "t3.large"
}

variable "data_gnss_ga_gov_au_external_dns_role_arn" {
  type = string
}

variable "metadata_gnss_ga_gov_au_external_dns_role_arn" {
  type = string
}

variable "gnss_common_reader_role_prod_arn" {
  type = string
}

variable "min_nodes" {
  type    = number
  default = 2
}

variable "desired_nodes" {
  type    = number
  default = 3
}

variable "max_nodes" {
  type    = number
  default = 4
}

variable "loki_chunks_expiration_days" {
  type    = number
  default = 90
}
