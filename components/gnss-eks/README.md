# GNSS-EKS

GNSS Informatics maintains the following three EKS clusters:

| Environment | AWS Account                | ARN                                                               |
|-------------|----------------------------|-------------------------------------------------------------------|
| Dev         | ga-aws-gnss-common-nonprod | `arn:aws:eks:ap-southeast-2:023072794443:cluster/gnss-eks-2-dev`  |
| Test        | ga-aws-gnss-common-nonprod | `arn:aws:eks:ap-southeast-2:023072794443:cluster/gnss-eks-2-test` |
| Prod        | ga-aws-gnss-common-prod    | `arn:aws:eks:ap-southeast-2:334594953176:cluster/gnss-eks-2-prod` |

![VPC Diagram](docs/gnss-eks-vpc-diagram.png)

## Authentication and Authorisation

To access EKS you will need to authenticate with AWS either directly as an IAM user or as a GA staff
member using GA's SSO authentication for AWS. IAM user accounts should ideally be reserved for use
by automation and non-GA staff.

Once authenticated, you would normally assume one of EKS access roles defined in this repository.
Use [init-kubeconfig.sh](scripts/init-kubeconfig.sh) to insert cluster access information into your
kubeconfig file `~/.kube/config`, including an EKS access role.

```bash
$ init-kubeconfig.sh dev --cached --role read-only
Updated context arn:aws:eks:ap-southeast-2:023072794443:cluster/gnss-eks-2-dev in ~/.kube/config
Using EKS access role arn:aws:iam::023072794443:role/eks-read-only.gnss-eks-2-dev
```

The above command will set you up to use `kubectl` against the dev environment, provided that your
AWS IAM user or SSO role can assume the requested role.

```bash
$ kubectl auth can-i list pod
yes
$ kubectl auth can-i delete pod
no
```

Some tools, like `eksctl`, do not use kubeconfig and will require you to explicitely assume an
EKS access role. You can do this manually using `aws sts assume-role` or you can use
`init-kubeconfig.sh` with option `--assume-role`.

```bash
$ eval $(init-kubeconfig.sh dev --cached --assume-role admin)
$ aws sts get-caller-identity
{
    "UserId": "AROAQKXZ645FUXVTPC3RW:gnss-eks-user",
    "Account": "023072794443",
    "Arn": "arn:aws:sts::023072794443:assumed-role/eks-admin.gnss-eks-2-dev/gnss-eks-user"
}
$ eksctl --cluster=gnss-eks-2 create nodegroup
```

### Cluster-Wide Access Roles

| Access Role                        | Kubernetes Role                | Description                        |
|------------------------------------|--------------------------------|------------------------------------|
| [`read-only`](access-read-only.tf) | `read-only`                    | Read-only access to all resources  |
| [`admin`](access-admin.tf)         | `cluster-admin` (rbac-default) | Read-write access to all resources |

### Namespace Access Roles

We expect system developers to normally restrict their write access to one namespace at a time. When
you are using namespaced read-write access, you will still have read-only access to the entire cluster.

```bash
$ init-kubeconfig.sh dev --cached --role ginan-ops-admin
$ kubectl auth can-i delete pod -n ginan-ops
yes
$ kubectl auth can-i delete pod -n gnss-stream
no
$ kubectl auth can-i list pod -n gnss-stream
yes
```

| Access Role                                 | Kubernetes Role   | Description                                               | 
|---------------------------------------------|-------------------|-----------------------------------------------------------|
| [`ginan-ops-admin`](namespace-ginan-ops.tf) | `read-only`       | Read-only access to all resources                         |
|                                             | `ginan-ops/admin` | Read-write access to all resources in namespace ginan-ops |

### Trusted Principals

The table below lists AWS principals that can assume each defined EKS access role.

| Access Role                                         | AWS Account                       | Current Principals | Proposed Principals                    |
|-----------------------------------------------------|-----------------------------------|--------------------|----------------------------------------|
| [`read-only`](access-read-only.tf)                  | `ga-aws-gnss-common-nonprod/prod` | all                | `role/AWSReservedSSO_GAReadOnly`       |
|                                                     |                                   |                    | `role/AWSReservedSSO_GASuperDeveloper` |
|                                                     |                                   |                    |                                        |
| [`admin`](access-admin.tf)                          | `ga-aws-gnss-common-nonprod/prod` | all                | `role/AWSReservedSSO_GASuperDeveloper` |
|                                                     |                                   |                    | `user/bitbucket-pipelines`             |
|                                                     |                                   |                    |                                        |
| [`gnss-stream-ops-admin`](namespace-gnss-stream.tf) | `ga-aws-gnss-common-nonprod/prod` | all                | `role/AWSReservedSSO_GASuperDeveloper` |
|                                                     |                                   |                    | `user/bitbucket-pipelines`             |
|                                                     |                                   |                    |                                        |
| [`ginan-ops-admin`](namespace-ginan-ops.tf)         | `ga-aws-gnss-common-nonprod/prod` | all                | `role/AWSReservedSSO_GASuperDeveloper` | 
|                                                     |                                   |                    |                                        |
|                                                     | `ga-aws-gpsprocess`               | all                | `user/bitbucket-pipelines`             |
|                                                     |                                   |                    | `user/<external-user>`                 |

### Implementation

Role-based EKS access control comprises AWS roles, Kubernetes roles, role bindings, and `aws-auth` configmap,
which maps AWS roles to Kubernetes users and groups.

Documentation links

* https://docs.aws.amazon.com/eks/latest/userguide/add-user-role.html
* https://kubernetes.io/docs/reference/access-authn-authz/rbac/

Below is a walk-through of cluster-wide read-only access implementation.

Source Files

* [access-read-only.tf](access-read-only.tf): AWS IAM role, Kubernetes role, role binding
* [odc-k8s.tf](odc-k8s.tf): `aws-auth` config map

#### Kubernetes Role: [read-only](access-read-only.tf)

Kubernetes role `read-only` grants read-only access to all resources. 

```bash
$ kubectl get clusterrole read-only -o yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: read-only
  ...
rules:
- apiGroups:
  - '*'
  resources:
  - '*'
  verbs:
  - get
  - list
  - watch
```

#### Kubernetes Role Binding: [read-only](access-read-only.tf)

Kubernetes role binding `read-only` assigns `read-only` role to all kubernetes users in group
`read-only`.

```bash
$ kubectl get clusterrolebinding read-only -o yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: read-only
  ...
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: read-only
subjects:
- apiGroup: rbac.authorization.k8s.io
  kind: Group
  name: read-only
  namespace: default
```

#### Kubernetes Config Map: [aws-auth](odc-k8s.tf)

Kubernetes config map `aws-auth` maps AWS role `read-only` to Kubernetes group `read-only`.

```bash
$ kubectl -n kube-system get configmap aws-auth -o yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: aws-auth
  namespace: kube-system
  ...
data:
  mapRoles: |
    - rolearn: arn:aws:iam::023072794443:role/eks-read-only.gnss-eks-2-dev
      username: read-only
      groups:
        - read-only
    ...
```

## AWS Load Balancer Controller

The ALB controller can fail to clean up node security group ingress rules.
Script [./scripts/eks-node-ingress.sh](./scripts/eks-node-ingress.sh) can find and delete old
ingress rules.

## Flux

Flux authenticates as flux.gagnss@gmail.com against bitbucket.org to gain write
access to https://bitbucket.org/geoscienceaustralia/gnss-helm-releases.

### Gmail Account

**username**: flux.gagnss@gmail.com  
**ssm password key**: /gmail.com/users/flux.gagnss/password  
**contact email**: gnss@ga.gov.au  
**contact phone**:  0434 946 731  

### Bitbucket Account

**username**: flux-gagnss  
**email**: flux.gagnss@gmail.com  
**ssh public key**: `fluxctl identity --k8s-fwd-ns flux`
