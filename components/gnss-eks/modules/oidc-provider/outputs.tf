output "arn" {
  value = aws_iam_openid_connect_provider.identity_provider.arn
}

output "name" {
  value = replace(data.aws_eks_cluster.cluster.identity[0].oidc[0].issuer, "https://", "")
}
