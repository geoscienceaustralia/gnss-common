output "rds_hostname" {
  value = module.rds.rds_cluster_endpoint
}

output "rds_port" {
  value = module.rds.rds_cluster_port
}

output "rds_name" {
  value = local.rds_name
}
