variable "region" {
}

variable "cluster_id" {
}

variable "vpc_id" {
}

variable "database_subnets" {
}

variable "eks_node_security_group_id" {
}
