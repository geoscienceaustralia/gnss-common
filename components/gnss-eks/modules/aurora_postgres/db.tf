locals {
  rds_name = "${var.cluster_id}-aurora-postgres"
}

# RDS Aurora - https://github.com/terraform-aws-modules/terraform-aws-rds-aurora
module "rds" {
  source  = "terraform-aws-modules/rds-aurora/aws"
  version = "= 5.2.0"

  name                = local.rds_name
  engine              = "aurora-postgresql"
  engine_version      = "13.3"
  instance_type       = "db.r5.large"
  storage_encrypted   = true
  apply_immediately   = true
  deletion_protection = true
  database_name       = lower(replace(title(replace(local.rds_name, "-", " ")), " ", "")) # kebab-case -> camelCase
  username            = "superuser"
  password            = random_password.aurora_postgres.result

  # snapshot
  copy_tags_to_snapshot = true
  skip_final_snapshot   = false

  # networking
  vpc_id                 = var.vpc_id
  subnets                = var.database_subnets
  create_security_group  = false
  vpc_security_group_ids = [aws_security_group.aurora_postgres.id]

  # scaling
  replica_scale_enabled = false
  replica_count         = 1
  # TODO: let's try with scaling initially
  # replica_scale_min          = 1
  # replica_scale_max          = 3
  # predefined_metric_type     = "RDSReaderAverageCPUUtilization"
  # replica_scale_connections  = 2200 # Average number of connections to trigger autoscaling at
  # replica_scale_cpu          = 70   # CPU usage to trigger autoscaling at
  # replica_scale_in_cooldown  = 600  # before allowing further scaling operations after scale in - 10min
  # replica_scale_out_cooldown = 300  # before allowing further scaling operations after a scale out - in second

  db_parameter_group_name         = aws_db_parameter_group.aurora_postgres.id
  db_cluster_parameter_group_name = aws_rds_cluster_parameter_group.aurora_postgres.id
}

resource "aws_db_parameter_group" "aurora_postgres" {
  name   = local.rds_name
  family = "aurora-postgresql13"
}

resource "aws_rds_cluster_parameter_group" "aurora_postgres" {
  name   = local.rds_name
  family = "aurora-postgresql13"
}

resource "aws_security_group" "aurora_postgres" {
  name   = local.rds_name
  vpc_id = var.vpc_id
}

resource "aws_security_group_rule" "aurora_postgres" {
  description              = "Allow eks node group to connect to db"
  type                     = "ingress"
  from_port                = 5432
  to_port                  = 5432
  protocol                 = "tcp"
  security_group_id        = aws_security_group.aurora_postgres.id
  source_security_group_id = var.eks_node_security_group_id
}

resource "aws_ssm_parameter" "aurora_postgres_master_password" {
  name  = "/${local.rds_name}/user/master/password"
  type  = "SecureString"
  value = random_password.aurora_postgres.result
}

resource "random_password" "aurora_postgres" {
  length  = 16
  special = false
}
