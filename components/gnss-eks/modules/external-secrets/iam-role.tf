locals {
  service_account = "external-secrets"
}

resource "aws_iam_role" "external_secrets" {
  name = local.aws_prefix
  path = "/"

  assume_role_policy = data.aws_iam_policy_document.external_secrets_trust_policy.json
}

data "aws_iam_policy_document" "external_secrets_trust_policy" {
  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]
    effect  = "Allow"

    principals {
      identifiers = [var.cluster_oidc_provider_arn]
      type        = "Federated"
    }

    condition {
      test     = "StringEquals"
      variable = "${var.cluster_oidc_provider_name}:sub"

      values = [
        "system:serviceaccount:${local.k8s_namespace}:${local.service_account}",
      ]
    }
  }
}

data "aws_iam_policy_document" "external_secrets" {
  statement {
    effect    = "Allow"
    resources = ["*"]

    actions = [
      "sts:AssumeRole",
    ]
  }
}

resource "aws_iam_role_policy" "external_secrets" {
  role   = aws_iam_role.external_secrets.id
  policy = data.aws_iam_policy_document.external_secrets.json
}
