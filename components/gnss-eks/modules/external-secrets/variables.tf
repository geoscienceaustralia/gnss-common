variable "region" {
  type = string
}

variable "cluster_id" {
  type = string
}

variable "cluster_oidc_provider_name" {
  type = string
}

variable "cluster_oidc_provider_arn" {
  type = string
}
