locals {
  app           = "external-secrets"
  aws_prefix    = "${var.cluster_id}-${local.app}"
  k8s_namespace = "${local.app}-operator"
}

module "crds" {
  source = "./crds"
}

resource "helm_release" "external_secrets" {
  name       = local.app
  repository = "https://charts.external-secrets.io"
  chart      = "external-secrets"
  version    = "0.7.2"

  namespace        = local.k8s_namespace
  create_namespace = true

  values = [
    templatefile("${path.module}/helm-release-values.yaml", {
      service_account = local.service_account
      iam_role        = aws_iam_role.external_secrets.arn
    })
  ]

  force_update = true

  depends_on = [
    module.crds
  ]
}
