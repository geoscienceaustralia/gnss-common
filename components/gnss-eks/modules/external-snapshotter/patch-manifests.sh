#!/usr/bin/env bash

# Process manifests
#
# * Run kustomize
# * Slice resources into files
# * Customise namespace
# * Remove `metadata.creationTimestamp` to avoid terraform churn, it will be set server-side
# * Remove `status`, which the kubernetes provider doesn't permit

# TODO: Terraform may one day ignore `status`.
# https://github.com/hashicorp/terraform-provider-kubernetes/issues/1428

set -euo pipefail 

eval "$(jq -r '@sh "MANIFESTS=\(.manifests) NAMESPACE=\(.namespace)"')"

cd "$MANIFESTS"

rm -rf patched
kustomize build . | kubectl-slice -f - -o patched

for f in patched/*.yaml; do
  sed -i "s/namespace: kube-system/namespace: $NAMESPACE/g" "$f"
  yq -iy 'del(.metadata.creationTimestamp)' "$f"
  yq -iy 'del(.status)' "$f"
done

export MANIFESTS

# shellcheck disable=SC2012
# https://www.shellcheck.net/wiki/SC2012
ls patched | jq -R -s 'split("\n")[:-1] | reduce .[] as $i ({}; .[$i] = env.MANIFESTS + "/patched/" + $i)'
