# Install Snapshot CRDs and Snapshot Controller.
# See https://github.com/kubernetes-csi/external-snapshotter.

locals {
  namespace = "external-snapshotter"
}

resource "kubernetes_namespace" "external_snapshotter" {
  metadata {
    name = local.namespace
  }
}

# This is not a terraform module. The effect is to fetch k8s yaml manifest
# files to a known location under `.terraform/modules/`. We could clone in
# in an external data resource, but then we would need to implement caching 
# to avoid cloning on every terraform plan/apply.
module "manifests" {
  source = "github.com/kubernetes-csi/external-snapshotter?ref=v6.2.1"
  count  = 0
}

locals {
  manifests = "${path.root}/.terraform/modules/external_snapshotter.manifests"
}

data "external" "crd_manifests" {
  program = ["${path.module}/patch-manifests.sh"]
  query = {
    manifests = "${local.manifests}/client/config/crd"
    namespace = local.namespace
  }
}

data "external" "controller_manifests" {
  program = ["${path.module}/patch-manifests.sh"]
  query = {
    manifests = "${local.manifests}/deploy/kubernetes/snapshot-controller"
    namespace = local.namespace
  }
}

resource "kubernetes_manifest" "crds" {
  for_each = data.external.crd_manifests.result
  manifest = yamldecode(file(each.value))

  depends_on = [
    kubernetes_namespace.external_snapshotter,
  ]
}

resource "kubernetes_manifest" "controller" {
  for_each = data.external.controller_manifests.result
  manifest = yamldecode(file(each.value))

  depends_on = [
    kubernetes_namespace.external_snapshotter,
    kubernetes_manifest.crds,
  ]
}
