locals {
  application = "efs-csi-driver"
  prefix      = "${var.cluster_id}-${local.application}"
}

resource "kubernetes_namespace" "efs_csi" {
  metadata {
    name = local.application
  }
}

resource "aws_efs_file_system" "storage" {
  tags = {
    Name = "${local.prefix}-default"
  }

  throughput_mode = "elastic"
}

resource "aws_efs_mount_target" "storage" {
  count = length(var.cluster_private_subnets)

  file_system_id = aws_efs_file_system.storage.id

  subnet_id = element(
    tolist(var.cluster_private_subnets),
    count.index,
  )

  security_groups = [aws_security_group.efs.id]
}

resource "aws_security_group" "efs" {
  name   = local.prefix
  vpc_id = var.vpc_id

  ingress {
    from_port       = "2049"
    to_port         = "2049"
    protocol        = "tcp"
    security_groups = var.cluster_security_groups
  }
}
