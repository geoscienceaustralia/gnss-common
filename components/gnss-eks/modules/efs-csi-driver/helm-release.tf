resource "helm_release" "efs_csi_driver" {
  name       = local.application
  repository = "https://kubernetes-sigs.github.io/aws-efs-csi-driver"
  chart      = "aws-efs-csi-driver"
  version    = "2.4.0"

  namespace        = local.application
  create_namespace = true

  values = [
    templatefile("${path.module}/helm-release-values.yaml", {
      controller_service_account = local.controller_service_account
      node_service_account       = local.node_service_account
      iam_role                   = aws_iam_role.efs_access.arn
      file_system_id             = aws_efs_file_system.storage.id
    })
  ]

  force_update = true
}
