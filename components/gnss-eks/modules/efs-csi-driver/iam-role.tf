# https://github.com/kubernetes-sigs/aws-efs-csi-driver/blob/v1.5.3/docs/iam-policy-example.json

locals {
  controller_service_account = "efs-csi-controller"
  node_service_account       = "efs-csi-node"
}

resource "aws_iam_role" "efs_access" {
  name = local.prefix
  path = "/"

  assume_role_policy = data.aws_iam_policy_document.efs_access_trust_policy.json
}

data "aws_iam_policy_document" "efs_access_trust_policy" {
  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]
    effect  = "Allow"

    principals {
      identifiers = [var.cluster_oidc_provider_arn]
      type        = "Federated"
    }

    condition {
      test     = "StringEquals"
      variable = "${var.cluster_oidc_provider_name}:sub"

      values = [
        "system:serviceaccount:${local.application}:${local.controller_service_account}",
        "system:serviceaccount:${local.application}:${local.node_service_account}"
      ]
    }
  }
}

data "aws_iam_policy_document" "efs_access" {
  statement {
    effect    = "Allow"
    resources = ["*"]

    actions = [
      "elasticfilesystem:DescribeAccessPoints",
      "elasticfilesystem:DescribeFileSystems",
      "elasticfilesystem:DescribeMountTargets",
      "ec2:DescribeAvailabilityZones",
    ]
  }

  statement {
    effect    = "Allow"
    resources = ["*"]
    actions   = ["elasticfilesystem:CreateAccessPoint"]

    condition {
      test     = "StringLike"
      variable = "aws:RequestTag/efs.csi.aws.com/cluster"
      values   = ["true"]
    }
  }

  statement {
    effect    = "Allow"
    resources = ["*"]
    actions   = ["elasticfilesystem:DeleteAccessPoint"]

    condition {
      test     = "StringEquals"
      variable = "aws:ResourceTag/efs.csi.aws.com/cluster"
      values   = ["true"]
    }
  }
}

resource "aws_iam_role_policy" "efs_access" {
  role   = aws_iam_role.efs_access.id
  policy = data.aws_iam_policy_document.efs_access.json
}
