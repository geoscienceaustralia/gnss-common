variable "vpc_id" {
  type = string
}

variable "cluster_id" {
  type = string
}

variable "cluster_private_subnets" {
  type = list(string)
}

variable "cluster_security_groups" {
  type = list(string)
}

variable "cluster_oidc_provider_name" {
  type = string
}

variable "cluster_oidc_provider_arn" {
  type = string
}
