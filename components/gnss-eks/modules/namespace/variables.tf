variable "cluster_id" {
  type = string
}

variable "namespace" {
  type = string
}

variable "admin_aws_prod_principals" {
  type = list(string)
}

variable "admin_aws_nonprod_principals" {
  type = list(string)
}

variable "aws_role_policy_arn" {
  type = string
}
