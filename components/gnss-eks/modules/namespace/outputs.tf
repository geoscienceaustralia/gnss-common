output "aws_admin_role_arn" {
  value = aws_iam_role.admin.arn
}

output "k8s_admin_user" {
  value = "eks-${var.namespace}-admin"
}

output "k8s_admin_group" {
  value = "${var.namespace}-admin"
}
