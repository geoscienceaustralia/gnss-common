# Create AWS admin role

data "aws_iam_policy_document" "read_only_trust_policy" {
  statement {
    sid     = "1"
    actions = ["sts:AssumeRole"]
    principals {
      type        = "AWS"
      identifiers = terraform.workspace == "prod" ? var.admin_aws_prod_principals : var.admin_aws_nonprod_principals
    }
    condition {
      test     = "Bool"
      variable = "aws:SecureTransport"
      values   = ["true"]
    }
  }
}

resource "aws_iam_role" "read_only" {
  name                 = "eks-${var.namespace}-read-only.${var.cluster_id}"
  assume_role_policy   = data.aws_iam_policy_document.read_only_trust_policy.json
  max_session_duration = "28800"
}

resource "aws_iam_role_policy_attachment" "read_only" {
  role       = aws_iam_role.read_only.name
  policy_arn = var.aws_role_policy_arn
}

data "aws_iam_policy_document" "admin_trust_policy" {
  statement {
    sid     = "1"
    actions = ["sts:AssumeRole"]
    principals {
      type        = "AWS"
      identifiers = terraform.workspace == "prod" ? var.admin_aws_prod_principals : var.admin_aws_nonprod_principals
    }
    condition {
      test     = "Bool"
      variable = "aws:SecureTransport"
      values   = ["true"]
    }
  }
}

resource "aws_iam_role" "admin" {
  name                 = "eks-${var.namespace}-admin.${var.cluster_id}"
  assume_role_policy   = data.aws_iam_policy_document.admin_trust_policy.json
  max_session_duration = "28800"
}

resource "aws_iam_role_policy_attachment" "admin" {
  role       = aws_iam_role.admin.name
  policy_arn = var.aws_role_policy_arn
}
