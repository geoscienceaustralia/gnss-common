# Create
# * k8s namespace
# * k8s admin role bound to group ${namespace}-admin
#
# See ./odc-k8s.tf for aws-auth config map configuration

resource "kubernetes_namespace" "namespace" {
  metadata {
    name = var.namespace
  }
}

resource "kubernetes_role" "admin" {
  metadata {
    name      = "admin"
    namespace = kubernetes_namespace.namespace.metadata[0].name
  }
  rule {
    api_groups = ["*"]
    resources  = ["*"]
    verbs      = ["*"]
  }
}

resource "kubernetes_role_binding" "admin" {
  metadata {
    name      = "admin"
    namespace = kubernetes_namespace.namespace.metadata[0].name
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = kubernetes_role.admin.metadata[0].name
  }
  subject {
    kind      = "Group"
    name      = "${kubernetes_namespace.namespace.metadata[0].name}-admin"
    api_group = "rbac.authorization.k8s.io"
  }
}
