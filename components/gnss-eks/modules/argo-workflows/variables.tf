variable "region" {
}

variable "cluster_id" {
}

variable "certificate_arn" {
}

variable "cluster_oidc_provider_name" {
  type = string
}

variable "cluster_oidc_provider_arn" {
  type = string
}

variable "domain_name" {
}

variable "namespace" {
}

variable "enable_persistence" {
  default = false # Argo persistence has not been tested
}

variable "rds_hostname" {
  default = ""
}

variable "rds_port" {
  default = ""
}

variable "rds_name" {
  default = ""
}

variable "cognito_user_pool_id" {
}

variable "cognito_user_prod_pool_id" {
}

variable "cognito_user_pool_client" {
  type = object({
    id     = string
    secret = string
  })
}

variable "cognito_user_prod_pool_clients" {
  type = map(object({
    id     = string
    secret = string
  }))
}
