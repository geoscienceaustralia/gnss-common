locals {
  prefix         = "argo-workflows"
  component_name = "${local.prefix}-${var.namespace}"
  db_name        = local.component_name
  db_user        = "argo"
}

data "template_file" "server_config" {
  template = file("${path.module}/argo-workflows.yaml")

  vars = {
    namespace            = var.namespace
    service_account_name = local.prefix
    role_arn             = aws_iam_role.artifact_repository.arn

    region          = var.region
    certificate_arn = var.certificate_arn
    hostname        = "${local.component_name}.${var.domain_name}"

    allowed_sso_domains = "ga.gov.au"

    enable_persistence = var.enable_persistence
    db_hostname        = var.rds_hostname
    db_port            = var.rds_port
    db_name            = local.db_name
    db_table_name      = "workflows"

    cognito_region       = var.region
    cognito_user_pool_id = var.cognito_user_prod_pool_id

    artifact_bucket = aws_s3_bucket.artifact_repository.id
  }
}

resource "kubernetes_config_map" "server_config" {
  metadata {
    name      = "${local.prefix}-server-config"
    namespace = var.namespace
  }
  data = {
    "values.yaml" = data.template_file.server_config.rendered
  }
}

resource "kubernetes_secret" "server_sso" {
  metadata {
    name      = "${local.prefix}-server-sso"
    namespace = var.namespace
  }
  data = (terraform.workspace == "prod"
    ? {
      client-id     = var.cognito_user_pool_client.id
      client-secret = var.cognito_user_pool_client.secret
    }
    : {
      client-id     = var.cognito_user_prod_pool_clients[terraform.workspace].id
      client-secret = var.cognito_user_prod_pool_clients[terraform.workspace].secret
    }
  )
  type = "Opaque"
}

resource "aws_ssm_parameter" "db_creds" {
  count = var.enable_persistence ? 1 : 0
  name  = "/${var.rds_name}-${local.db_name}/user/${local.db_user}/password"
  type  = "SecureString"
  value = random_password.db_user[0].result
}

resource "random_password" "db_user" {
  count   = var.enable_persistence ? 1 : 0
  length  = 16
  special = false
}

resource "kubernetes_secret" "db_creds" {
  count = var.enable_persistence ? 1 : 0
  metadata {
    name      = "${local.prefix}-db-credentials"
    namespace = var.namespace
  }

  data = {
    username = local.db_user
    password = random_password.db_user[0].result
  }

  type = "Opaque"
}

resource "aws_s3_bucket" "artifact_repository" {
  bucket = "${var.cluster_id}-${local.component_name}-artifacts"
  acl    = "private"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

locals {
  artifact_repository_role_name = "${var.cluster_id}-${local.component_name}-artifacts"
  account_id                    = data.aws_caller_identity.current.account_id
}

data "aws_caller_identity" "current" {}

# Note: Argo server k8s service account will assume this role via IRSA, but it will then again want
# to assume this role because of artifactRepository.s3.roleARN setting in argo-workflows.yaml.
resource "aws_iam_role" "artifact_repository" {
  name = local.artifact_repository_role_name

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Action = "sts:AssumeRole"
        Principal = {
          AWS = "arn:aws:iam::${local.account_id}:root"
        }
        Condition = {
          ArnEquals = {
            "aws:PrincipalArn" = "arn:aws:iam::${local.account_id}:role/${local.artifact_repository_role_name}"
          }
        }
      },
      {
        Action = "sts:AssumeRole"
        Principal = {
          AWS = (
            terraform.workspace == "prod"
            ? "arn:aws:iam::456468468056:root" # ginan-ops-prod
            : "arn:aws:iam::330606217751:root" # ginan-ops-nonprod
          )
        }
        Effect = "Allow"
      },
      {
        Action = "sts:AssumeRoleWithWebIdentity"
        Principal = {
          Federated = var.cluster_oidc_provider_arn
        }
        Condition = {
          StringLike = {
            "${var.cluster_oidc_provider_name}:sub" = "system:serviceaccount:${var.namespace}:*"
          }
        }
        Effect = "Allow"
      }
    ]
  })

  inline_policy {
    name = "artifact-repository-read-write"
    policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Effect = "Allow"
          Action = [
            "s3:ListBucket",
          ]
          Resource = aws_s3_bucket.artifact_repository.arn
        },
        {
          Effect = "Allow"
          Action = [
            "s3:GetObject",
            "s3:PutObject"
          ]
          Resource = "${aws_s3_bucket.artifact_repository.arn}/*"
        },
        {
          Effect = "Allow"
          Action = [
            "sts:AssumeRole",
          ]
          Resource = "arn:aws:iam::${local.account_id}:role/${local.artifact_repository_role_name}"
        },
      ]
    })
  }
}
