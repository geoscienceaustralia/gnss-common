variable "cluster_id" {
  type = string
}

variable "add_on_version" {
  type = string
}

variable "cluster_oidc_provider_name" {
  type = string
}

variable "cluster_oidc_provider_arn" {
  type = string
}
