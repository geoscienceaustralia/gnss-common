resource "aws_eks_addon" "ebs_csi_driver" {
  cluster_name             = var.cluster_id
  addon_name               = "aws-ebs-csi-driver"
  addon_version            = var.add_on_version
  service_account_role_arn = aws_iam_role.ebs_csi_driver.arn
  resolve_conflicts        = "OVERWRITE"

  depends_on = [
    aws_iam_role_policy_attachment.ebs_csi_driver_policy
  ]
}

resource "aws_iam_role" "ebs_csi_driver" {
  name               = "${var.cluster_id}-ebs-csi-driver"
  assume_role_policy = data.aws_iam_policy_document.ebs_csi_driver_role_trust_policy.json
}

data "aws_iam_policy_document" "ebs_csi_driver_role_trust_policy" {
  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]
    effect  = "Allow"

    principals {
      identifiers = [var.cluster_oidc_provider_arn]
      type        = "Federated"
    }

    condition {
      test     = "StringEquals"
      variable = "${var.cluster_oidc_provider_name}:sub"
      values   = ["system:serviceaccount:kube-system:ebs-csi-controller-sa"]
    }
  }
}

resource "aws_iam_role_policy_attachment" "ebs_csi_driver_policy" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy"
  role       = aws_iam_role.ebs_csi_driver.id
}

resource "kubernetes_storage_class_v1" "storage_class" {
  metadata {
    name = "ebs-sc"
  }
  storage_provisioner = "ebs.csi.aws.com"
  volume_binding_mode = "WaitForFirstConsumer"
  reclaim_policy      = "Delete"
}

resource "kubernetes_manifest" "volume_snapshot_class" {
  manifest = {
    "apiVersion"     = "snapshot.storage.k8s.io/v1"
    "deletionPolicy" = "Delete"
    "driver"         = "ebs.csi.aws.com"
    "kind"           = "VolumeSnapshotClass"
    "metadata" = {
      "name" = "csi-aws-vsc"
    }
  }
}
