# Peering from gnss-eks-test to corsnet-nsw-test
resource "aws_vpc_peering_connection" "corsnet_nsw_requester_test" {
  count         = terraform.workspace == "test" ? 1 : 0
  vpc_id        = module.eks.vpc_id
  peer_owner_id = "184279481395"
  peer_region   = "ap-southeast-2"
  peer_vpc_id   = "vpc-0e1a73f379721835f"

  tags = {
    "Name" = "${local.cluster_id}-corsnet-nsw-requester_test"
  }

}

resource "aws_vpc_peering_connection_options" "corsnet_nsw_requester_test" {
  count                     = terraform.workspace == "test" ? 1 : 0
  vpc_peering_connection_id = aws_vpc_peering_connection.corsnet_nsw_requester_test[0].id

  requester {
    allow_remote_vpc_dns_resolution = true
  }
}

resource "aws_route" "corsnet_nsw_test" {
  for_each                  = terraform.workspace == "test" ? data.aws_route_table.private_subnets : {}
  route_table_id            = each.value.id
  destination_cidr_block    = "10.11.4.0/22"
  vpc_peering_connection_id = aws_vpc_peering_connection.corsnet_nsw_requester_test[0].id
}

# Peering from gnss-eks-prod to corsnet-nsw-prod
resource "aws_vpc_peering_connection" "corsnet_nsw_requester_prod" {
  count         = terraform.workspace == "prod" ? 1 : 0
  vpc_id        = module.eks.vpc_id
  peer_owner_id = "435368323128"
  peer_region   = "ap-southeast-2"
  peer_vpc_id   = "vpc-0aa66f36e567db897"

  tags = {
    "Name" = "${local.cluster_id}-corsnet-nsw-requester_prod"
  }

  requester {
    allow_remote_vpc_dns_resolution = true
  }
}

resource "aws_route" "corsnet_nsw_prod" {
  for_each                  = terraform.workspace == "prod" ? data.aws_route_table.private_subnets : {}
  route_table_id            = each.value.id
  destination_cidr_block    = "10.11.28.0/22"
  vpc_peering_connection_id = aws_vpc_peering_connection.corsnet_nsw_requester_prod[0].id
}

data "aws_route_table" "private_subnets" {
  for_each  = toset(module.eks.private_subnets)
  subnet_id = each.key
}
