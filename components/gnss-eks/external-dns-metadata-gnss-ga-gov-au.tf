# external-dns installation for $env-metadatadata.gnss.ga.gov.au

data "template_file" "external_dns_metadata_gnss_ga_gov_au" {
  template = file("${path.module}/external-dns.yaml")
  vars = {
    cluster_id          = local.cluster_id
    domain_name         = "${terraform.workspace}-metadata.gnss.ga.gov.au"
    service_account_arn = var.metadata_gnss_ga_gov_au_external_dns_role_arn
  }
}

resource "kubernetes_secret" "external_dns_metadata_gnss_ga_gov_au" {
  metadata {
    name      = "external-dns-metadata-gnss-ga-gov-au"
    namespace = kubernetes_namespace.external_dns.metadata[0].name
  }

  data = {
    "values.yaml" = data.template_file.external_dns_metadata_gnss_ga_gov_au.rendered
  }

  type = "Opaque"
}

resource "aws_acm_certificate" "metadata_gnss_ga_gov_au" {
  domain_name = "*.${terraform.workspace}-metadata.gnss.ga.gov.au"

  subject_alternative_names = concat(
    [
      "${terraform.workspace}-metadata.gnss.ga.gov.au",

      # TODO: Remove once the old domain name is decommissioned.
      "${terraform.workspace}geodesy-webservices.geodesy.ga.gov.au",
      "${terraform.workspace}.geodesy.ga.gov.au",
    ],
    terraform.workspace == "prod"
    ? [
      "*.metadata.gnss.ga.gov.au",
      "metadata.gnss.ga.gov.au",

      # TODO: Remove once the old domain name is decommissioned.
      "gws.geodesy.ga.gov.au",
    ]
    : []
  )

  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}
