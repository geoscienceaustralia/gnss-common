# Create
# * AWS role `eks-read-only.${cluster_id}` TODO: Rename to `${cluster_id}-read-only`
# * k8s role `read-only` bound to group `read-only`
#
# See ./odc-k8s.tf for `aws-auth` config map configuration

locals {
  k8s_read_only_group = "read-only"
}

data "aws_iam_policy_document" "read_only_trust_policy" {
  statement {
    sid     = "1"
    actions = ["sts:AssumeRole"]
    principals {
      type        = "AWS"
      identifiers = terraform.workspace == "prod" ? var.prod_aws_principals : var.nonprod_aws_principals
    }
    condition {
      test     = "Bool"
      variable = "aws:SecureTransport"
      values   = ["true"]
    }
  }
}

resource "aws_iam_role" "read_only" {
  name                 = "eks-read-only.${local.cluster_id}"
  assume_role_policy   = data.aws_iam_policy_document.read_only_trust_policy.json
  max_session_duration = "28800"
}

resource "aws_iam_role_policy_attachment" "readonly" {
  role       = aws_iam_role.read_only.name
  policy_arn = aws_iam_policy.eks_user.arn
}

resource "kubernetes_cluster_role" "read_only" {
  metadata {
    name = "read-only"
  }
  rule {
    api_groups = ["*"]
    resources  = ["*"]
    verbs      = ["get", "list", "watch"]
  }
}

resource "kubernetes_cluster_role_binding" "read_only" {
  metadata {
    name = "read-only"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_cluster_role.read_only.metadata[0].name
  }
  subject {
    kind      = "Group"
    name      = local.k8s_read_only_group
    api_group = "rbac.authorization.k8s.io"
  }
}
