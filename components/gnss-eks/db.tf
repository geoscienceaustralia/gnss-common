module "aurora_postgres" {
  source = "./modules/aurora_postgres"
  count  = 0

  region                     = var.region
  cluster_id                 = local.cluster_id
  vpc_id                     = module.eks.vpc_id
  database_subnets           = module.eks.database_subnets
  eks_node_security_group_id = module.eks.node_security_group
}
