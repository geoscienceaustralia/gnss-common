# external-dns installation for $env-data.gnss.ga.gov.au

data "template_file" "external_dns_data_gnss_ga_gov_au" {
  template = file("${path.module}/external-dns.yaml")
  vars = {
    cluster_id          = local.cluster_id
    domain_name         = "${terraform.workspace}-data.gnss.ga.gov.au"
    service_account_arn = var.data_gnss_ga_gov_au_external_dns_role_arn
  }
}

resource "kubernetes_secret" "external_dns_data_gnss_ga_gov_au" {
  metadata {
    name      = "external-dns-data-gnss-ga-gov-au"
    namespace = kubernetes_namespace.external_dns.metadata[0].name
  }

  data = {
    "values.yaml" = data.template_file.external_dns_data_gnss_ga_gov_au.rendered
  }

  type = "Opaque"
}
