resource "kubernetes_role" "gnss_stream_developer" {
  metadata {
    name      = "developer"
    namespace = "gnss-stream"
  }

  rule {
    api_groups = ["", "extensions", "apps", "helm.fluxcd.io"]
    resources  = ["deployments", "events", "replicasets", "pods", "pods/log", "pods/exec", "helmreleases", "ingresses", "services", "endpoints", "secrets"]
    verbs      = ["get", "list", "watch", "describe", "create", "update", "patch", "delete"]
  }
}
