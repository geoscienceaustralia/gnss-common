data "terraform_remote_state" "cognito" {
  backend = "s3"
  config = {
    bucket = "gnss-common-${terraform.workspace == "prod" ? "prod" : "nonprod"}-terraform-state"
    key    = "gnss-cognito/${terraform.workspace}/terraform.tfstate"
    region = "ap-southeast-2"
  }
}

data "terraform_remote_state" "cognito_prod" {
  backend = "s3"
  config = {
    bucket   = "gnss-common-prod-terraform-state"
    key      = "gnss-cognito/prod/terraform.tfstate"
    region   = "ap-southeast-2"
    role_arn = var.gnss_common_reader_role_prod_arn
  }
}
