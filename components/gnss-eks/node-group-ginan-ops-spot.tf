data "aws_ssm_parameter" "eks_ami_release_version" {
  name = "/aws/service/eks/optimized-ami/${var.k8s_version}/amazon-linux-2023/x86_64/standard/recommended/release_version"
}

locals {
  spot_node_group_name = "ginan-ops-spot"
  spot_instance_types  = ["c5a.4xlarge", "m5a.4xlarge", "c6a.4xlarge", "m6a.4xlarge"]
}

resource "aws_launch_template" "spot" {
  name_prefix = "${module.eks.cluster_id}-${local.spot_node_group_name}"

  network_interfaces {
    associate_public_ip_address = false
    security_groups             = [module.eks.node_security_group] # TODO: cluster_security_group or custom SG?
    delete_on_termination       = true
  }

  block_device_mappings {
    device_name = "/dev/xvda"

    ebs {
      volume_size = 50
    }
  }


  tag_specifications {
    resource_type = "instance"

    tags = {
      Name = "${module.eks.cluster_id}-${local.spot_node_group_name}"
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_eks_node_group" "spot" {
  cluster_name    = module.eks.cluster_id
  node_group_name = local.spot_node_group_name
  node_role_arn   = module.eks.node_role_arn
  release_version = nonsensitive(data.aws_ssm_parameter.eks_ami_release_version.value)
  subnet_ids      = module.eks.private_subnets
  capacity_type   = "SPOT"
  instance_types  = local.spot_instance_types

  launch_template {
    id      = aws_launch_template.spot.id
    version = aws_launch_template.spot.latest_version
  }

  # managed by cluster-autoscaler
  scaling_config {
    desired_size = 0
    min_size     = 0
    max_size     = 3
  }

  update_config {
    max_unavailable = 1
  }

  labels = {
    dedicated = local.spot_node_group_name
  }

  taint {
    key    = "dedicated"
    value  = local.spot_node_group_name
    effect = "NO_SCHEDULE"
  }

  lifecycle {
    ignore_changes = [scaling_config[0].desired_size]
  }
}
