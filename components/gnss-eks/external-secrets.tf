module "external_secrets" {
  source = "./modules/external-secrets"

  region                     = var.region
  cluster_id                 = module.eks.cluster_id
  cluster_oidc_provider_name = local.cluster_oidc_provider_name
  cluster_oidc_provider_arn  = local.cluster_oidc_provider_arn
}
