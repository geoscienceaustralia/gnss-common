data "aws_iam_policy_document" "alb_controller_access_trust_policy" {
  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]
    effect  = "Allow"

    principals {
      identifiers = [local.cluster_oidc_provider_arn]
      type        = "Federated"
    }

    condition {
      test     = "StringEquals"
      variable = "${local.cluster_oidc_provider_name}:sub"
      values   = ["system:serviceaccount:alb-controller:alb-controller-aws-load-balancer-controller"]
    }
  }
}

resource "aws_iam_role" "alb_controller_access" {
  name = "${local.cluster_id}-alb-controller"
  path = "/"

  assume_role_policy = data.aws_iam_policy_document.alb_controller_access_trust_policy.json
}

resource "aws_iam_role_policy" "alb_controller_access" {
  role   = aws_iam_role.alb_controller_access.id
  policy = file("${path.module}/alb-controller-policy.json")
}

data "template_file" "alb_controller" {
  template = file("${path.module}/alb-controller.yaml")
  vars = {
    region              = var.region
    cluster_id          = local.cluster_id
    service_account_arn = aws_iam_role.alb_controller_access.arn
  }
}

resource "kubernetes_namespace" "alb_controller" {
  metadata {
    name = "alb-controller"
  }
}

resource "kubernetes_secret" "alb_controller" {
  metadata {
    name      = "alb-controller"
    namespace = kubernetes_namespace.alb_controller.metadata[0].name
  }

  data = {
    "values.yaml" = data.template_file.alb_controller.rendered
  }

  type = "Opaque"
}
