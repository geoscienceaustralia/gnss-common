module "namespace_ginan_ops" {
  source              = "./modules/namespace"
  cluster_id          = local.cluster_id
  aws_role_policy_arn = aws_iam_policy.eks_user.arn
  namespace           = "ginan-ops"
  admin_aws_nonprod_principals = [
    "arn:aws:iam::023072794443:root", # gnss-common-nonprod
    "arn:aws:iam::604917042985:root", # gpsprocess (non-prod)
    "arn:aws:iam::330606217751:root", # ginan-ops-nonprod
  ]
  admin_aws_prod_principals = [
    "arn:aws:iam::334594953176:root", # gnss-common-prod
    "arn:aws:iam::456468468056:root", # ginan-ops-prod
  ]
}
