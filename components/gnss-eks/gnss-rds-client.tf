locals {
  private_subnet_route_table_ids = sort(concat([
    for s in data.aws_route_table.private_subnets : s.id
  ]))
}

module "rds_client" {
  source = "git@bitbucket.org:geoscienceaustralia/gnss-common//components/rds-client?ref=774bdea"

  providers = {
    aws.accepter = aws
  }

  region = var.region

  requester_system = local.cluster_id

  requester_vpc = {
    id              = module.eks.vpc_id
    cidr_block      = local.vpc_cidr
    route_table_ids = local.private_subnet_route_table_ids
  }

  accepter_system = "gnss-rds-${terraform.workspace}"
}

