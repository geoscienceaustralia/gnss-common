terraform {
  required_version = ">= 0.14.11, < 2.0.0"

  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
    helm = {
      source = "hashicorp/helm"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
    }
    kustomization = {
      source = "kbst/kustomization"
    }
    external = {
      source = "hashicorp/external"
    }
  }
}

provider "aws" {
  region = var.region
}

provider "aws" {
  alias  = "us-east-1"
  region = "us-east-1"
}

provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
  }
  experiments {
    manifest = true
  }
}

provider "kustomization" {
  kubeconfig_path = "~/.kube/config"
}
