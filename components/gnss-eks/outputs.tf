output "cluster_id" {
  value = module.eks.cluster_id
}

output "node_role_arn" {
  value = module.eks.node_role_arn
}

output "node_security_group" {
  value = module.eks.node_security_group
}

output "private_subnets" {
  value = module.eks.private_subnets
}

output "cluster_admin_role" {
  value = aws_iam_role.admin.arn
}

output "cluster_read_only_role" {
  value = aws_iam_role.read_only.arn
}
