#!/usr/bin/env bash

TF_VAR_gnss_common_reader_role_prod_arn=$(gnss-common-reader-role.sh -e prod)
export TF_VAR_gnss_common_reader_role_prod_arn
