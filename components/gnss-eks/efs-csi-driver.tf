module "efs_csi_driver" {
  source = "./modules/efs-csi-driver"

  vpc_id                     = module.eks.vpc_id
  cluster_id                 = module.eks.cluster_id
  cluster_private_subnets    = module.eks.private_subnets
  cluster_security_groups    = [module.eks.node_security_group, module.eks.cluster_security_group]
  cluster_oidc_provider_name = local.cluster_oidc_provider_name
  cluster_oidc_provider_arn  = local.cluster_oidc_provider_arn
}
