# Configure default EBS storage class

resource "null_resource" "allow_ebs_volume_expansion" {
  provisioner "local-exec" {
    command = "kubectl patch storageclass gp2 -p '{\"allowVolumeExpansion\": true}'"
  }

  provisioner "local-exec" {
    when    = destroy
    command = "kubectl patch storageclass gp2 -p '{\"allowVolumeExpansion\": false}'"
  }
}
