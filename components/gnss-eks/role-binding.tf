resource "kubernetes_role_binding" "gnss_stream_developer" {
  metadata {
    name      = "eks-gnss-stream-developer"
    namespace = "gnss-stream"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = "developer"
  }
  subject {
    kind      = "Group"
    name      = "kube-gnss-stream-developer"
    api_group = "rbac.authorization.k8s.io"
  }
}
