locals {
  flux2_namespace = "flux2"
}

# Chart: https://github.com/fluxcd-community/helm-charts/tree/main/charts/flux2
# https://fluxcd.io/docs/components/
resource "helm_release" "flux2" {
  name       = "flux2"
  repository = "https://fluxcd-community.github.io/helm-charts"
  chart      = "flux2"
  version    = "2.13.0"

  namespace        = local.flux2_namespace
  create_namespace = true

  set {
    name  = "kustomizeController.create"
    value = true
  }

  set {
    name  = "helmController.create"
    value = true
  }

  set {
    name  = "notificationController.create"
    value = false
  }

  set {
    name  = "imageAutomationController.create"
    value = false
  }

  set {
    name  = "imageReflectionController.create"
    value = false
  }
}

data "aws_ssm_parameter" "flux2_bitbucket_token" {
  name = "/bitbucket.org/gnss-helm-releases/access-token/flux2"
}

resource "kubernetes_secret" "flux2_bitbucket_key" {
  depends_on = [helm_release.flux2]

  metadata {
    name      = "flux2-bitbucket-key"
    namespace = local.flux2_namespace
  }

  data = {
    username = "x-token-auth"
    password = data.aws_ssm_parameter.flux2_bitbucket_token.value
  }
}

# Chart: https://github.com/fluxcd-community/helm-charts/tree/main/charts/flux2-sync
# https://fluxcd.io/docs/components/source/gitrepositories/
resource "helm_release" "flux2_sync" {
  depends_on = [helm_release.flux2]
  name       = "gnss-helm-releases"
  repository = "https://fluxcd-community.github.io/helm-charts"
  chart      = "flux2-sync"
  version    = "1.9.0"
  namespace  = local.flux2_namespace

  values = [
    templatefile("${path.module}/flux2-sync.yaml", {
      git_repo_url    = "https://bitbucket.org/geoscienceaustralia/gnss-helm-releases.git"
      git_branch      = terraform.workspace == "dev" ? "master" : terraform.workspace
      git_path        = "helm-releases/${terraform.workspace}"
      flux_git_secret = kubernetes_secret.flux2_bitbucket_key.metadata[0].name
    })
  ]
}
