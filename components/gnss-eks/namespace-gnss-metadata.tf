module "namespace_gnss_metadata" {
  source              = "./modules/namespace"
  cluster_id          = local.cluster_id
  aws_role_policy_arn = aws_iam_policy.eks_user.arn
  namespace           = "gnss-metadata"
  admin_aws_nonprod_principals = [
    "arn:aws:iam::704132972316:root", # gnss-metadata-nonprod
  ]
  admin_aws_prod_principals = [
    "arn:aws:iam::484116167126:root", # gnss-metadata-prod
  ]
}
