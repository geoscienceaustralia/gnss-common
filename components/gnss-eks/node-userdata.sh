#!/usr/bin/env bash

# TODO: what is our tenable key?
# REGION=ap-southeast-2
# AWS_INSTANCE_ID=`curl -s http://169.254.169.254/latest/meta-data/instance-id`
# EC2_NAME=GNSS-$HOSTNAME
# echo Platform identified as Amazon Linux 2
# wget -O /tmp/NessusAgent.rpm https://s3-ap-southeast-2.amazonaws.com/golden-ami-stack-test-goldenamiconfigbucket-2m56fudqmxlj/NessusAgent.rpm
# rpm -ivh --force --nosignature /tmp/NessusAgent.rpm
# /opt/nessus_agent/sbin/nessuscli agent link \
#   --key=xxx \
#   --name=$EC2_NAME \
#   --cloud \
#   --groups=AgGrp_AWS_Linux_Servers_Continuous
# systemctl start nessusagent
# chkconfig nessusagent on
# enable ssm-agent

# shellcheck disable=SC2016
echo '${docker_config_json}' > /var/lib/kubelet/config.json

sudo systemctl enable amazon-ssm-agent
sudo systemctl start amazon-ssm-agent
