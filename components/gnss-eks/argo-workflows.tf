module "argo_workflows" {
  source = "./modules/argo-workflows"

  region          = var.region
  cluster_id      = local.cluster_id
  certificate_arn = module.eks.certificate_arn

  cluster_oidc_provider_name = local.cluster_oidc_provider_name
  cluster_oidc_provider_arn  = local.cluster_oidc_provider_arn

  domain_name = aws_route53_zone.eks.name
  namespace   = "ginan-ops"

  cognito_user_pool_id     = data.terraform_remote_state.cognito.outputs.gnss_user_pool.id
  cognito_user_pool_client = data.terraform_remote_state.cognito.outputs.gnss_users_argo_workflows_ginan_ops_client

  cognito_user_prod_pool_id      = data.terraform_remote_state.cognito_prod.outputs.gnss_user_pool.id
  cognito_user_prod_pool_clients = data.terraform_remote_state.cognito_prod.outputs.gnss_users_prod_argo_workflows_ginan_ops_clients
}
