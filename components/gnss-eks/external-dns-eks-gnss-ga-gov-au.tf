# External-dns installation for $env-eks.gnss.ga.gov.au

data "aws_iam_policy_document" "dns_access_trust_policy" {
  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]
    effect  = "Allow"

    principals {
      identifiers = [local.cluster_oidc_provider_arn]
      type        = "Federated"
    }

    condition {
      test     = "StringEquals"
      variable = "${local.cluster_oidc_provider_name}:sub"
      values   = ["system:serviceaccount:external-dns:external-dns-eks-gnss-ga-gov-au"]
    }
  }
}

resource "aws_iam_role" "dns_access" {
  name = "${local.cluster_id}-external-dns"
  path = "/"

  assume_role_policy = data.aws_iam_policy_document.dns_access_trust_policy.json
}

data "aws_iam_policy_document" "dns_access" {
  statement {
    actions = [
      "route53:ChangeResourceRecordSets",
    ]
    resources = [
      "arn:aws:route53:::hostedzone/${aws_route53_zone.eks.zone_id}",
    ]
  }

  statement {
    actions = [
      "route53:ListHostedZones",
      "route53:ListResourceRecordSets",
    ]
    resources = [
      "*",
    ]
  }
}

resource "aws_iam_role_policy" "dns_access" {
  role   = aws_iam_role.dns_access.id
  policy = data.aws_iam_policy_document.dns_access.json
}

data "template_file" "external_dns_eks_gnss_ga_gov_au" {
  template = file("${path.module}/external-dns.yaml")
  vars = {
    cluster_id          = local.cluster_id
    domain_name         = aws_route53_zone.eks.name
    service_account_arn = aws_iam_role.dns_access.arn
  }
}

resource "kubernetes_secret" "external_dns_eks_gnss_ga_gov_au" {
  metadata {
    name      = "external-dns-eks-gnss-ga-gov-au"
    namespace = kubernetes_namespace.external_dns.metadata[0].name
  }
  data = {
    "values.yaml" = data.template_file.external_dns_eks_gnss_ga_gov_au.rendered
  }
  type = "Opaque"
}
