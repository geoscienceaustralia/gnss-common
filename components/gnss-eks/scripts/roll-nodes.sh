#!/usr/bin/env bash

# Based on https://bitbucket.org/geoscienceaustralia/datakube/src/master/scripts/roll_instances.sh

# TODO: Fix strict mode
# set -euo pipefail

# shellcheck disable=SC2178,SC2128

# time to wait for deployments to be healthy
wait_limit=${1:-900} # default set to 15 mins
# nodes to patch in a single go
max_nodes=${2:-50} # default set to max 50 nodes

# Helper function that will return the name of the newest instance in the cluster
get-newest-instance-name(){
    name=$(kubectl get nodes --selector='!eks.amazonaws.com/nodegroup' -o custom-columns=":metadata.creationTimestamp,:metadata.name" --no-headers | sort -k1 -r | awk '{print $2}' | head -1)
    echo "$name"
}

# Helper function that will return the name of the oldest instance in the cluster
get-oldest-instance-name(){
    name=$(kubectl get nodes --selector='!eks.amazonaws.com/nodegroup' -o custom-columns=":metadata.name" --sort-by=.metadata.creationTimestamp --no-headers | head -1)
    echo "$name"
}

# Return oldest instance id
get-oldest-instance-id() {
    kubectl get nodes --selector='!eks.amazonaws.com/nodegroup' -o custom-columns=":metadata.labels.instance-id" \
        --sort-by=.metadata.creationTimestamp --no-headers \
        | head -1
}

# Helper function that will return the creationTimestamp of the oldest instnce timestamp in the cluster
get-oldest-instance-timestamp(){
    iso=$(kubectl get nodes --selector='!eks.amazonaws.com/nodegroup' -o custom-columns=":metadata.creationTimestamp" --sort-by=.metadata.creationTimestamp --no-headers | head -1)
    epoch=$(date -d"$iso" +%s)
    echo "$epoch"
}

# Checks all deployments are healthy
wait-for-deployments(){
    # Wait max 15 mins per nodes for deployments to be healthy
    max_wait=$wait_limit
    ready=false
    while [[ $max_wait -gt 0 ]] && [ $ready == false ]; do
        # Check if deployments are healthy

        ready=true
        # deployment: an array of all the deployment names
        mapfile -t deployment < <(kubectl get deployments --all-namespaces -o custom-columns=":metadata.name" --sort-by=.metadata.name --no-headers)
        # available: an array of all available replicas same order as deployment
        mapfile -t available < <(kubectl get deployments --all-namespaces -o custom-columns=":status.availableReplicas" --sort-by=.metadata.name --no-headers)
        # desired: an array of all desired replicas same order as deployment
        mapfile -t desired < <(kubectl get deployments --all-namespaces -o custom-columns=":status.replicas" --sort-by=.metadata.name --no-headers)

        count="${#deployment[@]}"
        for (( i=0; i<count; i++ )); do
            if ! [[ "${desired[$i]}" =~ ^[0-9]+$ ]]; then
                    echo "Warning: Deployment ${deployment[$i]} doesn't have any replicas"
                    continue
            fi
            if [ "${available[$i]}" -lt "${desired[$i]}" ]; then
                echo "Deployment ${deployment[$i]} not ready, desired pods: ${desired[$i]}, available pods: ${available[$i]}"
                ready=false
            fi
        done
        echo "Deployments ready: $ready"

        if [ $ready = false ]; then
            sleep 10
            max_wait=$((max_wait - 10))
            echo "Waited 10 seconds. Still waiting max. $max_wait"
        fi
    done
}

# From a node name, find the ASG the node is hosted in
find-asg(){
    instanceid=$(kubectl get node "$1" -o jsonpath='{.metadata.labels.instance-id}')
    asg=$(aws autoscaling describe-auto-scaling-instances --instance-ids "$instanceid" | jq -r '.AutoScalingInstances[0].AutoScalingGroupName')
    echo "$asg"
}

echo "Checking deployments are healthy"
wait-for-deployments

if [ $ready = false ]; then
    echo "Deployments not in healthy state"
    # ensure cluster autoscaler is back online
    kubectl scale deployment/cluster-autoscaler-aws-cluster-autoscaler -n admin --replicas 1
    exit 1
fi

start_time=$(date '+%s')

echo "Starting to patch at time: $start_time"

# ensure we have 2 dns pods running
kubectl scale deployments/coredns --replicas=2 -n kube-system
# disable cluster autoscaler as it messes with stuff
# TODO: Use autoscaler?
# kubectl scale deployment/cluster-autoscaler-aws-cluster-autoscaler -n admin --replicas 0

node_count=0

# Run until we have patched every node
until [ "$start_time" -lt "$(get-oldest-instance-timestamp)" ]; do

    newest_node=$(get-newest-instance-name)
    oldest_node=$(get-oldest-instance-name)
    oldest_instance_id=$(get-oldest-instance-id)

    echo "Draining oldest node: $oldest_node"

    asg=$(find-asg "$oldest_node")

    aws autoscaling detach-instances \
        --auto-scaling-group-name "$asg" \
        --instance-ids "$oldest_instance_id" \
        --no-should-decrement-desired-capacity

    # Wait for new node instance
    while true; do
        echo Waiting for new instance
        sleep 20
        node=$(get-newest-instance-name)
        if [ "$newest_node" != "$node" ]; then
            newest_node=$node
            break
        fi
    done

    # Wait until new node is ready
    while [[ $(kubectl get node "$newest_node" -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}') != "True" ]]; do
      echo "Waiting for new node to be ready: $newest_node" && sleep 1;
    done

    # Taint node with noschedule, then drain the pods off it, if something breaks move on
    kubectl cordon "$oldest_node" \
        && kubectl drain "$oldest_node" --delete-local-data --ignore-daemonsets --force \
        || echo "Warning: node could not be drained, continuing"

    # Wait until all deployments are healthy
    echo "Waiting for deployments to be healthy"
    wait-for-deployments

    # Remove the node from kubernetes (So we don't keep trying to remove the same node)
    kubectl delete node "$oldest_node"

    aws ec2 terminate-instances --instance-ids "$oldest_instance_id"

    node_count=$((node_count+1))
    if [ "$node_count" -ge "$max_nodes" ]; then
        echo "Patched max number of nodes, finishing"
        break
    fi
done

#ensure cluster autoscaler is back online
# kubectl scale deployment/cluster-autoscaler-aws-cluster-autoscaler -n admin --replicas 1
# TODO: Use autoscaler?
echo "Patching complete, patched $node_count nodes"
