#!/usr/bin/env bash

# This script uses `aws eks update-config...` to register cluster gnss-eks-dev with $KUBECONFIG.

set -euo pipefail

declare -A accountNumber
accountNumber[dev]=023072794443
accountNumber[test]=023072794443
accountNumber[prod]=334594953176

function usage {
    cat << EOF
Usage: init-kubeconfig.sh <environment> [--cached] [--role|assume-role <role>] [--profile <aws-cli profile>]
where
    environment is dev, test, or prod
    role is read-only, admin, ginan-ops-admin, gnss-data-admin, etc
    profile is not used if not supplied
EOF
}

role=
assumeRole=false
cached=false
while [[ $# -gt 0 ]]; do
    case $1 in
       dev|test|prod)
            environment=$1
            shift
            ;;
       --role|--assume-role)
            if [[ $1 = --assume-role ]]; then
                assumeRole=true
            fi
            role=$2
            shift 2
            ;;
       --profile)
            export AWS_PROFILE=$2
            shift 2
            ;;
       --cached)
            cached=true
            shift
            ;;
        *)
            echo "Unknown option: $1"
            usage
            exit 1
            ;;
    esac
done

if [[ -z ${environment+x} ]]; then
    echo "Unspecified environment"
    usage
    exit 1
fi

if $cached; then
    clusterId=gnss-eks-2-$environment
    clusterUserRoleArn=arn:aws:iam::${accountNumber[$environment]}:role/eks-$role.$clusterId
else
    export TF_DATA_DIR
    TF_DATA_DIR=$(mktemp -d)
    trap 'rm -rf "$TF_DATA_DIR"' EXIT

    terraformOutputs=$(
        eval "$(gnss-common-reader-role.sh -e "$environment" --assume)"
        scriptDir="$(dirname "${BASH_SOURCE[0]}")"
        cd "$scriptDir"/..
        terraform init -backend-config "./environments/$environment/backend.cfg" > /dev/null
        terraform workspace select "$environment" > /dev/null
        terraform output -json
    )

    clusterId=$(jq <<< "$terraformOutputs" -r '.cluster_id.value')
    clusterUserRoleArn=$(jq <<< "$terraformOutputs" -r ".cluster_${role//-/_}_role.value")
fi

if [[ -n $role ]]; then
    credentials=$(aws sts assume-role --role-arn "$clusterUserRoleArn" --role-session-name gnss-eks-user | jq '.Credentials')

    export AWS_ACCESS_KEY_ID
    export AWS_SECRET_ACCESS_KEY
    export AWS_SESSION_TOKEN

    AWS_ACCESS_KEY_ID=$(jq <<< "$credentials" '.AccessKeyId' -r)
    AWS_SECRET_ACCESS_KEY=$(jq <<< "$credentials" '.SecretAccessKey' -r)
    AWS_SESSION_TOKEN=$(jq <<< "$credentials" '.SessionToken' -r)

    if [[ $assumeRole = true ]]; then
        echo "export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID"
        echo "export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY"
        echo "export AWS_SESSION_TOKEN=$AWS_SESSION_TOKEN"
        exec &> /dev/null
    fi
fi

# Unset AWS_PROFILE so that `aws eks update-kubeconfig` doesn't capture the profile in the
# generated kubeconfig, but retain the region and the credentials (but only if the
# credentials are established by the profile).
if [[ -n ${AWS_PROFILE:-} ]]; then
    if [[ -z $role && -z ${AWS_ACCESS_KEY_ID:-} ]]; then
        if awsCreds=$(aws configure export-credentials --format env); then
            eval "$awsCreds"
        else
            echo >&2 "$awsCreds"
            exit 1
        fi
    fi
    region=$(aws configure get region || echo "")
    unset AWS_PROFILE
fi

aws ${region:+--region "$region"} eks update-kubeconfig --name "$clusterId" ${role:+--role-arn "$clusterUserRoleArn"} --alias "${clusterId}"

if [[ -n $role ]]; then
    echo "Using EKS access role $clusterUserRoleArn"
fi

if [[ -n ${KUBECONFIG:-} ]]; then
    chmod 600 "$KUBECONFIG"
else
    chmod 600 ~/.kube/config
fi
