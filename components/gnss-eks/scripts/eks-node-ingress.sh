#!/usr/bin/env bash

# Delete EKS node security group rules left behind by the AWS load balancer controller.
#
# There doesn't seem to be much info online.
#     *) https://github.com/kubernetes/kubernetes/issues/81610
#
# Find orphans by inspecting SG rule descriptions, which are of the form
#
#     kubernetes.io/rule/nlb/(client|health)=<load balancer id>
#
# and select rules that mention load balancers that no longer exist.
#
# Usage:
#     ./eks-node-ingress.sh (print-used | print-orphans | delete-orphans | restore <backup.json>) --env <env> [--dry-run]
#     
#     options
#         env,                     dev/test/prod
#         print-used,              print used ingress rules and exit
#         print-orphans,           print orphan ingress rules and exit
#         delete-orphans,          delete orphan ingress rules, save backup file
#         restore <backup.json>,   restore ingress rules from backup file
#         --dry-run,               print aws cli commands
#
# Examples:
#
#     ./eks-node-ingress.sh --env dev print-orphans  # same format as backup saved by delete-orphans
#     ./eks-node-ingress.sh --env dev delete-orphans --dry-run
#     ./eks-node-ingress.sh --env dev delete-orphans # saves backup to gnss-eks-2-dev-node-sg-orphans-backup-<timestemp>.json
#     ./eks-node-ingress.sh --env dev restore gnss-eks-2-dev-node-sg-orphans-backup-<timestamp>.json
#
# You can manually cross reference output of
#
#     ./eks-node-ingress.sh print-orphans --env dev | jq -r '.SecurityGroupRules[].ToPort' | sort | uniq > aws-orphans
#
# with 
#
#     kubectl get service --all-namespaces -o json | jq '.items[].spec.ports[].nodePort | select(. != null)' | sort > k8s-used
#
# using
#
#     join aws-orphans k8s-used
#
# which should print nothing, if intersection of aws-orphans and k8s-used is empty, as it should be.
#
# Note that the above kubectl command returns more ports than `eks-node-ingress.sh print-used`,
# because not all access is controlled via the node security group. Some load balancers have
# dedicated security groups. NLBs support dedicated security groups as of Auguest 2023, see
# https://aws.amazon.com/blogs/containers/network-load-balancers-now-support-security-groups/.

set -euo pipefail

printUsed=0
printOrphans=0
deleteOrphans=0
restore=0
dryRun=
env=
while [ $# -gt 0 ]; do
    case $1 in
        print-used)
            printUsed=1
            shift
            ;;
        print-orphans)
            printOrphans=1
            shift
            ;;
        delete-orphans)
            deleteOrphans=1
            shift
            ;;
        restore)
            restore=1
            backupFile=$2
            shift 2
            ;;
        -d|--dry-run)
            dryRun=true
            shift
            ;;
        --env)
            env=$2
            shift 2
            ;;
        *)
            shift 1
            ;;
    esac
done

if ((printUsed + printOrphans + deleteOrphans + restore != 1)); then
    echo "You must specify exactly one of print-used, print-orphans, delete-orphans, restore"
    exit 1
fi

if [[ ! $env =~ ^(dev|test|prod)$ ]]; then
    echo "You must specify --env (dev|test|prod)"
    exit 1
fi

# Restore from backup
if (( restore == 1 )); then
    # Convert backup file to json requests to `authorize-security-group-ingress` cli command
    readarray -t restoreRequests < <(jq -c '.SecurityGroupRules[] | {
        "GroupId": .GroupId,
        "IpPermissions": [{
            "IpProtocol": .IpProtocol,
            "FromPort": .FromPort,
            "ToPort": .ToPort,
            "IpRanges": [{
                "CidrIp": .CidrIpv4,
                "Description": .Description
            }]
        }]
    }' < "$backupFile")

    # Restore
    for restoreRequest in "${restoreRequests[@]}"; do
        ${dryRun:+echo} aws ec2 authorize-security-group-ingress --cli-input-json "$restoreRequest"
    done

    exit 0
fi

groupName=gnss-eks-2-$env-node-sg

# Load balancer names
readarray -t loadBalancers < <(aws elbv2 describe-load-balancers | jq -r .LoadBalancers[].LoadBalancerName)

# Node security group id
groupId=$(aws ec2 describe-security-groups \
    --filters "Name=group-name,Values=$groupName" \
    | jq -r '.SecurityGroups[0].GroupId'
)

# Node security group ingress rules
allRules=$(aws ec2 describe-security-group-rules --filters "Name=group-id,Values=$groupId" --no-paginate)

# Used or orphaned rules
selectedRules=$(jq --arg orphans "$((printOrphans + deleteOrphans))" --arg loadBalancers "${loadBalancers[*]}" -r '
    .SecurityGroupRules
    |= map(select(
        .Description // "" | capture("kubernetes.io/rule/nlb/(client|health)=(?<lb>.*)") | .lb as $lb
        | $loadBalancers | contains($lb) | if $orphans == "1" then not else . end
    ))' <<< "$allRules"
)

# Print
if (( printUsed + printOrphans == 1 )); then
    echo "$selectedRules"
# or delete
elif (( deleteOrphans == 1 )); then
    if [[ -z $dryRun ]]; then
        echo "$selectedRules" > "$groupName-orphans-backup-$(date +'%s%N').json"
    fi
    for orphan in $(jq -r '.SecurityGroupRules[].SecurityGroupRuleId' <<< "$selectedRules"); do
        ${dryRun:+echo} aws ec2 revoke-security-group-ingress --group-id "$groupId" --security-group-rule-ids "$orphan"
    done
fi
