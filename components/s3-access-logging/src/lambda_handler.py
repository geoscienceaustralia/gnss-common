import boto3
import re
import os
import requests
from datetime import datetime
from requests_aws4auth import AWS4Auth

region = os.environ["REGION"]
credentials = boto3.Session().get_credentials()
s3 = boto3.client("s3")
awsauth = AWS4Auth(
    credentials.access_key,
    credentials.secret_key,
    region,
    "es",
    session_token=credentials.token,
)

host = os.environ["ELASTICSEARCH_ENDPOINT"]
index_prefix = os.environ["ELASTICSEARCH_INDEX_NAME_PREFIX"]
headers = {"Content-Type": "application/json"}


def lambda_handler(event, context):
    print(event)
    es_url = host + "/" + index_prefix + datetime.now().strftime("-%Y-%m") + "/_doc"
    print(es_url)

    for record in event["Records"]:
        bucket, key = record["s3"]["bucket"]["name"], record["s3"]["object"]["key"]
        obj = s3.get_object(Bucket=bucket, Key=key)
        body = obj["Body"].read()

        for line in body.splitlines():
            document = parse_s3_log(line.decode("utf-8"))
            resp = requests.post(es_url, auth=awsauth, json=document, headers=headers)
            print(resp.status_code, resp.text)
            resp.raise_for_status()


def parse_s3_log(log):
    s3_log_regex = '([^ ]*) ([^ ]*) \\[(.*?)\\] ([^ ]*) ([^ ]*) ([^ ]*) ([^ ]*) ([^ ]*) (-|\\".*\\") (-|[0-9]*) ([^ ]*) ([^ ]*) ([^ ]*) ([^ ]*) ([^ ]*) ([^ ]*) (-|"[^"]*") ([^ ]*)(?: ([^ ]*) ([^ ]*) ([^ ]*) ([^ ]*) ([^ ]*) ([^ ]*))?.*$'
    s3_log_keys = [
        "bucket_owner",
        "bucket",
        "@timestamp",
        "remote_ip",
        "requester",
        "request_id",
        "operation",
        "key",
        "request",
        "http_status",
        "error_code",
        "bytes_sent",
        "object_size",
        "total_time",
        "turnaround_time",
        "referrer",
        "user_agent",
        "version_id",
        "host_id",
        "sigv",
        "cipher_suite",
        "auth_type",
        "endpoint",
        "tls_version",
    ]
    log_pattern = re.compile(s3_log_regex)

    match = log_pattern.match(log)
    values = match.groups(0)

    document = dict(zip(s3_log_keys, values))
    document["@timestamp"] = datetime.strptime(document["@timestamp"], "%d/%b/%Y:%H:%M:%S +0000").isoformat()
    return document


def test_parse_s3_log():
    example_log_line = '451dcf38e6ae44dbec44ca1793feedaa6d90d16cb84dfee808a43648ba701b10 auscors-sitelogs-converted-prod [14/Dec/2021:23:19:48 +0000] 52.64.209.106 arn:aws:sts::623223935732:assumed-role/auscors-sitelogs_iam_role_prod/auscors-sitelogs-lambda-prod DZPQNHTY2SHJN36W REST.PUT.OBJECT tltn_20211214.log "PUT /tltn_20211214.log HTTP/1.1" 200 - - 9335 56 29 "-" "Boto3/1.17.42 Python/2.7.18 Linux/4.14.246-198.474.amzn2.x86_64 exec-env/AWS_Lambda_python2.7 Botocore/1.20.42" - 0FLVTM2ZGh7PRN720TzMUld/Ge60wr8qEOhzIcRDog5nSgM+N7KDP5SrnZEFdUQS1UMJAKAmv44= SigV4 ECDHE-RSA-AES128-GCM-SHA256 AuthHeader auscors-sitelogs-converted-prod.s3.ap-southeast-2.amazonaws.com TLSv1.2 -'
    example_log_dict = {
        "bucket_owner": "451dcf38e6ae44dbec44ca1793feedaa6d90d16cb84dfee808a43648ba701b10",
        "bucket": "auscors-sitelogs-converted-prod",
        "@timestamp": "2021-12-14T23:19:48",
        "remote_ip": "52.64.209.106",
        "requester": "arn:aws:sts::623223935732:assumed-role/auscors-sitelogs_iam_role_prod/auscors-sitelogs-lambda-prod",
        "request_id": "DZPQNHTY2SHJN36W",
        "operation": "REST.PUT.OBJECT",
        "key": "tltn_20211214.log",
        "request": '"PUT /tltn_20211214.log HTTP/1.1"',
        "http_status": "200",
        "error_code": "-",
        "bytes_sent": "-",
        "object_size": "9335",
        "total_time": "56",
        "turnaround_time": "29",
        "referrer": '"-"',
        "user_agent": '"Boto3/1.17.42 Python/2.7.18 Linux/4.14.246-198.474.amzn2.x86_64 exec-env/AWS_Lambda_python2.7 Botocore/1.20.42"',
        "version_id": "-",
        "host_id": "0FLVTM2ZGh7PRN720TzMUld/Ge60wr8qEOhzIcRDog5nSgM+N7KDP5SrnZEFdUQS1UMJAKAmv44=",
        "sigv": "SigV4",
        "cipher_suite": "ECDHE-RSA-AES128-GCM-SHA256",
        "auth_type": "AuthHeader",
        "endpoint": "auscors-sitelogs-converted-prod.s3.ap-southeast-2.amazonaws.com",
        "tls_version": "TLSv1.2",
    }
    assert parse_s3_log(example_log_line) == example_log_dict


def test_parse_s3_log_no_request_uri():
    example_log_line = "bd078aaa0310aa22c1139686257f3377b99d4d4ec7a6cf0a7e66f7c4b25ef205 geodesy-archive-permanent-storage-dev [16/Dec/2021:06:57:54 +0000] 10.49.10.87 arn:aws:sts::688660191997:assumed-role/gnss-data-dev-rinex-file-s3-projection-1/aws-sdk-java-1639636692445 JPG0ZZ9FZTH7BFW0 REST.COPY.OBJECT_GET SystemTest-81af2b5b-c728-46b5-bd8a-f31be65ee244/bala3080.17d.gz - 200 - - 938477 - - - - - hM8FLYsSWEUaTNIq8Fup0r3UV9pW7y1FYLqVbMu28voTP0w0TDxcbyNVRRuwloFSW72m5aa0NjM= SigV4 ECDHE-RSA-AES128-GCM-SHA256 AuthHeader ga-gnss-data-rinex-v1-dev.s3.ap-southeast-2.amazonaws.com TLSv1.2 -"
    example_log_dict = {
        "bucket_owner": "bd078aaa0310aa22c1139686257f3377b99d4d4ec7a6cf0a7e66f7c4b25ef205",
        "bucket": "geodesy-archive-permanent-storage-dev",
        "@timestamp": "2021-12-16T06:57:54",
        "remote_ip": "10.49.10.87",
        "requester": "arn:aws:sts::688660191997:assumed-role/gnss-data-dev-rinex-file-s3-projection-1/aws-sdk-java-1639636692445",
        "request_id": "JPG0ZZ9FZTH7BFW0",
        "operation": "REST.COPY.OBJECT_GET",
        "key": "SystemTest-81af2b5b-c728-46b5-bd8a-f31be65ee244/bala3080.17d.gz",
        "request": "-",
        "http_status": "200",
        "error_code": "-",
        "bytes_sent": "-",
        "object_size": "938477",
        "total_time": "-",
        "turnaround_time": "-",
        "referrer": "-",
        "user_agent": "-",
        "version_id": "-",
        "host_id": "hM8FLYsSWEUaTNIq8Fup0r3UV9pW7y1FYLqVbMu28voTP0w0TDxcbyNVRRuwloFSW72m5aa0NjM=",
        "sigv": "SigV4",
        "cipher_suite": "ECDHE-RSA-AES128-GCM-SHA256",
        "auth_type": "AuthHeader",
        "endpoint": "ga-gnss-data-rinex-v1-dev.s3.ap-southeast-2.amazonaws.com",
        "tls_version": "TLSv1.2",
    }
    assert parse_s3_log(example_log_line) == example_log_dict
