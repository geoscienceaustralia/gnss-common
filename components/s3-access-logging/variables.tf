variable "region" {}

variable "elasticsearch_endpoint" {
  default = "https://search-gnss-elasticsearch-prod-5omhch5quzlu5dcpbct4ev5qz4.ap-southeast-2.es.amazonaws.com"
}

variable "elasticsearch_index_name_prefix" {
  type        = string
  default     = "s3-access-logs"
  description = "Elasticsearch index name which is suffixed with -YYYY.MM - does not need to be different per project because the log entries include bucket name"
}

variable "resource_name_prefix" {
  type        = string
  description = "Prepended to the names of resources created by this module - e.g. project name might be an appropriate value"
}

variable "push_enabled" {
  type        = bool
  default     = true
  description = "If false, don't push S3 access logs to Elasticsearch"
}
