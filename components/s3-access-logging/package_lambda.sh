#!/usr/bin/env bash

set -eou pipefail

cd "$(dirname "$(realpath "$0")")"/src

python -m venv env

# shellcheck source=/dev/null
. env/bin/activate

mkdir -p lambda_package
pip install -r requirements.txt --target ./lambda_package
cp lambda_handler.py lambda_package/

deactivate
rm -rf env/
