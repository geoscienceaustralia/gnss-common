## S3 Access Logging into Elasticsearch

Terraform module for pushing S3 Access Logs into Elasticsearch (AWS OpenSearch).

This module creates an S3 bucket which other S3 buckets can be configured to submit
their "server logs" to (`logging` attribute of the `s3_bucket` terraform resource).
S3 logs are then picked up by a Lambda function which parses and publishes them to
Elasticsearch.

The default Elasticsearch endpoint variable is the gnss-common Elasticsearch cluster.
The default index name prefix should also be left as default in most cases, because
the documents contain the source bucket name which are already globally unique.

#### Example Usage

```
module "s3_logs_to_elasticsearch" {
    source = "git@bitbucket.org:geoscienceaustralia/gnss-common//components/s3-access-logging"
    region = var.region

    resource_name_prefix = "${var.application}-${var.environment}"
}
```

Configure S3 bucket logging for the bucket from which you want to collects logs:

```
resource "aws_s3_bucket_logging" "bucket" {
  bucket        = aws_s3_bucket.bucket.id
  target_bucket = module.s3_logs_to_elasticsearch.log_bucket_id
  target_prefix = "${var.some_identifying_prefix}/"
}
```

Note that `target_prefix` isn't strictly required but might be preferred for neatness.
