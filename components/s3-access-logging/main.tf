resource "aws_s3_bucket" "logs" {
  bucket = "${var.resource_name_prefix}-s3-access-logs"
}

resource "aws_s3_bucket_acl" "logs" {
  bucket = aws_s3_bucket.logs.id
  acl    = "log-delivery-write"
}

resource "aws_s3_bucket_server_side_encryption_configuration" "logs" {
  bucket = aws_s3_bucket.logs.id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_iam_role" "lambda" {
  name = "${var.resource_name_prefix}-s3-access-logs"

  assume_role_policy = <<-EOF
    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Action": "sts:AssumeRole",
          "Principal": {
            "Service": "lambda.amazonaws.com"
          },
          "Effect": "Allow"
        }
      ]
    }
  EOF
}

resource "aws_iam_role_policy" "logging" {
  name = "${var.resource_name_prefix}-s3-access-logs"
  role = aws_iam_role.lambda.id

  policy = <<-EOF
    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Effect": "Allow",
          "Action": [
            "logs:CreateLogGroup",
            "logs:CreateLogStream",
            "logs:PutLogEvents"
          ],
          "Resource": [
            "arn:aws:logs:*:*:*"
          ]
        },
        {
          "Effect": "Allow",
          "Action": "s3:GetObject",
          "Resource": "${aws_s3_bucket.logs.arn}/*"
        },
        {
          "Effect": "Allow",
          "Action": "es:ESHttpPost",
          "Resource": "arn:aws:es:*:*:*"
        }
      ]
    }
  EOF
}

locals {
  lambda_package     = "${path.module}/src/lambda_package"
  lambda_zip_package = "${path.module}/src/lambda_package.zip"
}

resource "null_resource" "package_lambda" {
  triggers = {
    now = timestamp()
  }

  provisioner "local-exec" {
    command = "bash ${path.module}/package_lambda.sh"
  }
}

data "archive_file" "lambda_package" {
  depends_on  = [null_resource.package_lambda]
  type        = "zip"
  source_dir  = local.lambda_package
  output_path = local.lambda_zip_package
}

resource "aws_lambda_function" "log_handler" {
  filename         = data.archive_file.lambda_package.output_path
  function_name    = "${var.resource_name_prefix}-s3-access-logs"
  role             = aws_iam_role.lambda.arn
  handler          = "lambda_handler.lambda_handler"
  source_code_hash = data.archive_file.lambda_package.output_base64sha256
  runtime          = "python3.9"
  timeout          = 15

  environment {
    variables = {
      REGION                          = var.region
      ELASTICSEARCH_ENDPOINT          = var.elasticsearch_endpoint
      ELASTICSEARCH_INDEX_NAME_PREFIX = var.elasticsearch_index_name_prefix
    }
  }
}

resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.log_handler.arn
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.logs.arn
}

resource "aws_s3_bucket_notification" "bucket_notification" {
  count  = var.push_enabled ? 1 : 0
  bucket = aws_s3_bucket.logs.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.log_handler.arn
    events              = ["s3:ObjectCreated:*"]
  }

  depends_on = [aws_lambda_permission.allow_bucket]
}
