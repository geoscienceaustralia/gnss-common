terraform {
  backend "s3" {
    encrypt = true
  }
}

locals {
  prefix = "${var.system}-${terraform.workspace}"
}

provider "aws" {
  region      = var.region
  max_retries = 10
}

data "aws_caller_identity" "current" {}

resource "aws_s3_bucket" "artifactory" {
  bucket = local.prefix
  acl    = "private"
}

resource "aws_s3_bucket_ownership_controls" "artifactory" {
  bucket = aws_s3_bucket.artifactory.id

  rule {
    object_ownership = "BucketOwnerEnforced"
  }
}

resource "aws_s3_bucket_policy" "artifactory" {
  bucket = aws_s3_bucket.artifactory.id

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Principal = {
          AWS = concat(var.nonprod_aws_principals, var.prod_aws_principals)
        }
        Action = [
          "s3:ListBucket",
          "s3:GetObject*",
          "s3:PutObject",
          "s3:PutObjectAcl"
        ]
        Resource = [
          aws_s3_bucket.artifactory.arn,
          "${aws_s3_bucket.artifactory.arn}/*",
        ]
      },
    ]
  })
}
