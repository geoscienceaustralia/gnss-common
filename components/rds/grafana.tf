resource "random_password" "grafana_database_password" {
  length  = 16
  special = false
}

module "create_grafana_user" {
  source = "./modules/psql"

  name          = "create-grafana-user"
  k8s_namespace = "prometheus"
  rds_endpoint  = module.rds.db_instance_endpoint
  username      = "master"
  password      = data.external.rds_master_credentials.result.password
  database      = "postgres"

  script = <<-EOF
    select 'create user grafana'
        where not exists (select from pg_user where usename = 'grafana')\\gexec

    alter user grafana with password '${random_password.grafana_database_password.result}';
    
    grant pg_read_all_data to grafana;
    alter user grafana set search_path to geodesy,public;
  EOF
}

resource "kubernetes_secret" "grafana_datasource" {
  metadata {
    name      = "grafana-datasource-postgresql"
    namespace = "prometheus"
    labels = {
      grafana_datasource = "1"
    }
  }

  data = {
    "postgresql-datasource.yaml" = <<-EOF
      apiVersion: 1
      datasources:
        - name: PostgreSQL
          type: postgres
          url: ${module.rds.db_instance_endpoint}
          user: grafana
          secureJsonData:
            password: '${random_password.grafana_database_password.result}'
          jsonData:
            database: geodesy
            sslmode: 'disable'
            postgresVersion: 1400
    EOF
  }
}
