terraform {
  backend "s3" {
    encrypt = true
  }
}

provider "aws" {
  region      = var.region
  max_retries = 10
}

provider "kubernetes" {
  config_path = "~/.kube/config"
}

data "aws_availability_zones" "available" {}

locals {
  name = "gnss-rds-${terraform.workspace}"
}

module "vpc" {
  source = "git@github.com:terraform-aws-modules/terraform-aws-vpc.git?ref=v3.18.1"

  name             = local.name
  cidr             = var.vpc_cidr
  azs              = data.aws_availability_zones.available.names
  database_subnets = var.database_subnet_cidrs

  create_database_subnet_route_table = true

  enable_dns_hostnames = true
  enable_dns_support   = true
  enable_nat_gateway   = false
}

module "rds_sg" {
  source = "git@github.com:terraform-aws-modules/terraform-aws-security-group//modules/postgresql?ref=v4.16.2"

  name                = "${local.name}-postgresql"
  vpc_id              = module.vpc.vpc_id
  ingress_cidr_blocks = [var.vpc_cidr]
}

module "rds" {
  source = "git@github.com:terraform-aws-modules/terraform-aws-rds.git?ref=v5.2.0"

  identifier             = local.name
  create_db_instance     = var.create_db_instance
  engine                 = "postgres"
  instance_class         = var.db_instance_type
  allocated_storage      = 100
  username               = data.external.rds_master_credentials.result.username
  password               = aws_ssm_parameter.rds_master_password.value
  create_random_password = false
  port                   = "5432"

  iam_database_authentication_enabled = true

  create_db_subnet_group = true
  vpc_security_group_ids = [module.rds_sg.security_group_id]

  multi_az = terraform.workspace == "prod"

  maintenance_window      = "Mon:17:30-Mon:18:00"
  backup_window           = "03:00-06:00"
  backup_retention_period = 14

  # Creates an IAM role that permits RDS to send enhanced
  # monitoring metrics to CloudWatch
  monitoring_interval    = "30"
  monitoring_role_name   = "${local.name}-monitoring"
  create_monitoring_role = true

  subnet_ids           = module.vpc.database_subnets
  family               = "postgres${var.db_major_engine_version}"
  major_engine_version = var.db_major_engine_version
  deletion_protection  = var.create_db_instance
}
