#!/usr/bin/env bash

set -euo pipefail

jq -n \
   --arg password "$(openssl rand -base64 32 | tr -dc a-zA-Z0-9'\+\=')" \
   '{"username": "master", "password": $password}'

