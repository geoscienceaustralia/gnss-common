variable "region" {
  description = "The AWS region we are going to create these resources in"
}

variable "nonprod_aws_principals" {
  type = list(string)
}

variable "prod_aws_principals" {
  type = list(string)
}

variable "vpc_cidr" {
  type    = string
  default = "10.0.0.0/16"
}

variable "database_subnet_cidrs" {
  description = "List of private database cidrs, for all availability zones."
  type        = list(string)
  default     = ["10.0.21.0/24", "10.0.22.0/24", "10.0.23.0/24"]
}

variable "create_db_instance" {
  type    = bool
  default = true
}

variable "db_major_engine_version" {
  type    = string
  default = "14"
}

variable "db_instance_type" {
  type    = string
  default = "db.t3.small"
}
