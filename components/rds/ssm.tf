resource "aws_ssm_parameter" "rds_endpoint" {
  count = var.create_db_instance ? 1 : 0
  name  = "/${local.name}/endpoint"
  type  = "String"
  value = module.rds.db_instance_endpoint
}

resource "aws_ssm_parameter" "rds_master_password" {
  name  = "/${local.name}/user/master/password"
  type  = "SecureString"
  value = data.external.rds_master_credentials.result.password
}

data "external" "rds_master_credentials" {
  program = ["bash", "${path.module}/generate-rds-credentials.sh"]
}
