#!/usr/bin/env bash

export PGHOST=${PGHOST}
export PGPORT=${PGPORT}
export PGDATABASE=${PGDATABASE}
export PGUSER=${PGUSER}
export PGPASSWORD=${PGPASSWORD}

psql -v ON_ERROR_STOP=on << EOF
${SCRIPT}
EOF
