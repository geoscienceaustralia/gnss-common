Run a k8s job to execute arbitrary SQL code against the shared RDS instance.

The shared PostgreSQL RDS instance configured in `components/rds` is not externally accessible.
However, containers running in the shared k8s cluster (`components/gnss-eks`) are able to access
the RDS instance using a peering connection that exists between the EKS and RDS VPCs.

Example:

```terraform
data "aws_ssm_parameter" "rds_endpoint" {
  provider = aws.gnss_common
  name     = "/gnss-rds-dev/endpoint"
}

data "aws_ssm_parameter" "rds_master_password" {
  provider        = aws.gnss_common
  name            = "/gnss-rds-dev/user/master/password"
  with_decryption = "true"
}

module "create_database" {
  source = "git@bitbucket.org:geoscienceaustralia/gnss-common//components/rds/modules/psql?ref=master"

  name          = "create-database"
  k8s_namespace = "gnss-metadata"
  rds_endpoint  = data.aws_ssm_parameter.rds_endpoint.value
  username      = "master"
  password      = data.aws_ssm_parameter.rds_master_password.value
  database      = "postgres"

  script = <<-EOF
    create database metadata;
    ...
  EOF
}
```
