variable "name" {
  type        = string
  description = "Kubernetes job name"
}

variable "k8s_namespace" {
  type        = string
  description = "Kubernetes namespace in which to create the job"
}

variable "rds_endpoint" {
  type        = string
  description = "RDS endpoint to pass to psql, e.g., gnss-rds-dev.cw6nydmsclws.ap-southeast-2.rds.amazonaws.com:5432"
}

variable "username" {
  type        = string
  description = "Username to pass to psql"
}

variable "password" {
  type        = string
  sensitive   = true
  description = "Password to pass to psql"
}

variable "database" {
  type        = string
  description = "Database name to pass to psql"
}

variable "script" {
  type        = string
  sensitive   = true
  description = "SQL code to pass to psql"
}
