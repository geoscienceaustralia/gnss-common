locals {
  script = templatefile("${path.module}/script.sh", {
    PGHOST     = split(":", var.rds_endpoint)[0]
    PGPORT     = split(":", var.rds_endpoint)[1]
    PGUSER     = var.username
    PGPASSWORD = var.password
    PGDATABASE = var.database
    SCRIPT     = var.script
  })
}

resource "kubernetes_secret" "script" {
  metadata {
    name      = "${var.name}-sh"
    namespace = var.k8s_namespace
    labels = {
      app = var.name
    }
  }

  data = {
    "script.sh" = local.script
  }
}

resource "kubernetes_job" "script" {
  metadata {
    # Force re-create when script changes
    name      = substr("${var.name}-${sha256(local.script)}", 0, 63)
    namespace = var.k8s_namespace
    labels = {
      app = var.name
    }
  }

  spec {
    template {
      metadata {
        labels = {
          app = var.name
        }
      }

      spec {
        restart_policy = "Never"

        container {
          image   = "bitnami/postgresql:15.1.0"
          name    = "main"
          command = ["/usr/local/bin/script.sh"]

          resources {
            limits = {
              cpu    = "10m"
              memory = "64Mi"
            }

            requests = {
              cpu    = "10m"
              memory = "32Mi"
            }
          }

          volume_mount {
            name       = "script-sh"
            mount_path = "/usr/local/bin"
          }
        }

        volume {
          name = "script-sh"
          secret {
            secret_name  = kubernetes_secret.script.metadata[0].name
            default_mode = "0777"
            items {
              key  = "script.sh"
              path = "script.sh"
            }
          }
        }
      }
    }
  }

  wait_for_completion = true
}
