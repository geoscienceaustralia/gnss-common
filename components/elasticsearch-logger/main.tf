resource "aws_iam_role" "elasticsearch_logger" {
  name = "${var.resource_name_prefix}-elasticsearch-lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF

}

resource "aws_iam_role_policy" "elasticsearch_logger" {
  name = "${var.resource_name_prefix}-elasticsearch-logger"
  role = aws_iam_role.elasticsearch_logger.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": [
        "arn:aws:logs:*:*:*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": "es:ESHttpPost",
      "Resource": "arn:aws:es:*:*:*"
    }
  ]
}
EOF
}

locals {
  lambda_source_file = "${path.module}/src/elasticSearchLogger.js"
  lambda_zip_package = "${path.module}/src/lambda_package.zip"
}

data "archive_file" "lambda_package" {
  type        = "zip"
  source_file = local.lambda_source_file
  output_path = local.lambda_zip_package
}

resource "aws_lambda_function" "elasticsearch_logger" {
  depends_on = [data.archive_file.lambda_package]

  filename         = file(local.lambda_zip_package)
  function_name    = "${var.resource_name_prefix}-elasticsearch-logger"
  role             = aws_iam_role.elasticsearch_logger.arn
  handler          = "elasticSearchLogger.handler"
  source_code_hash = filebase64sha256(local.lambda_zip_package)
  runtime          = "nodejs12.x"

  environment {
    variables = {
      elasticsearch_endpoint = var.elasticsearch_endpoint
      index_name_prefix      = var.elasticsearch_index_name_prefix
    }
  }
}

resource "aws_lambda_permission" "log_group" {
  statement_id  = "${var.resource_name_prefix}-elasticsearch-log-group"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.elasticsearch_logger.arn
  principal     = "logs.${var.region}.amazonaws.com"
}

output "lambda_arn" {
  value = aws_lambda_function.elasticsearch_logger.arn
}
