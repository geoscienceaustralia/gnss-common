## Elasticsearch Logger for Cloudwatch Log Groups

Terraform module for streaming Cloudwatch logs into Elasticsearch indexes.

This module creates a Lambda function which parses Cloudwatch log messages and sends 
them to a specific index in a given Elasticsearch cluster. The Cloudwatch Log Group 
Subscription Filter which invokes the log submission lambda function must be created 
outside of this module (see Example Usage).

The index name is generated as `{elasticsearch_index_name_prefix}-YYYY.MM.DD`. 

#### Example Usage

```
module "elasticsearch_logger" {
  source               = "https://github.com/geoscienceaustralia/gnss-common/components/elasticsearch-logger"
  region               = var.region
  resource_name_prefix = "${var.system}-${terraform.workspace}"

  elasticsearch_endpoint          = var.elasticsearch_endpoint
  elasticsearch_index_name_prefix = "${var.system}-${terraform.workspace}"
}
```

Stream logs from a Lambda log group:

```
# Lambda will automatically create it's own log group, but we need to apply a subscription filter 
# to the log group before the lambda has had a chance to create it, so we create the log group first

resource "aws_cloudwatch_log_group" "some_lambda_log_group" {
  name = "/aws/lambda/${aws_lambda_function.some_lambda.function_name}"
}
  
resource "aws_cloudwatch_log_subscription_filter" "some_lambda_log_group_subscription_filter" {
  name            = "${var.application}-some-lambda-log-group-subscription-filter-${terraform.workspace}"
  destination_arn = module.elasticsearch_logger.lambda_arn
  log_group_name  = aws_cloudwatch_log_group.submit_command_handler_log_group.name
  filter_pattern  = var.log_group_filter_pattern
}
```

AWS Docs for filter patterns: https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/FilterAndPatternSyntax.html

This example is from the GNSS Data Repository:

```
"[level=INFO || level=ERROR, correlation_id, principal_id, tenant_id, thread, logger=au.gov.ga.*, message]"
```
