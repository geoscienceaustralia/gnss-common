# Domain Policy provides cross account access to Elasticsearch
data "aws_iam_policy_document" "cluster_access" {
  statement {
    actions = ["es:ESHttp*"]

    principals {
      type        = "AWS"
      identifiers = concat(var.nonprod_aws_principals, var.prod_aws_principals)
    }
  }
}

resource "aws_elasticsearch_domain_policy" "cluster" {
  domain_name     = aws_elasticsearch_domain.cluster.domain_name
  access_policies = data.aws_iam_policy_document.cluster_access.json
}
