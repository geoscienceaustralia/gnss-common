variable "elasticsearch_endpoint" {
  type        = string
  description = "host"
  default     = "search-gnss-elasticsearch-prod-5omhch5quzlu5dcpbct4ev5qz4.ap-southeast-2.es.amazonaws.com"
}

variable "index_prefix" {
  description = "Elasticsearch index prefix, e.g., 'gnss-data' will generate indices like 'gnss-data-2020-02'"
  type        = string
}

variable "namespace" {
  description = <<-EOF
    Fluentd-elasticsearch will be deployed into this namespace and it will watch container logs in
    this namespace.
  EOF

  type = string
}

variable "pod" {
  description = <<-EOF
    Identify containers to watch, default is all containers in all pods in the given namespace.
  EOF

  type    = string
  default = null
}

variable "line_pattern" {
  description = <<-EOF
    Log lines to match. Lines not matching this pattern will be appended to the most recently
    matched log lines.

    Line pattern

    ^\[(?<time>[^\]]*)\] \[(?<level>[^\]]*)\] \[(?<logger>[^\]]*)\] (?<message>.*)

    will add 'time', 'level', 'logger', and 'message' fields to the document sent to
    Elasticsearch. Field 'time' is uninterpreted, the document @timestamp field will
    instead be parsed from the docker log event timestamp.
  EOF

  type = string
}

variable "date_format" {
  description = <<-EOF
    Format of Elasticsearch index name date suffix - e.g., %Y.%m
  EOF

  type    = string
  default = "%Y.%m"
}

variable "types" {
  description = <<-EOF
    Specify types of parsed fields.

    By default, fluentd will parse fields as strings. For example, given the configuration

    types = {
      "count": "integer",
      "ratio": "float"
    }

    the 'count' and 'ratio' fields will be parsed as an integer and float, respectively.
  EOF

  type    = map(string)
  default = {}

  validation {
    condition     = alltrue([for type in values(var.types) : length(regexall("string|bool|integer|float|time|array", type)) > 0])
    error_message = "Fluentd field types supported are 'string', 'bool', 'integer', 'float', 'time', and 'array'."
  }
}

variable "exclude_fields" {
  description = <<-EOF
    Exclude fields from Elasticsearch payload
  EOF

  type    = list(string)
  default = []
}

variable "filter" {
  description = <<-EOF
    Optionally apply further filters to fields defined in line_pattern.
    
    Given the line_pattern above, the filter

    filter = {
      fields   = ["level", "logger"],
      patterns = [
        [".*", "WARN|ERROR"],
        ["au\\.gov\\.ga\\.*", "INFO"],
      ]
    }

    Will restrict matched log events to warnings and errors for all loggers
    (including 'au.gov.ga') and info messages only for loggers under 'au.gov.ga'.
  EOF

  type = object({
    fields : list(string),
    patterns : list(list(string)),
  })

  default = {
    fields   = []
    patterns = []
  }
}

variable "resources" {
  type = object({
    fluentd = object({
      memory_request = string
      memory_limit   = string
      cpu_request    = string
    })
    sidecar = object({
      memory_request = string
      memory_limit   = string
      cpu_request    = string
    })
  })

  default = {
    fluentd = {
      memory_request = "64Mi"
      memory_limit   = "1024Mi"
      cpu_request    = "10m"
    }
    sidecar = {
      memory_request = "32Mi"
      memory_limit   = "64Mi"
      cpu_request    = "10m"
    }
  }
}

variable "read_from_head" {
  type    = bool
  default = false
}
