# An installation of Fluentd-elasticsearch to forward kubernetes log events to Elasticsearch.
# See variables.tf for usage.

resource "helm_release" "elasticsearch" {
  namespace  = var.namespace
  name       = "fluentd-elasticsearch-${var.namespace}" # namespace is required to prevent ClusterRole collision
  repository = "https://kokuwaio.github.io/helm-charts"
  chart      = "fluentd-elasticsearch"
  version    = "13.5.0"

  # This force_update seems to have the effect of re-creating the fluentd-elasticsearch daemonset,
  # which can otherwise be partially patched.
  # It may be related to https://github.com/helm/helm/issues/1844 or a bunch of other similar issues.
  force_update = true

  values = [
    templatefile("${path.module}/values.yaml", {
      elasticsearch_endpoint = var.elasticsearch_endpoint
      index_prefix           = var.index_prefix
      date_format            = var.date_format
      namespace              = var.namespace
      pod                    = var.pod != null ? var.pod : "*"
      resources              = var.resources

      line_pattern    = trimspace(var.line_pattern)
      filter_fields   = join(" ", [for field in var.filter.fields : "$${record[\"${field}\"]}"])
      filter_patterns = [for pattern in var.filter.patterns : "(${join(") (", pattern)})"]
      types           = join(",", [for key, type in var.types : join(":", [key, type])])
      exclude_fields  = join(",", var.exclude_fields)
      read_from_head  = var.read_from_head
    })
  ]
}
