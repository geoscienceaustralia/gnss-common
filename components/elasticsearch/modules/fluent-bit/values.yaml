diagnosticMode:
  enabled: false

daemonset:
  enabled: true

command: [ "fluent-bit" ]
args:
  # Verbose logging
  # - -v
  - -c
  - /opt/bitnami/fluent-bit/conf/fluent-bit.conf
  - -R
  - /opt/bitnami/fluent-bit/conf/parsers.conf
  - -R
  - /opt/bitnami/fluent-bit/conf/custom_parsers.conf

containerSecurityContext:
  enabled: false

serviceAccount:
  annotations:
      eks.amazonaws.com/role-arn: arn:aws:iam::334594953176:role/gnss-elasticsearch-eks-full-access-prod

resources:
  requests:
    cpu: ${resources.requests.cpu}
    memory: ${resources.requests.memory}
  limits:
    cpu: ${resources.limits.cpu}
    memory: ${resources.limits.memory}

config:
  logLevel: info
  inputs: |
    [INPUT]
        Name tail
        Tag kube.*
        Path /var/log/containers/${pod}-*_${namespace}_*.log
        Exclude_Path /var/log/containers/fluent-bit-elasticsearch-*
        Multiline.Parser cri
        DB /var/log/flb_kube_${namespace}.db
        Read_From_Head ${read_from_head}
        Mem_Buf_Limit 5MB
        Skip_Long_Lines On
        Refresh_Interval 10

  filters: |
    # Attach k8s fields, e.g., kubernetes_namespace, kubernetes_container
    [FILTER]
        Name kubernetes
        Match kube.*
        Merge_Log On

    # TODO: Multiline parsing is conditional, because it is slow. It could be faster if made
    # part of tail, rather than a separate filter as is now. We should try to make this change
    # after migrating k8s runtime to containerd, at which point that might be eaiser because logs
    # will be straight text, rather than json.
    %{ if multiline }

    # Combine multiline messages into one
    [FILTER]
        Name                  multiline
        Match                 kube.*
        Multiline.Key_Content log
        Multiline.Parser      multiline

    %{ endif }

    # Parse the log entry, e.g., time, level, thread id, message, etc.
    [FILTER]
        Name parser
        Match kube.*
        Key_Name log
        Parser message
        Preserve_Key True
        Reserve_Data False

    %{ if length(filter_fields) > 0 }

    # Add temporary field __filter
    [FILTER]
        Name lua
        Match kube.*
        Script filters.lua
        Call add_filter_field

    # Grep for filter patterns
    [FILTER]
        Name grep
        Match kube.*
        Logical_Op or
    %{ for pattern in filter_patterns }
        Regex __filter ^${pattern}$
    %{ endfor }

    # Remove temporary field __filter
    [FILTER]
        Name modify
        Match kube.*
        Remove __filter

    %{ endif }

    %{ if length(exclude_fields) > 0 }

    # Remove temporary excluded fields
    [FILTER]
        Name modify
        Match kube.*
    %{ for field in exclude_fields }
        Remove ${field}
    %{ endfor }

    %{ endif }

  outputs: |
    # Send output to Opensearch
    [OUTPUT]
        Name  opensearch
        Match *
        Host  ${elasticsearch_endpoint}
        Port 80
        Logstash_Format On
        Logstash_Prefix ${index_prefix}
        Logstash_DateFormat ${date_format}
        AWS_Auth On
        AWS_Region ap-southeast-2

  customParsers: |
    # Parse log entry
    [PARSER]
        Name message
        Format regex
        Regex ${line_pattern}
        %{ if length(types) > 0 }
        Types ${types}
        %{ endif }

    # Combine multiple log entries into one
    [MULTILINE_PARSER]
        Name          multiline
        Type          regex
        Flush_Timeout 1000
        Rule          "start_state" "${line_pattern}$" "cont"
        Rule          "cont"        "^[^\[].*"         "cont"

  extraFiles:
    "filters.lua": |
      -- Concatenate filter patterns into new field `__filter`.
      function add_filter_field(tag, ts, record)
        record.__filter = ""
        %{ for f in filter_fields }
        if record.${f} ~= nil then
          record.__filter = record.__filter .. " " .. record.${f}
        end
        %{ endfor }
        %{ if length(filter_fields) > 0 }
          -- Trim leading space
          record.__filter = record.__filter:sub(2)
        %{ endif }
        return 1, ts, record
      end

extraVolumeMounts:
  - mountPath: /opt/bitnami/fluent-bit/conf/filters.lua
    name: config
    subPath: filters.lua
