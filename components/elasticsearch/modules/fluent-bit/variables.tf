variable "namespace" {
  description = <<-EOF
    Fluent-bit-elasticsearch will be deployed into this namespace and it will watch container logs in
    this namespace.
  EOF

  type = string
}

variable "pod" {
  description = <<-EOF
    Identify containers to watch, default is all containers in all pods in the given namespace.
  EOF

  type    = string
  default = null
}

variable "elasticsearch_endpoint" {
  description = "Just the host, no scheme and no port."
  type        = string
  default     = "search-gnss-elasticsearch-prod-5omhch5quzlu5dcpbct4ev5qz4.ap-southeast-2.es.amazonaws.com"
}

variable "index_prefix" {
  description = "Elasticsearch index prefix, e.g., 'gnss-data' will generate indices like 'gnss-data-2020-02'."
  type        = string
}

variable "date_format" {
  description = <<-EOF
    Format of Elasticsearch index name date suffix - e.g., %Y.%m
  EOF

  type    = string
  default = "%Y.%m"
}

variable "line_pattern" {
  description = <<-EOF
    Log lines to match. Lines not matching this pattern will be appended to the most recently
    matched log lines.

    Line pattern

    ^\[(?<time>[^\]]*)\] \[(?<level>[^\]]*)\] \[(?<logger>[^\]]*)\] (?<message>.*)

    will add 'time', 'level', 'logger', and 'message' fields to the document sent to
    Elasticsearch. Field 'time' is uninterpreted, the document @timestamp field will
    instead be parsed from the docker log event timestamp.
  EOF

  type = string
}

variable "types" {
  description = <<-EOF
    Specify types of parsed fields.

    By default, all fields as strings. For example, given the configuration

    types = {
      "count": "integer",
      "ratio": "float"
    }

    the 'count' and 'ratio' fields will be parsed as an integer and float, respectively.
  EOF

  type    = map(string)
  default = {}

  validation {
    condition     = alltrue([for type in values(var.types) : length(regexall("string|bool|integer|float|time|array", type)) > 0])
    error_message = "Field types supported are 'string', 'bool', 'integer', 'float', 'time', and 'array'."
  }
}


variable "filter" {
  description = <<-EOF
    Optionally apply further filters to fields defined in line_pattern.
    
    Given the line_pattern above, the filter

    filter = {
      fields   = ["level", "logger"],
      patterns = [
        [".*", "WARN|ERROR"],
        ["au\\.gov\\.ga\\.*", "INFO"],
      ]
    }

    Will restrict matched log events to warnings and errors for all loggers
    (including 'au.gov.ga') and info messages only for loggers under 'au.gov.ga'.
  EOF

  type = object({
    fields : list(string),
    patterns : list(list(string)),
  })
  default = {
    fields   = []
    patterns = []
  }
}

variable "exclude_fields" {
  description = <<-EOF
   Exclude fields from Elasticsearch payload
  EOF

  type    = list(string)
  default = []
}

variable "multiline" {
  description = "Join multiline log messages into one."
  type        = bool
  default     = true
}

variable "read_from_head" {
  description = "Don't catch-up on any log messages missed during restarts and downtimes."
  type        = bool
  default     = false
}

variable "resources" {
  description = "Fluent-bit pod resource specification."

  type = object({
    requests = object({
      cpu    = string
      memory = string
    })
    limits = object({
      cpu    = string
      memory = string
    })
  })

  default = {
    requests = {
      cpu    = "20m"
      memory = "20Mi"
    }
    limits = {
      cpu    = "200m"
      memory = "100Mi"
    }
  }
}
