# Use Fluent Bit to parse and forward k8s logs to Elasticsearch.

locals {
  name = "fluent-bit-elasticsearch"
}

resource "helm_release" "elasticsearch" {
  namespace  = var.namespace
  name       = local.name
  repository = "oci://registry-1.docker.io"
  chart      = "bitnamicharts/fluent-bit"
  version    = "0.6.4"

  # This force_update seems to have the effect of re-creating the fluentd-elasticsearch daemonset,
  # which can otherwise be partially patched.
  # It may be related to https://github.com/helm/helm/issues/1844 or a bunch of other similar issues.
  force_update = true

  values = [
    templatefile("${path.module}/values.yaml", {
      pod                    = var.pod != null ? var.pod : "*"
      namespace              = var.namespace
      elasticsearch_endpoint = var.elasticsearch_endpoint
      index_prefix           = var.index_prefix
      date_format            = var.date_format

      line_pattern    = chomp(var.line_pattern)
      types           = join(" ", [for key, type in var.types : join(":", [key, type])])
      filter_fields   = var.filter.fields
      filter_patterns = [for p in var.filter.patterns : join(" ", p)]
      exclude_fields  = var.exclude_fields
      read_from_head  = var.read_from_head
      multiline       = var.multiline

      resources = var.resources
    })
  ]

  depends_on = [
    kubernetes_role_binding_v1.fluent_bit_sa
  ]
}

resource "kubernetes_role_v1" "fluent_bit" {
  metadata {
    name      = local.name
    namespace = var.namespace
  }
  rule {
    api_groups = ["*"]
    resources  = ["*"]
    verbs      = ["get", "list"]
  }
}

resource "kubernetes_role_binding_v1" "fluent_bit_sa" {
  metadata {
    name      = "${local.name}-sa"
    namespace = var.namespace
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = kubernetes_role_v1.fluent_bit.metadata[0].name
  }
  subject {
    kind      = "ServiceAccount"
    name      = local.name # must match service account in helm chart
    namespace = var.namespace
  }
}
