terraform {
  backend "s3" {
    encrypt = true
  }
}

provider "aws" {
  region      = var.region
  max_retries = 10
}

resource "aws_elasticsearch_domain" "cluster" {
  domain_name           = "${var.system}-${terraform.workspace}"
  elasticsearch_version = "7.10"

  depends_on = [aws_iam_role.cognito]

  cluster_config {
    instance_type          = "r5.large.elasticsearch"
    instance_count         = 3
    zone_awareness_enabled = true
    zone_awareness_config {
      availability_zone_count = 3
    }
  }

  ebs_options {
    ebs_enabled = true
    volume_size = 350
    volume_type = "gp3"
    iops        = 3000
    throughput  = 250
  }

  snapshot_options {
    automated_snapshot_start_hour = 23
  }

  log_publishing_options {
    cloudwatch_log_group_arn = aws_cloudwatch_log_group.cluster_logs.arn
    log_type                 = "ES_APPLICATION_LOGS"
  }

  cognito_options {
    enabled          = true
    user_pool_id     = local.user_pool.id
    identity_pool_id = aws_cognito_identity_pool.identities.id
    role_arn         = aws_iam_role.cognito.arn
  }
}

resource "aws_cloudwatch_log_group" "cluster_logs" {
  name = "${var.system}-cluster-logs-${terraform.workspace}"
}

resource "aws_cloudwatch_log_resource_policy" "cluster_logs" {
  policy_name = "${var.system}-cluster-logs-${terraform.workspace}"

  policy_document = <<CONFIG
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "es.amazonaws.com"
      },
      "Action": [
        "logs:PutLogEvents",
        "logs:PutLogEventsBatch",
        "logs:CreateLogStream"
      ],
      "Resource": "arn:aws:logs:*"
    }
  ]
}
CONFIG
}

output "elasticsearch_endpoint" {
  value = aws_elasticsearch_domain.cluster.endpoint
}
