variable "region" {
  description = "The AWS region we are going to create these resources in"
}

variable "nonprod_aws_principals" {
  type = list(string)
}

variable "prod_aws_principals" {
  type = list(string)
}

variable "iam_oidc_providers" {
  description = "IAM OpenId Connect Provider ARNs"
  type        = list(string)
}

variable "system" {
  default = "gnss-elasticsearch"
}

variable "gnss_common_reader_role_prod_arn" {
  type = string
}
