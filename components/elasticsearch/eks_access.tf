resource "aws_iam_role" "eks_access" {
  name               = "${var.system}-eks-full-access-${terraform.workspace}"
  assume_role_policy = data.aws_iam_policy_document.eks_access.json
}

data "aws_caller_identity" "current" {}

data "aws_iam_policy_document" "eks_access" {
  dynamic "statement" {
    for_each = var.iam_oidc_providers
    content {
      actions = ["sts:AssumeRoleWithWebIdentity"]
      effect  = "Allow"

      principals {
        identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:oidc-provider/${statement.value}"]
        type        = "Federated"
      }
    }
  }
}

resource "aws_iam_role_policy" "eks_access" {
  name = "es-full-access"
  role = aws_iam_role.eks_access.id

  policy = <<-EOF
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": [
          "es:ESHttp*"
        ],
        "Effect": "Allow",
        "Resource": "*"
      }
    ]
  }
  EOF
}
