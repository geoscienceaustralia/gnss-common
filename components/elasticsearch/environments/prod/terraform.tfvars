iam_oidc_providers = [
  # TODO: remove once the switch to gnss-eks-2 is complete
  "oidc.eks.ap-southeast-2.amazonaws.com/id/3F779F6937D8225208A888B1C9167B26", # gnss-eks-dev
  "oidc.eks.ap-southeast-2.amazonaws.com/id/26296AB6894BE1DAE52BDDDC7EEAE8C1", # gnss-eks-test
  "oidc.eks.ap-southeast-2.amazonaws.com/id/0BD10FC87FFE6885DA3D7785B7C6084F", # gnss-eks-prod

  "oidc.eks.ap-southeast-2.amazonaws.com/id/131FFE77EE3721BCB81CFBAD9A69F644", # gnss-eks-2-dev
  "oidc.eks.ap-southeast-2.amazonaws.com/id/F7D2C0041F66C989451C7851B2FB7FC4", # gnss-eks-2-test
  "oidc.eks.ap-southeast-2.amazonaws.com/id/BA7EE02FBA25889FE1EB95C62EC1318F", # gnss-eks-2-prod
]
