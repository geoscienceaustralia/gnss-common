variable "region" {
}

variable "nonprod_aws_principals" {
  type = list(string)
}

variable "prod_aws_principals" {
  type = list(string)
}
