locals {
  prefix = "gnss-common-${terraform.workspace}"
}

module "reader_role" {
  source = "git@github.com:JamesWoolfenden/terraform-aws-cross-account-role?ref=v0.2.17"

  role_name = "${local.prefix}-reader"

  policy_arns = [
    data.aws_iam_policy.read_only.arn
  ]

  principal_arns = concat(var.nonprod_aws_principals, var.prod_aws_principals)

  common_tags = {
    name = "${local.prefix}-reader"
  }
}

data "aws_iam_policy" "read_only" {
  arn = "arn:aws:iam::aws:policy/ReadOnlyAccess"
}
