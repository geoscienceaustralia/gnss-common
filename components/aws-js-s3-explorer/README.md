# AWS JS S3 Explorer

Adds bucket explorer webpage as index.html in the root of a specified S3 bucket.

Source from https://github.com/awslabs/aws-js-s3-explorer.

## Example Usage

```
module "explorer" {
    source = "git@bitbucket.org:geoscienceaustralia/gnss-common//components/aws-js-s3-explorer"

    bucket_id = aws_s3_bucket.some_bucket.id
    prefix    = "public/"
}
```
