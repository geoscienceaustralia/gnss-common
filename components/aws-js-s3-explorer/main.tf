locals {
  index_html = "${path.module}/index.html"
}

resource "aws_s3_object" "index_html" {
  bucket       = var.bucket_id
  key          = "index.html"
  content_type = "text/html"
  etag         = md5(file(local.index_html))

  content = templatefile(local.index_html, {
    bucket = var.bucket_id,
    prefix = var.prefix
  })
}
