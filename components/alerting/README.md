### Alerting Feed

Query the Kibana Alerting /alert API and return Alerts as an Atom feed.

Considering hosting at gnss.ga.gov.au/feed or something similar. Which means
the source of this component may end up moving depending on how it is deployed.

So long as we format our Alerts in Kibana consistently this should serve as a
good way to keep users informed about issues without sending emails when streams
go down or data files are missing. Though, email alerts are still useful for
issues / events which are known about ahead of time.

It's also possible to modify the query params to include the `search` parameter,
and use this to get a history of alerts for a given site - which may be useful
to embed in the GNSS Portal.

It doesn't seem possible to add additional context to Alerts as they are
acknowledged - however, I believe acknowledgement should be enough to show users
that we are on top of issues.

Example API response:

```json
{
  "ok": true,
  "alerts": [
    {
      "id": "QLDjQ3EBFdkaxXJDYair",
      "monitor_id": "K4tHQ3EBatr-v2ENvbDs",
      "schema_version": 1,
      "monitor_version": 2,
      "monitor_name": "WALH00AUS0 - Stream",
      "trigger_id": "oK5PQ3EBFdkaxXJDG0TX",
      "trigger_name": "Completeness < 95%",
      "state": "ACTIVE",
      "error_message": null,
      "alert_history": [],
      "severity": "1",
      "action_execution_results": [],
      "start_time": 1585981907230,
      "last_notification_time": 1585984607275,
      "end_time": null,
      "acknowledged_time": null,
      "version": 46
    },
    {
      "id": "vK-BQ3EBFdkaxXJDgxQT",
      "monitor_id": "bK5HQ3EBFdkaxXJD1SVy",
      "schema_version": 1,
      "monitor_version": 2,
      "monitor_name": "XMIS00AUS0 - Stream",
      "trigger_id": "mItPQ3EBatr-v2ENM9Bv",
      "trigger_name": "Latency > 2 Seconds",
      "state": "COMPLETED",
      "error_message": null,
      "alert_history": [],
      "severity": "1",
      "action_execution_results": [],
      "start_time": 1585975493354,
      "last_notification_time": 1585978973207,
      "end_time": 1585979033238,
      "acknowledged_time": null,
      "version": 1
    },
    {
      "id": "0a5NQ3EBFdkaxXJD9j7k",
      "monitor_id": "vYtFQ3EBatr-v2ENuaaF",
      "schema_version": 1,
      "monitor_version": 2,
      "monitor_name": "ALIC00AUS0 - Stream",
      "trigger_id": "VotNQ3EBatr-v2ENGMU0",
      "trigger_name": "Completeness < 95%",
      "state": "ACKNOWLEDGED",
      "error_message": null,
      "alert_history": [],
      "severity": "1",
      "action_execution_results": [],
      "start_time": 1585972115003,
      "last_notification_time": 1585976615113,
      "end_time": null,
      "acknowledged_time": 1585976635230,
      "version": 77
    },
        ...
```

Converted to Atom Feed:

```xml
<?xml version="1.0" encoding="UTF-8"?><feed xmlns="http://www.w3.org/2005/Atom">
  <title>GA GNSS Alerts</title>
  <id>https://gnss.ga.gov.au</id>
  <updated>2020-04-04T17:39:38+11:00</updated>
  <subtitle>Geoscience Australia GNSS Alerts</subtitle>
  <link href="https://gnss.ga.gov.au"></link>
  <author>
    <name>GA</name>
    <email>gnss@ga.gov.au</email>
  </author>
  <entry>
    <title>WALH00AUS0 - Stream Completeness &lt; 95%!</title>
    <updated>2020-04-04T06:38:47Z</updated>
    <id>tag:gnss.ga.gov.au,2020-04-04:/</id>
    <link href="http://gnss.ga.gov.au/" rel="alternate"></link>
    <summary type="html">ACTIVE,2020-04-04T06:31:47Z,</summary>
  </entry>
  <entry>
    <title>XMIS00AUS0 - Stream Latency &gt; 2 Seconds</title>
    <updated>2020-04-04T05:42:53Z</updated>
    <id>tag:gnss.ga.gov.au,2020-04-04:/</id>
    <link href="http://gnss.ga.gov.au/" rel="alternate"></link>
    <summary type="html">COMPLETED,2020-04-04T04:44:53Z,</summary>
  </entry>
  <entry>
    <title>ALIC00AUS0 - Stream Latency &gt; 2 Seconds</title>
    <updated>2020-04-04T05:03:35Z</updated>
    <id>tag:gnss.ga.gov.au,2020-04-04:/</id>
    <link href="http://gnss.ga.gov.au/" rel="alternate"></link>
    <summary type="html">ACKNOWLEDGED,2020-04-04T03:48:35Z,2020-04-04T05:03:55Z</summary>
  </entry>
        ...
```

TODO: Consider including target recovery time based off of Alert severity
mapping to CORS Tiers (using our target response times). Severities 1 through 4
are available.

TODO: Implement [request signing for AWS Elasticsearch](https://docs.aws.amazon.com/elasticsearch-service/latest/developerguide/es-request-signing.html#es-request-signing-go).

TODO: Could host in [AWS Lambda](https://docs.aws.amazon.com/lambda/latest/dg/golang-handler.html)
or in Kubernetes as a container running something like the following:

```go
func FeedHandler(w http.ResponseWriter, r *http.Request) {
	feed, err := GenerateFeed()
	if err != nil {
		log.Print(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	fmt.Fprintf(w, feed)
}

func main() {
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/feed", FeedHandler)
	log.Fatal(http.ListenAndServe(":8080", router))
}
```
