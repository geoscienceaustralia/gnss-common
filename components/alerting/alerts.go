package alerting

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"

	"github.com/gorilla/feeds"
)

// Alert represents an individual Alert from the Kibana Alerting API
type Alert struct {
	ID                   string   `json:"id"`
	MonitorID            string   `json:"monitor_id"`
	SchemaVersion        int      `json:"schema_version"`
	MonitorVersion       int      `json:"monitor_version"`
	MonitorName          string   `json:"monitor_name"`
	TriggerID            string   `json:"trigger_id"`
	TriggerName          string   `json:"trigger_name"`
	State                string   `json:"state"`
	ErrorMessage         string   `json:"error_message"`
	AlertHistory         []string `json:"alert_history"`
	Severity             string   `json:"severity"`
	StartTime            int64    `json:"start_time"`
	LastNotificationTime int64    `json:"last_notification_time"`
	EndTime              int64    `json:"end_time"`
	AcknowledgedTime     int64    `json:"acknowledged_time"`
	Version              int      `json:"version"`
}

// Alerts represents the collection of Alert objects returned by the Kibana
// Alerting API /alerts endpoint
type Alerts struct {
	Status      bool    `json:"status"`
	Alerts      []Alert `json:"alerts"`
	TotalAlerts int     `json:"totalAlerts"`
}

// AsFeedItems returns the list of Alert objects as a list of feeds.Item
// TODO: Don't return completed Alerts older than a given date
func (alerts Alerts) AsFeedItems() (items []*feeds.Item) {
	for _, alert := range alerts.Alerts {
		if alert.State == "DELETED" { // TODO: || alert.State == "COMPLETED" {
			continue
		}

		// TODO: Consider appropriate description content
		description := fmt.Sprintf("%v,%v,", alert.State,
			time.Unix(alert.StartTime/1000, 0).UTC().Format(time.RFC3339))

		if alert.AcknowledgedTime != 0 {
			description += time.Unix(alert.AcknowledgedTime/1000, 0).UTC().Format(time.RFC3339)
		}

		items = append(items, &feeds.Item{
			Id:          alert.ID,
			Title:       fmt.Sprintf("%v %v", alert.MonitorName, alert.TriggerName),
			Link:        &feeds.Link{Href: "http://gnss.ga.gov.au/"}, // TODO: ?
			Description: description,
			Updated:     time.Unix(alert.LastNotificationTime/1000, 0).UTC(),
			Created:     time.Unix(alert.StartTime/1000, 0).UTC(),
		})
	}
	return items
}

var (
	// HTTPClient is exported so it can be overwritten by unit tests
	// TODO: Consider other ways to achieve this
	HTTPClient *http.Client = http.DefaultClient
)

// GetAlerts queries Kibana Alerting API for list of Alerts
func GetAlerts(elasticsearchEndpoint string) (alerts Alerts, err error) {
	// TODO: Parameterize query parameters
	u := url.URL{
		Scheme:   "https",
		Host:     elasticsearchEndpoint,
		Path:     "_plugin/kibana/api/alerting/alerts",
		RawQuery: "alertState=ALL&from=0&severityLevel=ALL&size=100&sortDirection=desc&sortField=start_time",
		//Host:     "search-gnss-elasticsearch-prod-5omhch5quzlu5dcpbct4ev5qz4.ap-southeast-2.es.amazonaws.com",
	}

	resp, err := HTTPClient.Get(u.String())
	if err != nil {
		return alerts, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return alerts, err
	}

	err = json.Unmarshal(body, &alerts)

	return alerts, err
}

// GenerateAtomFeed returns an Atom feed constructed using Alerts
func GenerateAtomFeed(alerts Alerts) (atom string, err error) {
	feed := &feeds.Feed{
		Title:       "Geoscience Australia GNSS Alerts",
		Link:        &feeds.Link{Href: "https://gnss.ga.gov.au"}, // TODO: ?
		Description: "",                                          // TODO: ?
		Author:      &feeds.Author{Name: "GA", Email: "gnss@ga.gov.au"},
		Created:     time.Now(),
		Items:       alerts.AsFeedItems(),
	}

	return feed.ToAtom()
}
