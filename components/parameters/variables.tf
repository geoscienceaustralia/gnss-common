variable "region" {
  description = "The AWS region we are going to create these resources in"
}

variable "nonprod_aws_principals" {
  type = list(string)
}

variable "prod_aws_principals" {
  type = list(string)
}
