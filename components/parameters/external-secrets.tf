resource "kubernetes_manifest" "cluster_secret_store_parameters" {
  manifest = {
    "apiVersion" = "external-secrets.io/v1beta1"
    "kind"       = "ClusterSecretStore"
    "metadata" = {
      "name" = "parameters"
    }
    "spec" = {
      "provider" = {
        "aws" = {
          "role"    = aws_iam_role.parameter_store_read_only.arn
          "region"  = var.region
          "service" = "ParameterStore"
        }
      }
    }
  }
}
