Read-only cross-account access role for gnss-common-nonprod/prod Parameter Store

Parameters should follow formatting rules described in [GNSS Guildelines](https://bitbucket.org/geoscienceaustralia/gnss-guidelines/src/master/).

Prod systems should never require access to non-prod secrets. Non-prod systems
must not be given access to gnss-common-prod parameter store. Access from
non-prod systems to prod systems should be carefully considered, and where this
is necessary, the secret should be duplicated into non-prod Parameter Store.

Access to this role is granted to all accounts listed in [common.tfvars](/common.tfvars).

TODO: Consider using tags to reduce scope of read-only policy (for example KMS
Decrypt * is probably too permissive) - see [AWS examples](https://docs.aws.amazon.com/systems-manager/latest/userguide/sysman-paramstore-access.html).

TODO: Consider better directory structure for this component - feels very messy
at the moment with go and terraform files sharing the same space
