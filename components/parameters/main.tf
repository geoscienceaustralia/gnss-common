terraform {
  backend "s3" {
    encrypt = true
  }
}

provider "aws" {
  region      = var.region
  max_retries = 10
}

provider "kubernetes" {
  config_path = "~/.kube/config"
}

locals {
  component = "parameter-store-${terraform.workspace}"
}

data "aws_iam_policy_document" "assume_parameter_store_read_only_role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "AWS"
      identifiers = concat(var.nonprod_aws_principals, var.prod_aws_principals)
    }
  }
}

resource "aws_iam_role" "parameter_store_read_only" {
  name               = "${local.component}-read-only"
  assume_role_policy = data.aws_iam_policy_document.assume_parameter_store_read_only_role.json
}

data "aws_iam_policy_document" "parameter_store_read_only" {
  statement {
    actions = [
      "ssm:DescribeParameters",
      "ssm:GetParameters",
      "ssm:GetParameter"
    ]
    resources = ["*"]
  }

  statement {
    actions = [
      "kms:Decrypt"
    ]
    resources = ["*"]
  }
}

resource "aws_iam_policy" "parameter_store_read_only" {
  name   = "${local.component}-read-only"
  policy = data.aws_iam_policy_document.parameter_store_read_only.json
}

resource "aws_iam_role_policy_attachment" "parameter_store_read_only" {
  role       = aws_iam_role.parameter_store_read_only.name
  policy_arn = aws_iam_policy.parameter_store_read_only.arn
}

output "parameter_store_read_only_role_arn" {
  value = aws_iam_role.parameter_store_read_only.arn
}
