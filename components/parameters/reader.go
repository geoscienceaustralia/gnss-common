package parameters

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ssm"
)

var (
	ReaderRoleARN = map[string]string{
		"dev":  "arn:aws:iam::023072794443:role/parameter-store-dev-read-only",
		"test": "arn:aws:iam::023072794443:role/parameter-store-test-read-only",
		"prod": "arn:aws:iam::334594953176:role/parameter-store-prod-read-only",
	}
)

// ParameterStoreReader wraps a Session to assume a Read Only role
// for accessing gnss-common SSM Parameters
type ParameterStoreReader struct {
	Session *session.Session
}

func NewParameterStoreReader(env string) ParameterStoreReader {
	s := session.Must(session.NewSessionWithOptions(
		session.Options{
			Config: aws.Config{
				CredentialsChainVerboseErrors: aws.Bool(true),
			},
			SharedConfigState: session.SharedConfigEnable,
		},
	))

	if env != "" {
		s = session.Must(session.NewSession(&aws.Config{
			Region:      aws.String("ap-southeast-2"),
			Credentials: stscreds.NewCredentials(s, ReaderRoleARN[env]),
		}))
	}
	return ParameterStoreReader{Session: s}
}

func (p ParameterStoreReader) GetParameter(name string, decrypt bool) (param *ssm.GetParameterOutput, err error) {
	return ssm.New(p.Session).GetParameter(&ssm.GetParameterInput{
		Name:           aws.String(name),
		WithDecryption: aws.Bool(decrypt),
	})
}

func (p ParameterStoreReader) GetParameters() (value *map[string]string, err error) {
	client := ssm.New(p.Session)
	describeOutput, err := client.DescribeParameters(&ssm.DescribeParametersInput{})
	if err != nil {
		return nil, err
	}
	names := make([]*string, len(describeOutput.Parameters))
	for i, param := range describeOutput.Parameters {
		names[i] = param.Name
	}
	paramsOutput, err := client.GetParameters(&ssm.GetParametersInput{
		Names:          names,
		WithDecryption: aws.Bool(true),
	})
	if err != nil {
		return nil, err
	}
	values := make(map[string]string)
	for _, param := range paramsOutput.Parameters {
		values[*param.Name] = *param.Value
	}
	return &values, nil
}
