terraform {
  required_version = ">= 0.14.11, < 2.0.0"
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
    external = {
      source = "hashicorp/external"
    }
  }
}
