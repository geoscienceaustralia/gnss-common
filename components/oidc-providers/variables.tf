variable "region" {
  type = string
}

variable "gnss_common_reader_role_dev_arn" {
  type = string
}

variable "gnss_common_reader_role_test_arn" {
  type = string
}

variable "gnss_common_reader_role_prod_arn" {
  type = string
}

variable "gnss_eks_dev_cluster_id" {
  type = string
}

variable "gnss_eks_test_cluster_id" {
  type = string
}

variable "gnss_eks_prod_cluster_id" {
  type = string
}
