#!/usr/bin/env bash

TF_VAR_gnss_common_reader_role_dev_arn=$(gnss-common-reader-role.sh -e dev)
export TF_VAR_gnss_common_reader_role_dev_arn

TF_VAR_gnss_common_reader_role_test_arn=$(gnss-common-reader-role.sh -e test)
export TF_VAR_gnss_common_reader_role_test_arn

TF_VAR_gnss_common_reader_role_prod_arn=$(gnss-common-reader-role.sh -e prod)
export TF_VAR_gnss_common_reader_role_prod_arn

export TF_VAR_gnss_eks_dev_cluster_id=gnss-eks-2-dev
export TF_VAR_gnss_eks_test_cluster_id=gnss-eks-2-test
export TF_VAR_gnss_eks_prod_cluster_id=gnss-eks-2-prod
