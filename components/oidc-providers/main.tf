terraform {
  backend "s3" {}
}

provider "aws" {
}

provider "aws" {
  alias = "dev"

  assume_role {
    role_arn = var.gnss_common_reader_role_dev_arn
  }
}

provider "aws" {
  alias = "test"

  assume_role {
    role_arn = var.gnss_common_reader_role_test_arn
  }
}

provider "aws" {
  alias = "prod"

  assume_role {
    role_arn = var.gnss_common_reader_role_prod_arn
  }
}

module "gnss_eks_dev_oidc_provider" {
  source = "../gnss-eks/modules/oidc-provider"

  providers = {
    aws.source = aws.dev
    aws.target = aws
  }

  region     = var.region
  cluster_id = var.gnss_eks_dev_cluster_id
}

module "gnss_eks_test_oidc_provider" {
  source = "../gnss-eks/modules/oidc-provider"

  providers = {
    aws.source = aws.test
    aws.target = aws
  }

  region     = var.region
  cluster_id = var.gnss_eks_test_cluster_id
}

module "gnss_eks_prod_oidc_provider" {
  source = "../gnss-eks/modules/oidc-provider"

  providers = {
    aws.source = aws.prod
    aws.target = aws
  }

  region     = var.region
  cluster_id = var.gnss_eks_prod_cluster_id
}
