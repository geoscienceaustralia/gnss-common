## Cognito Auth

Shared Authentication and Authorization system for GA GNSS applications.

Applications should create their own App Client and Groups in the User Pool.

Currently the only custom registration page for this is https://gnss.ga.gov.au/registration.
This page is managed by the Flying Hellfish team. It is used for registration
to the NTRIP caster. The registration confirmation email specifically mentions
the NTRIP caster system, but when we set this up for the Data Repository / Metadata
System, we will need to modify the wording in that email (or have different emails via a
Lambda function - however, there are far fewer users of these systems).

This is a way to get a list of confirmed user email addresses created through the
UI (for notification emails, for example):

```
aws cognito-idp list-users --user-pool-id ap-southeast-2_YtMByLuNg | \
    jq '.Users[] | select(.Attributes[].Name | contains("custom:organisation")) | select(.UserStatus == "CONFIRMED") | .Attributes[] | select (.Name == "email") | .Value' -r
```

Where the assumption is made that they were created with the UI if the custom
attribute "organisation" exists. Other accounts may have been created manually
by an admin (for example, system accounts or CORS data submission accounts).
These other accounts are typically created without an email address.

This returns a CSV containing users and their attributes:

```
aws cognito-idp list-users --user-pool-id ap-southeast-2_YtMByLuNg \
    | jq '[.Users[] | select(.Attributes[].Name | contains("custom:organisation")) | select(.UserStatus == "CONFIRMED") | [{username: .Username}] + [.Attributes[] | {(.Name): .Value}] | add]' \
    | jq -r '(map(keys) | add | unique) as $cols | map(. as $row | $cols | map($row[.])) as $rows | $cols, $rows[] | @csv' > users.csv
```


#### TODO

**Backup Users** - Changes to the schema will cause the User Pool to be deleted
and recreated. Deletion protection is enabled on the Terraform resource, but in
the event of a legitimate need to recreate the pool (or a major accident) we
should backup users. Custom attributes can be added without recreating the user
pool, but they cannot be deleted (so choose new attributes wisely).
