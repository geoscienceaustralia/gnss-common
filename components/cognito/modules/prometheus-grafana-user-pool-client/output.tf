output "id" {
  value = aws_cognito_user_pool_client.prometheus_grafana.id
}

output "secret" {
  value = aws_cognito_user_pool_client.prometheus_grafana.client_secret
}
