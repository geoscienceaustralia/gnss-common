variable "client_name" {
  type = string
}

variable "user_pool" {
  type = object({
    id   = string
    name = string
  })
}

variable "grafana_url" {
  type = string
}
