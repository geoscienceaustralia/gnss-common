resource "aws_cognito_user_pool_client" "prometheus_grafana" {
  name                                 = "${var.user_pool.name}-${var.client_name}"
  user_pool_id                         = var.user_pool.id
  generate_secret                      = true
  allowed_oauth_flows                  = ["code"]
  allowed_oauth_flows_user_pool_client = true
  allowed_oauth_scopes                 = ["openid", "aws.cognito.signin.user.admin", "email"]
  supported_identity_providers         = ["COGNITO"]
  callback_urls                        = ["${var.grafana_url}/login/generic_oauth"]
  logout_urls                          = ["${var.grafana_url}/login/generic_oauth"]
}

resource "aws_ssm_parameter" "prometheus_grafana_client_secret" {
  name  = "/cognito/userpool/${var.user_pool.name}/client/${var.client_name}/secret"
  type  = "SecureString"
  value = aws_cognito_user_pool_client.prometheus_grafana.client_secret
}
