variable "client_name" {
  type = string
}

variable "user_pool" {
  type = object({
    id   = string
    name = string
  })
}

variable "argo_workflows_url" {
  type = string
}
