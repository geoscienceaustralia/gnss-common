output "id" {
  value = aws_cognito_user_pool_client.argo_workflows.id
}

output "secret" {
  value     = aws_cognito_user_pool_client.argo_workflows.client_secret
  sensitive = true
}
