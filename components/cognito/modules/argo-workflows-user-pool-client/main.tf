resource "aws_cognito_user_pool_client" "argo_workflows" {
  name                                 = var.client_name
  user_pool_id                         = var.user_pool.id
  generate_secret                      = true
  allowed_oauth_flows                  = ["code"]
  allowed_oauth_flows_user_pool_client = true
  allowed_oauth_scopes                 = ["openid", "email", "profile"]
  supported_identity_providers         = ["COGNITO"]
  callback_urls                        = ["${var.argo_workflows_url}/oauth2/callback"]
  logout_urls                          = ["${var.argo_workflows_url}/oauth2/callback"]
}

resource "aws_ssm_parameter" "argo_workflows_client_secret" {
  name  = "/cognito/userpool/${var.user_pool.name}/client/${var.client_name}/secret"
  type  = "SecureString"
  value = aws_cognito_user_pool_client.argo_workflows.client_secret
}
