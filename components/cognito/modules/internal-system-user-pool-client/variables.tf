variable "client_name" {
  description = "User pool client name"
  type        = string
}

variable "user_pool" {
  description = "User pool info"
  type = object({
    id   = string
    name = string
  })
}

variable "aws_principals" {
  description = "AWS principals who are allowed to make admin-initiate-auth Cognito API requests"
  type        = list(string)
}
