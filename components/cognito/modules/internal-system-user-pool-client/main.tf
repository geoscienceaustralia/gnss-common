# Shared client configuration for GA's internal systems
#
# At the moment our internal systems tend to authenticate as superuser (i.e., with claim
# `cognito:groups: ["superuser"]`), and allowing only ADMIN_USER_PASSWORD_AUTH flow adds an
# additional level of security.

resource "aws_cognito_user_pool_client" "internal_system" {
  name         = "${var.user_pool.name}-${var.client_name}"
  user_pool_id = var.user_pool.id

  explicit_auth_flows = [
    "ALLOW_ADMIN_USER_PASSWORD_AUTH", # Requires AWS permission `cognito-idp:AdminInitiateAuth`
    "ALLOW_REFRESH_TOKEN_AUTH",
  ]
}

# Internal systems will need to assume this role before they can authenticate.
resource "aws_iam_role" "admin_authoriser" {
  name               = "${aws_cognito_user_pool_client.internal_system.name}-admin-authoriser"
  assume_role_policy = data.aws_iam_policy_document.admin_authoriser_trust_policy.json
}

data "aws_iam_policy_document" "admin_authoriser_trust_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "AWS"
      identifiers = var.aws_principals
    }
  }
}

resource "aws_iam_role_policy" "admin_authoriser_role_policy" {
  role   = aws_iam_role.admin_authoriser.id
  policy = data.aws_iam_policy_document.admin_authoriser_role_policy.json
}

data "aws_iam_policy_document" "admin_authoriser_role_policy" {
  statement {
    actions = [
      "cognito-idp:AdminInitiateAuth"
    ]
    resources = ["arn:aws:cognito-idp:ap-southeast-2:*:userpool/${var.user_pool.id}"]
  }
}
