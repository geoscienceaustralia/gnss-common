terraform {
  backend "s3" {
    encrypt = true
  }
}

provider "aws" {
  region      = var.region
  max_retries = 10
}

resource "aws_cognito_user_pool" "users" {
  name                     = "gnss-users-${terraform.workspace}"
  alias_attributes         = ["email"]
  auto_verified_attributes = ["email"]

  lifecycle {
    prevent_destroy = true
  }

  username_configuration {
    case_sensitive = false
  }

  schema {
    name                     = "email"
    developer_only_attribute = false
    attribute_data_type      = "String"
    mutable                  = false
    required                 = true

    string_attribute_constraints {
      max_length = "2048"
      min_length = "0"
    }
  }

  schema {
    name                     = "country"
    developer_only_attribute = false
    attribute_data_type      = "String"
    mutable                  = true
    required                 = false

    string_attribute_constraints {
      max_length = "2048"
      min_length = "0"
    }
  }

  schema {
    name                     = "industry"
    developer_only_attribute = false
    attribute_data_type      = "String"
    mutable                  = true
    required                 = false

    string_attribute_constraints {
      max_length = "2048"
      min_length = "0"
    }
  }

  schema {
    name                     = "organisation"
    developer_only_attribute = false
    attribute_data_type      = "String"
    mutable                  = true
    required                 = false

    string_attribute_constraints {
      max_length = "2048"
      min_length = "0"
    }
  }

  email_configuration {
    email_sending_account = "DEVELOPER"
    source_arn            = aws_ses_email_identity.from_address.arn
  }

  verification_message_template {
    default_email_option  = "CONFIRM_WITH_LINK"
    email_subject_by_link = "AUSCORS NTRIP Broadcaster Account Confirmation ${terraform.workspace == "prod" ? "" : terraform.workspace}"
    email_message_by_link = <<EOT
      Thank you for registering for an account with the AUSCORS NTRIP Broadcaster. We have received your registration request and now need to verify your email address.<br /><br />

      Please click the following link to verify your email address: {##Click Here##}<br /><br />

      Once verified, you will be able to connect to the AUSCORS NTRIP Broadcaster using the username and password provided during the registration process.<br /><br />

      Further details on how to connect can be found at: <a href="https://gnss.ga.gov.au/stream">https://gnss.ga.gov.au/stream</a><br /><br />
      
      Kind Regards,<br />
      Positioning Operations Centre<br />
      National Positioning Infrastructure | Place, Space and Communities Division<br />
      Geoscience Australia
      EOT
  }

  account_recovery_setting {
    recovery_mechanism {
      name     = "verified_email"
      priority = 1
    }
  }
}

# TODO: Add UI customizations to cognito login screen when added to terraform
# https://github.com/terraform-providers/terraform-provider-aws/issues/3634

resource "aws_cognito_user_pool_domain" "users" {
  domain       = "gnss-users-${terraform.workspace}"
  user_pool_id = aws_cognito_user_pool.users.id
}

resource "aws_cognito_user_group" "superuser" {
  name         = "superuser"
  user_pool_id = aws_cognito_user_pool.users.id
  description  = "System Admin Users"
}
