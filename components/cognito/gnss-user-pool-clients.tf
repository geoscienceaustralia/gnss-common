# This file defines the following clients for user pools `gnss-users-dev/test/prod`:
#
#   * Grafana
#   * Argo Workflows for ginan-ops
#   * Shared Argo Workflows (not used yet)
#   * GNSS Site Manager
#   * Shared client for GA's internal systems

# Grafana
#
# Grafana installations in dev, test, and prod are all configured to use the production user pool,
# gnss-users-prod. However, for experimenting we also have clients configured for dev and test
# user pools.
#
#   gnss-users-dev  -> grafana-dev (for testing, not used)
#   gnss-users-test -> grafana-test (for testing, not used)
#
#   gnss-users-prod -> grafana-dev
#   gnss-users-prod -> grafana-test
#   gnss-users-prod -> grafana-prod

# gnss-users-$env -> grafana-$env
module "gnss_users_prometheus_grafana_client" {
  source = "./modules/prometheus-grafana-user-pool-client"

  client_name = "prometheus-grafana-${terraform.workspace}"
  grafana_url = "https://grafana.${terraform.workspace}-eks.gnss.ga.gov.au"

  user_pool = {
    id   = aws_cognito_user_pool.users.id
    name = aws_cognito_user_pool.users.name
  }
}

# gnss-users-prod -> grafana-dev
module "gnss_users_prod_prometheus_grafana_client_dev" {
  source = "./modules/prometheus-grafana-user-pool-client"
  count  = terraform.workspace == "prod" ? 1 : 0

  client_name = "prometheus-grafana-dev"
  grafana_url = "https://grafana.dev-eks.gnss.ga.gov.au"

  user_pool = {
    id   = aws_cognito_user_pool.users.id
    name = aws_cognito_user_pool.users.name
  }
}

# gnss-users-prod -> grafana-test
module "gnss_users_prod_prometheus_grafana_client_test" {
  source = "./modules/prometheus-grafana-user-pool-client"
  count  = terraform.workspace == "prod" ? 1 : 0

  client_name = "prometheus-grafana-test"
  grafana_url = "https://grafana.test-eks.gnss.ga.gov.au"

  user_pool = {
    id   = aws_cognito_user_pool.users.id
    name = aws_cognito_user_pool.users.name
  }
}

# gnss-portal UI registration page: https://bitbucket.org/geoscienceaustralia/gnss-portal-ui
resource "aws_cognito_user_pool_client" "gnss-portal-ui" {
  name                                 = "gnss-portal-ui-${terraform.workspace}"
  user_pool_id                         = aws_cognito_user_pool.users.id
  generate_secret                      = false
  allowed_oauth_flows                  = ["code"]
  allowed_oauth_flows_user_pool_client = true
  allowed_oauth_scopes                 = ["openid", "profile", "email"]
  supported_identity_providers         = ["COGNITO"]
  callback_urls                        = ["https://${terraform.workspace == "prod" ? "" : "nonprod."}gnss.ga.gov.au"]
  logout_urls                          = ["https://${terraform.workspace == "prod" ? "" : "nonprod."}gnss.ga.gov.au"]
  explicit_auth_flows                  = ["ALLOW_ADMIN_USER_PASSWORD_AUTH", "ALLOW_USER_PASSWORD_AUTH", "ALLOW_REFRESH_TOKEN_AUTH"]
}

# Argo Workflows client for ginan-ops
#
# Argo Workflows for ginan-ops installations in dev, test, and prod are all configured to use
# the production user pool, gnss-users-prod. However, for experimenting we also have clients
# configured for dev and test user pools.
#
#   gnss-users-dev  -> argo-workflows-ginan-ops-dev (for testing, not used)
#   gnss-users-test -> argo-workflows-ginan-ops-test (for testing, not used)
#
#   gnss-users-prod -> argo-workflows-ginan-ops-dev
#   gnss-users-prod -> argo-workflows-ginan-ops-test
#   gnss-users-prod -> argo-workflows-ginan-ops-prod

# gnss-users-$env -> argo-workflows-ginan-ops-$env
module "gnss_users_argo_workflows_ginan_ops_client" {
  source = "./modules/argo-workflows-user-pool-client"

  client_name        = "argo-workflows-ginan-ops-${terraform.workspace}"
  argo_workflows_url = "https://argo-workflows-ginan-ops.${terraform.workspace}-eks.gnss.ga.gov.au"

  user_pool = {
    id   = aws_cognito_user_pool.users.id
    name = aws_cognito_user_pool.users.name
  }
}

# gnss-users-prod -> argo-workflows-ginan-ops-dev
module "gnss_users_prod_argo_workflows_ginan_ops_client_dev" {
  source = "./modules/argo-workflows-user-pool-client"
  count  = terraform.workspace == "prod" ? 1 : 0

  client_name        = "argo-workflows-ginan-ops-dev"
  argo_workflows_url = "https://argo-workflows-ginan-ops.dev-eks.gnss.ga.gov.au"

  user_pool = {
    id   = aws_cognito_user_pool.users.id
    name = aws_cognito_user_pool.users.name
  }
}

# gnss-users-prod -> argo-workflows-ginan-ops-test
module "gnss_users_prod_argo_workflows_ginan_ops_client_test" {
  source = "./modules/argo-workflows-user-pool-client"
  count  = terraform.workspace == "prod" ? 1 : 0

  client_name        = "argo-workflows-ginan-ops-test"
  argo_workflows_url = "https://argo-workflows-ginan-ops.test-eks.gnss.ga.gov.au"

  user_pool = {
    id   = aws_cognito_user_pool.users.id
    name = aws_cognito_user_pool.users.name
  }
}

# Argo Workflows client for global installation of Argo
# TODO: Global Argo installation is not yet complete

# gnss-users-$env -> argo-workflows-$env
module "gnss_users_argo_workflows_client" {
  source = "./modules/argo-workflows-user-pool-client"

  client_name        = "argo-workflows-${terraform.workspace}"
  argo_workflows_url = "https://argo-workflows.${terraform.workspace}-eks.gnss.ga.gov.au"

  user_pool = {
    id   = aws_cognito_user_pool.users.id
    name = aws_cognito_user_pool.users.name
  }
}

# gnss-users-prod -> argo-workflows-dev
module "gnss_users_prod_argo_workflows_client_dev" {
  source = "./modules/argo-workflows-user-pool-client"
  count  = terraform.workspace == "prod" ? 1 : 0

  client_name        = "argo-workflows-dev"
  argo_workflows_url = "https://argo-workflows.dev-eks.gnss.ga.gov.au"

  user_pool = {
    id   = aws_cognito_user_pool.users.id
    name = aws_cognito_user_pool.users.name
  }
}

# gnss-users-prod -> argo-workflows-test
module "gnss_users_prod_argo_workflows_client_test" {
  source = "./modules/argo-workflows-user-pool-client"
  count  = terraform.workspace == "prod" ? 1 : 0

  client_name        = "argo-workflows-test"
  argo_workflows_url = "https://argo-workflows.test-eks.gnss.ga.gov.au"

  user_pool = {
    id   = aws_cognito_user_pool.users.id
    name = aws_cognito_user_pool.users.name
  }
}

# GNSS Site Manager
#
# GNSS Site Manager installations in dev, test, and prod are configured to use gnss-user-dev,
# gnss-user-test, gnss-test-prod user pools, respectively.
#
#   gnss-users-dev  -> gnss-site-manager-dev
#   gnss-users-test -> gnss-site-manager-test
#   gnss-users-prod -> gnss-site-manager-prod

# gnss-users-$env -> gnss-site-manager-$env
resource "aws_cognito_user_pool_client" "gnss_site_manager" {
  name         = "gnss-site-manager-${terraform.workspace}"
  user_pool_id = aws_cognito_user_pool.users.id

  explicit_auth_flows = [
    "ALLOW_ADMIN_USER_PASSWORD_AUTH",
    "ALLOW_USER_PASSWORD_AUTH",
    "ALLOW_USER_SRP_AUTH",
    "ALLOW_REFRESH_TOKEN_AUTH",
  ]
}

resource "aws_ssm_parameter" "gnss_site_manager_client_id" {
  name  = "/${aws_cognito_user_pool.users.name}/app/${aws_cognito_user_pool_client.gnss_site_manager.name}/client-id"
  value = aws_cognito_user_pool_client.gnss_site_manager.id
  type  = "String"
}

# GNSS Data Repository
#
# GNSS Data Repository installations in dev, test, and prod are all configured to use gnss-user-prod,
# gnss-user-test, gnss-test-prod user pools, respectively.
#
#   gnss-users-dev  -> gnss-data-repository-dev
#   gnss-users-test -> gnss-data-repository-test
#   gnss-users-prod -> gnss-data-repository-prod

# gnss-users-$env -> gnss-data-repository-$env
resource "aws_cognito_user_pool_client" "gnss_data_repository" {
  name         = "gnss-data-repository-${terraform.workspace}"
  user_pool_id = aws_cognito_user_pool.users.id

  explicit_auth_flows = [
    "ALLOW_ADMIN_USER_PASSWORD_AUTH",
    "ALLOW_USER_PASSWORD_AUTH",
    "ALLOW_REFRESH_TOKEN_AUTH",
  ]
}

# Shared client for GA's internal systems
# gnss-users-$env -> internal-system-$env
module "gnss_users_internal_system_client" {
  source      = "./modules/internal-system-user-pool-client"
  client_name = "internal-system-${terraform.workspace}"

  user_pool = {
    id   = aws_cognito_user_pool.users.id
    name = aws_cognito_user_pool.users.name
  }

  aws_principals = terraform.workspace == "prod" ? var.prod_aws_principals : var.nonprod_aws_principals
}

# Shared client for authentication of external API users
# gnss-users-$env -> external-api-users-$env
resource "aws_cognito_user_pool_client" "external_api_users" {
  name         = "external-api-users-${terraform.workspace}"
  user_pool_id = aws_cognito_user_pool.users.id

  explicit_auth_flows = [
    "ALLOW_USER_PASSWORD_AUTH",
    "ALLOW_REFRESH_TOKEN_AUTH",
  ]
}
