output "gnss_user_pool" {
  value = {
    id     = aws_cognito_user_pool.users.id
    name   = aws_cognito_user_pool.users.name
    domain = aws_cognito_user_pool_domain.users.domain
  }
}

# gnss-users-$env -> grafana-$env
output "gnss_users_prometheus_grafana_client" {
  value = {
    id     = module.gnss_users_prometheus_grafana_client.id
    secret = module.gnss_users_prometheus_grafana_client.secret
  }
  sensitive = true
}

# gnss-users-prod -> grafana-(dev|test)
output "gnss_users_prod_prometheus_grafana_clients" {
  value = {
    "dev" = (
      length(module.gnss_users_prod_prometheus_grafana_client_dev) == 1
      ? {
        id     = module.gnss_users_prod_prometheus_grafana_client_dev[0].id
        secret = module.gnss_users_prod_prometheus_grafana_client_dev[0].secret
      }
      : null
    )
    "test" = (
      length(module.gnss_users_prod_prometheus_grafana_client_test) == 1
      ? {
        id     = module.gnss_users_prod_prometheus_grafana_client_test[0].id
        secret = module.gnss_users_prod_prometheus_grafana_client_test[0].secret
      }
      : null
    )
  }
  sensitive = true
}

# gnss-users-$env -> argo-workflows-ginan-ops-$env
output "gnss_users_argo_workflows_ginan_ops_client" {
  value = {
    id     = module.gnss_users_argo_workflows_ginan_ops_client.id
    secret = module.gnss_users_argo_workflows_ginan_ops_client.secret
  }
  sensitive = true
}

# gnss-users-prod -> argo-workflows-ginan-ops-(dev|test)
output "gnss_users_prod_argo_workflows_ginan_ops_clients" {
  value = {
    "dev" = (
      length(module.gnss_users_prod_argo_workflows_ginan_ops_client_dev) == 1
      ? {
        id     = module.gnss_users_prod_argo_workflows_ginan_ops_client_dev[0].id
        secret = module.gnss_users_prod_argo_workflows_ginan_ops_client_dev[0].secret
      }
      : null
    )
    "test" = (
      length(module.gnss_users_prod_argo_workflows_ginan_ops_client_test) == 1
      ? {
        id     = module.gnss_users_prod_argo_workflows_ginan_ops_client_test[0].id
        secret = module.gnss_users_prod_argo_workflows_ginan_ops_client_test[0].secret
      }
      : null
    )
  }
  sensitive = true
}
