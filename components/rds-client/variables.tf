variable "region" {
  type = string
}

variable "nonprod_aws_principals" {
  type    = list(string)
  default = []
}

variable "prod_aws_principals" {
  type    = list(string)
  default = []
}

variable "requester_system" {
  type = string
}

variable "requester_vpc" {
  type = object({
    id              = string
    cidr_block      = string
    route_table_ids = list(string)
  })
}

variable "accepter_system" {
  type = string
}

variable "db_instance_exists" {
  type    = bool
  default = true
}
