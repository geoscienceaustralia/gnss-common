data "aws_caller_identity" "requester" {
}

data "aws_caller_identity" "accepter" {
  provider = aws.accepter
}

data "aws_vpc" "accepter" {
  provider = aws.accepter

  tags = {
    Name = var.accepter_system
  }
}

data "aws_route_tables" "accepter" {
  provider = aws.accepter
  vpc_id   = data.aws_vpc.accepter.id

  filter {
    name   = "tag:Name"
    values = ["${var.accepter_system}-db-*"]
  }
}

data "aws_security_group" "accepter" {
  provider = aws.accepter

  tags = {
    Name = "${var.accepter_system}-postgresql"
  }
}

locals {
  accepter_vpc = {
    id              = data.aws_vpc.accepter.id
    cidr_block      = data.aws_vpc.accepter.cidr_block_associations.0.cidr_block
    route_table_ids = sort(data.aws_route_tables.accepter.ids)
  }
  is_cross_account = data.aws_caller_identity.requester.account_id == data.aws_caller_identity.accepter.account_id
}

resource "aws_vpc_peering_connection" "requester" {
  vpc_id        = var.requester_vpc.id
  peer_vpc_id   = local.accepter_vpc.id
  peer_owner_id = data.aws_caller_identity.accepter.account_id
  auto_accept   = local.is_cross_account

  tags = {
    Name = "${var.requester_system}-requester-of-${var.accepter_system}"
  }
}

resource "aws_route" "requester" {
  count = length(var.requester_vpc.route_table_ids)

  route_table_id            = var.requester_vpc.route_table_ids[count.index]
  destination_cidr_block    = local.accepter_vpc.cidr_block
  vpc_peering_connection_id = aws_vpc_peering_connection.requester.id
}

resource "aws_vpc_peering_connection_accepter" "accepter" {
  provider = aws.accepter
  count    = local.is_cross_account ? 0 : 1

  vpc_peering_connection_id = aws_vpc_peering_connection.requester.id
  auto_accept               = true

  tags = {
    Name = "${var.accepter_system}-accepter-of-${var.requester_system}"
  }
}

resource "aws_route" "accepter" {
  provider = aws.accepter
  count    = length(local.accepter_vpc.route_table_ids)

  route_table_id            = local.accepter_vpc.route_table_ids[count.index]
  destination_cidr_block    = var.requester_vpc.cidr_block
  vpc_peering_connection_id = aws_vpc_peering_connection.requester.id
}

resource "aws_security_group_rule" "requester" {
  type              = "ingress"
  from_port         = 5432
  to_port           = 5432
  protocol          = "tcp"
  description       = "PostgreSQL"
  cidr_blocks       = [var.requester_vpc.cidr_block]
  security_group_id = data.aws_security_group.accepter.id
}
