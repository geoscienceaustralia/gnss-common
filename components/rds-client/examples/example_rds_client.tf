locals {
  system = "example-rds-client-dev"

  vpc = {
    cidr_block                = "10.1.0.0/16"
    public_subnet_cidr_blocks = ["10.1.1.0/24"]
  }
}

data "aws_region" "current" {
}

data "aws_availability_zones" "available" {
}

module "example_vpc" {
  source = "git@github.com:terraform-aws-modules/terraform-aws-vpc.git?ref=v2.23.0"

  name           = local.system
  cidr           = local.vpc.cidr_block
  azs            = data.aws_availability_zones.available.names
  public_subnets = local.vpc.public_subnet_cidr_blocks
}

resource "aws_instance" "client" {
  ami                  = "ami-02a599eb01e3b3c5b"
  instance_type        = "t2.small"
  iam_instance_profile = data.aws_iam_instance_profile.ssm_quick_setup.name

  tags = {
    Name = "${local.system}"
  }

  subnet_id = module.example_vpc.public_subnets.0
}

data "aws_iam_instance_profile" "ssm_quick_setup" {
  name = "AmazonSSMRoleForInstancesQuickSetup"
}

module "rds_client" {
  source = "git@bitbucket.org:geoscienceaustralia/gnss-common//components/rds-client?ref=8cabb7f"
  region = data.aws_region.current.name

  requester_system = local.system

  requester_vpc = {
    id              = module.example_vpc.vpc_id
    cidr_block      = local.vpc.cidr_block
    route_table_ids = sort(module.example_vpc.public_route_table_ids)

    # The number of route tables needs to be known during the plan phase.
    # We will assume that there is one route table per subnet, which is the
    # default behaviour.
    route_table_count = length(local.vpc.public_subnet_cidr_blocks)
  }

  accepter_system = "gnss-rds-dev"
}

output "rds_endpoint" {
  value = module.rds_client.rds_endpoint
}
