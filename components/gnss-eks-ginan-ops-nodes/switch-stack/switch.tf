# 1) Test next node group
# 2) Activate next node group
# 3) Deactivate current node group
# 4) Update current stack label in SSM Parameters

locals {
  node_group         = "ginan-ops"
  current_node_group = "${local.node_group}-${var.current_stack}"
  next_node_group    = "${local.node_group}-${var.next_stack}"
}

# Run kubernetes job to completion in the next node group.
resource "kubernetes_job" "test_next_node_group" {
  metadata {
    generate_name = "${local.next_node_group}-test-"
  }
  spec {
    template {
      metadata {}
      spec {
        container {
          name    = "test"
          image   = "busybox:latest"
          command = ["sleep", "1"]
        }
        node_selector = {
          dedicated = local.node_group
        }
        toleration {
          effect   = "NoSchedule"
          key      = "dedicated"
          operator = "Equal"
          value    = local.next_node_group
        }
        restart_policy = "Never"
      }
    }
  }
  wait_for_completion = true
  timeouts {
    create = "60s"
  }
}

# Update next node group taint
# dedicated:ginan-ops-$current:NoSchedule -> dedicated:ginan-ops:NoSchedule
resource "null_resource" "activate_next_node_group" {
  triggers = {
    now = var.next_stack
  }
  provisioner "local-exec" {
    command = <<-EOF
      aws eks update-nodegroup-config \
        --cluster-name ${var.cluster_info.cluster_id} \
        --nodegroup-name ${local.next_node_group} \
        --taints 'addOrUpdateTaints={key=dedicated,value=${local.node_group},effect=NO_SCHEDULE}'
      aws eks wait nodegroup-active \
        --cluster-name ${var.cluster_info.cluster_id} \
        --nodegroup-name ${local.next_node_group}
    EOF
  }
  depends_on = [
    kubernetes_job.test_next_node_group,
  ]
}

# Update current node group taint
# dedicated:ginan-ops:NoSchedule -> dedicated:ginan-ops-$current:NoSchedule
resource "null_resource" "deactivate_current_node_group" {
  count = var.current_stack != "" ? 1 : 0
  triggers = {
    now = var.current_stack
  }
  provisioner "local-exec" {
    command = <<-EOF
      aws eks update-nodegroup-config \
        --cluster-name ${var.cluster_info.cluster_id} \
        --nodegroup-name ${local.current_node_group} \
        --taints 'addOrUpdateTaints={key=dedicated,value=${local.current_node_group},effect=NO_SCHEDULE}'
      aws eks wait nodegroup-active \
        --cluster-name ${var.cluster_info.cluster_id} \
        --nodegroup-name ${local.next_node_group}
    EOF
  }
  depends_on = [
    kubernetes_job.test_next_node_group,
    null_resource.activate_next_node_group,
  ]
}

# Set current node label to next
resource "aws_ssm_parameter" "current_stack" {
  name      = "/gnss-eks-ginan-ops-nodes/current-blue-green-stack-${terraform.workspace}"
  type      = "String"
  value     = var.next_stack
  overwrite = true
  depends_on = [
    null_resource.deactivate_current_node_group,
  ]
}
