variable "cluster_info" {
  description = "Terraform outputs of components/gnss-eks."
  type        = any
}

variable "current_stack" {
  description = "Switching will deactivate the current stack."
  type        = string
  validation {
    condition     = contains(["", "blue", "green"], var.current_stack)
    error_message = "Allowed values for current_stack are \"\", \"blue\", or \"green\"."
  }
}

variable "next_stack" {
  description = "Switching will activate the next stack."
  type        = string
  validation {
    condition     = contains(["blue", "green"], var.next_stack)
    error_message = "Allowed values for next_stack are \"blue\" or \"green\"."
  }
}
