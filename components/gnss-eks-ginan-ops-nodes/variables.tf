variable "region" {
  description = "AWS region"
  type        = string
}

variable "instance_type" {
  description = "Node instance type"
  type        = string
}

variable "scaling_config" {
  description = "https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eks_node_group#scaling_config"
  type = object({
    desired_size = number
    max_size     = number
    min_size     = number
  })
}

variable "current_stack" {
  type    = string
  default = ""
  validation {
    condition     = contains(["", "blue", "green"], var.current_stack)
    error_message = "Allowed values for current_stack are \"\", \"blue\", or \"green\"."
  }
}

variable "next_stack" {
  type    = string
  default = "blue"
  validation {
    condition     = contains(["blue", "green"], var.next_stack)
    error_message = "Allowed values for next_stack are \"blue\" or \"green\"."
  }
}

variable "node_volume_size" {
  description = "Node root volume size on gigabytes."
  type        = number
  default     = 100
}
