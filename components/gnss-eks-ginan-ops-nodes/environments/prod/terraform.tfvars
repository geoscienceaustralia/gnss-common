instance_type = "t3.xlarge"

scaling_config = {
  desired_size = 2
  max_size     = 3
  min_size     = 2
}

node_volume_size = 200
