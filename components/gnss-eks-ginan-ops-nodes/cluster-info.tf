data "terraform_remote_state" "gnss_eks" {
  backend = "s3"
  config = {
    bucket = "gnss-common-${terraform.workspace == "prod" ? "prod" : "nonprod"}-terraform-state"
    key    = "gnss-eks-2/${terraform.workspace}/terraform.tfstate"
    region = "ap-southeast-2"
  }
}

data "aws_eks_cluster" "gnss_eks" {
  name = data.terraform_remote_state.gnss_eks.outputs.cluster_id
}

locals {
  cluster_info = merge(data.terraform_remote_state.gnss_eks.outputs, {
    k8s_version = data.aws_eks_cluster.gnss_eks.version
  })
}
