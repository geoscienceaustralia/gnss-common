terraform {
  backend "s3" {
    encrypt = true
  }
}

module "switch" {
  source        = "./switch-stack"
  cluster_info  = local.cluster_info
  current_stack = var.current_stack
  next_stack    = var.next_stack
}

# TODO: Consider switching to `module.stack["blue"]` once
# - https://github.com/hashicorp/terraform/issues/29116
# - https://github.com/hashicorp/terraform/issues/25836
# It would mean we wouldn't have to provide module inputs twice

module "blue" {
  source           = "./blue-green-stack"
  count            = var.next_stack == "blue" ? 1 : 0
  stack            = var.next_stack
  region           = var.region
  cluster_info     = local.cluster_info
  instance_type    = var.instance_type
  scaling_config   = var.scaling_config
  node_volume_size = var.node_volume_size
}

module "green" {
  source           = "./blue-green-stack"
  count            = var.next_stack == "green" ? 1 : 0
  stack            = var.next_stack
  region           = var.region
  cluster_info     = local.cluster_info
  instance_type    = var.instance_type
  scaling_config   = var.scaling_config
  node_volume_size = var.node_volume_size
}
