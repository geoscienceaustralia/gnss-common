variable "region" {
  description = "AWS region"
  type        = string
}

variable "cluster_info" {
  description = "Terraform outputs of components/gnss-eks."
  type        = any
}

variable "instance_type" {
  description = "Node instance type"
  type        = string
}

variable "scaling_config" {
  description = "https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eks_node_group#scaling_config"
  type = object({
    desired_size = number
    max_size     = number
    min_size     = number
  })
}

variable "stack" {
  description = "Stack label, blue or green."
  type        = string
  validation {
    condition     = contains(["blue", "green"], var.stack)
    error_message = "Allowed values for input_parameter are \"blue\" or \"green\"."
  }
}

variable "node_volume_size" {
  description = "Node root volume size on gigabytes."
  type        = number
}
