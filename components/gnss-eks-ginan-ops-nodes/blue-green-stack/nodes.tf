locals {
  node_group_name = "ginan-ops"

  availability_zones = sort(data.aws_availability_zones.all.names)

  # If we are running fewer nodes than there are zones, ensure we always run in the same subset,
  # so that pods in stateful sets can always reconnect to their EBS volumes.
  target_availabilty_zones = slice(local.availability_zones,
    0, min(length(local.availability_zones), var.scaling_config.desired_size)
  )
}

data "aws_availability_zones" "all" {
  state = "available"
}

data "aws_subnets" "nodes" {
  filter {
    name   = "subnet-id"
    values = var.cluster_info.private_subnets
  }
}

data "aws_subnet" "nodes" {
  for_each = toset(data.aws_subnets.nodes.ids)
  id       = each.value
}

resource "aws_eks_node_group" "nodes" {
  cluster_name    = var.cluster_info.cluster_id
  node_group_name = "${local.node_group_name}-${var.stack}"
  node_role_arn   = var.cluster_info.node_role_arn

  subnet_ids = [
    for s in data.aws_subnet.nodes :
    s.id if contains(local.target_availabilty_zones, s.availability_zone)
  ]

  version = var.cluster_info.k8s_version

  launch_template {
    id      = aws_launch_template.node.id
    version = aws_launch_template.node.latest_version
  }

  scaling_config {
    desired_size = var.scaling_config.desired_size
    max_size     = var.scaling_config.max_size
    min_size     = var.scaling_config.min_size
  }

  update_config {
    max_unavailable = 1
  }

  labels = {
    dedicated = "${local.node_group_name}"
  }

  taint {
    key    = "dedicated"
    value  = "${local.node_group_name}-${var.stack}"
    effect = "NO_SCHEDULE"
  }

  provisioner "local-exec" {
    when    = destroy
    command = <<-EOF
      eksctl drain nodegroup \
        --cluster=${self.cluster_name} \
        --name=${self.node_group_name}
    EOF
  }
}

resource "aws_launch_template" "node" {
  name_prefix   = "${var.cluster_info.cluster_id}-${local.node_group_name}-${var.stack}"
  instance_type = var.instance_type

  network_interfaces {
    associate_public_ip_address = false
    security_groups             = [var.cluster_info.node_security_group]
    delete_on_termination       = true
  }

  lifecycle {
    create_before_destroy = true
  }

  block_device_mappings {
    device_name = "/dev/xvda"
    ebs {
      volume_size = var.node_volume_size
    }
  }

  tag_specifications {
    resource_type = "instance"

    tags = {
      Name = "${var.cluster_info.cluster_id}-${local.node_group_name}"
    }
  }
}
