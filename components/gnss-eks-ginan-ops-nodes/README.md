# GNSS-EKS Ginan-ops Nodes

This component deploys an EKS node group called `ginan-ops` with

* label: `dedicated:ginan-ops`
* taint: `dedicated:ginan-ops:NoSchedule`

used by https://bitbucket.org/geoscienceaustralia/ginan-ops.

The following is an example of a pod that will be scheduled to a ginan-ops node.

```yaml
kind: Pod
...
spec:
  ...
  nodeSelector:
    dedicated: ginan-ops
  tolerations:
    - effect: NoSchedule
      key: dedicated
      operator: Equals
      value: ginan-ops
```

## Blue-Green Deployments

Updates are managed using blue-green deployments by passing commands to option `--blue-green`.

```bash
$ ./deploy.sh components/gnss-eks-ginan-ops-nodes dev --blue-green (show|deploy|switch|destroy)
```

Instead of destructively updating a node group that is in active use, the depoyment script will
create a new inactive node group along side the existing active node group (`--blue-green deploy`).
The process provides the user with an opportunity to manually test the new node group, before
enabling it to take on work (`--blue-green switch`).

![Blue-Green State Diagram](blue-green-state-diagram.svg)

`./deploy.sh` scripts keeps the value of the current blue-green stack label in SSM parameters.

```bash
$ aws ssm get-parameter --name /gnss-eks-ginan-ops-nodes/current-blue-green-stack-dev | jq .Parameter.Value
$ "blue"
$ # or
$ ./deploy.sh components/gnss-eks-ginan-ops-nodes dev --blue-green show
$ Current stack is blue
$ Next stack will be green
```

Prior to the first deployment, current stack is none and next stack is "blue". The following sequence
of commands deploys the "blue" stack from scratch and then switches to "green".

```bash
$ ./deploy.sh components/gnss-eks-ginan-ops-nodes dev --blue-green deploy
$ ./deploy.sh components/gnss-eks-ginan-ops-nodes dev --blue-green switch
$ # "blue" nodes are now active and will take on new pods
$ ./deploy.sh components/gnss-eks-ginan-ops-nodes dev --blue-green deploy
# You can now test the new "green" nodes for regressions against "blue" nodes
$ ./deploy.sh components/gnss-eks-ginan-ops-nodes dev --blue-green switch
$ # "green" nodes are now active and will take on new pods, "blue" nodes are inactive
$ ./deploy.sh components/gnss-eks-ginan-ops-nodes dev --blue-green destroy
$ # "blue" nodes are now drained, cordoned, and destroyed

```

A node group is considered active if it has taint `dedicated:ginan-ops:NoSchedule`. Inactive nodes
(tainted `dedicated:ginan-ops-(blue|green):NoSchedule`) will not take on new pods, but any pods
already running on inactive nodes will not be evicted until the inactive node group is destroyed.

Before swapping active and inactive node groups, `switch` will first run a test pod in the inactive
node group.

All `--blue-green` commands will honour option `--dry-run` to print the output of `terraform plan`.
